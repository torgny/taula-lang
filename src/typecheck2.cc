#include "parser2.hh"
#include "typecheck2.hh"
#include "type_builder.hh"
#include <sstream>
#include <assert.h>
#include <stdlib.h>

#define debug(...) //printf(__VA_ARGS__)

namespace taula {

class Scope {
  typedef std::unordered_map<String, Type const*> Map;
  std::vector<Map> m_scopes;
  TypeBuilder const& tb;
public:
  Module const* module = nullptr;

  Scope(TypeBuilder const& tb) : tb(tb) { }

  bool empty() const {
    return m_scopes.empty();
  }

  void push_scope() {
    m_scopes.push_back(Map());
  }

  void pop_scope() {
    m_scopes.pop_back();
  }

  // A local variable can be defined if there is no local variable with the
  // same name in the top-most scope.
  bool can_define(String name) const {
    return m_scopes.back().count(name) == 0;
  }

  void insert(String name, Type const* type) {
    assert(not empty());
    Map& map = m_scopes.back();
    assert(map.find(name) == map.end());
    map[name] = type;
  }

  Type const* lookup(String name) const {
    debug("lookup(%s)\n", name.str().c_str());
    for (auto it = m_scopes.rbegin(); it != m_scopes.rend(); it++) {
      Map const& map = *it;
      auto i = map.find(name);
      if (i != map.end())
        return i->second;
    }

    if (module->has_variable(name))
      return module->lookup_variable(name);
    if (module->has_func(name))
      return module->lookup_func(name);

    auto builtins = tb.lookup_module(String("__builtins__.tau"));
    if (builtins->has_variable(name))
      return builtins->lookup_variable(name);
    if (builtins->has_func(name))
      return builtins->lookup_func(name);
    printf("Error: '%s' is not defined.\n", name.str().c_str());
    assert(false);
  }
};

class Checker : public ast::Visitor {
  TypeBuilder const& m_modules;
  NodeTypes& m_types;

  Scope m_scope;

  Type const* m_member_call_target = nullptr;

  // Is 'b' a subtype of 'a' (or the same type)?
  bool is_subtype(Type const* a, Type const* b) {
    // TODO: implement subtype check.
    if (a == b)
      assert(a->str() == b->str());
    else
      assert(a->str() != b->str());
    debug("is subtype: decl: %s, actual: %s\n", a->str().c_str(), b->str().c_str());
    return a == b;
  }

  bool arguments_match(std::vector<Type const*> const& decltypes,
                       std::vector<Type const*> const& calltypes) {
    if (decltypes.size() != calltypes.size())
      return false;
    //for (unsigned i = 0; i < decltypes.size(); i++) {
    //  if (not is_subtype(decltypes[i], calltypes[i]))
    //    return false;
    //}
    return true;
  }

  Type const* get_type_from_typename(ast::Type const* ast) {
    assert(ast->asttype == ast::TypeTag::Typename);
    auto tn = (ast::Typename const*)ast;
    auto filename = m_scope.module->filename_for_import(tn->module);

    std::vector<Type const*> params;
    for (auto t : tn->params) {
      params.push_back(get_type_from_typename(t));
    }
    return m_modules.lookup_type(filename, tn->name, params);
  }

  Type const* get_type(ast::Node const* node) const {
    auto it = m_types.find(node);
    assert(it != m_types.end() && "Type not yet inferred.");
    return it->second;
  }

  void set_type(ast::Node const* node, Type const* type) {
    assert(node != nullptr);
    assert(type != nullptr);
    auto it = m_types.find(node);
    assert(it == m_types.end() && "Type already inferred.");
    m_types[node] = type;
  }

  // Get the type of the last expression of a compound statement or expression
  // statement.
  Type const* type_of_stmt(ast::Stmt const* stmt) {
    if (stmt->asttype == ast::TypeTag::CompoundStmt) {
      auto cstmt = (ast::CompoundStmt const*)stmt;
      if (cstmt->stmts.size() == 0)
        return void_type();
      return type_of_stmt(cstmt->stmts.back());
    } else if (stmt->asttype == ast::TypeTag::ExprStmt) {
      return get_type(((ast::ExprStmt const*)stmt)->expr);
    }
    return void_type();
  }

  Type const* void_type() const {
    return m_modules.lookup_type(String("__builtins__.tau"), String("void"), {});
  }

  Type const* pointer_to_type(Type const* type) {
    return m_modules.lookup_type(String("__builtins__.tau"), String("Pointer"), {type});
  }

public:
  Checker(TypeBuilder const& modules, NodeTypes& types) : m_modules(modules), m_types(types), m_scope(modules) { }

  // NewExpr           < Expr :: type:*Type, args:[*Expr]
  void visit_new_expr(ast::NewExpr const* node) {
    debug("visit_new_expr\n");
    for (auto arg : node->args)
      visit(arg);

    auto type = (ObjectType const*)get_type_from_typename(node->type);
    if (type->def_name() == "__builtins__.tau:Array") {
      set_type(node, type);
    } else {
      set_type(node, pointer_to_type(type));
    }
  }

  // CastExpr           < Expr :: type:*Type, arg:*Expr
  void visit_cast_expr(ast::CastExpr const* node) {
    debug("visit_cast_expr\n");
    visit(node->arg);
    set_type(node, get_type_from_typename(node->type));
  }

  void visit_string_expr(ast::StringExpr const* node) {
    debug("visit_string_expr\n");
    set_type(node, m_modules.lookup_type(String("__builtins__.tau"), String("str"), {}));
  }

  void visit_number_expr(ast::NumberExpr const* node) {
    debug("visit_number_expr\n");
    set_type(node, m_modules.lookup_type(String("__builtins__.tau"), String("int"), {}));
  }

  void visit_fp_number_expr(ast::FpNumberExpr const* node) {
    debug("visit_fp_number_expr\n");
    set_type(node, m_modules.lookup_type(String("__builtins__.tau"), String("float"), {}));
  }

  void visit_ident_expr(ast::IdentExpr const* node) {
    debug("visit_ident_expr %s\n", node->name.str().c_str());
    if (node->name == String("true") or node->name == String("false")) {
      set_type(node, m_modules.lookup_type(String("__builtins__.tau"), String("bool"), {}));
    } else {
      set_type(node, m_scope.lookup(node->name));
    }
  }

  void visit_unary_expr(ast::UnaryExpr const* node) {
    debug("visit_unary_expr\n");
    visit(node->expr);
    if (node->op == String("not")) {
      assert(get_type(node->expr)->str() == "__builtins__.tau:bool");
      set_type(node, m_modules.lookup_type(String("__builtins__.tau"), String("bool"), {}));
      return;
    }
    assert(false && "Unhandled operator");
  }

  void visit_binary_expr(ast::BinaryExpr const* node) {
    debug("visit_binary_expr: %s\n", node->op.str().c_str());
    visit(node->lhs);
    visit(node->rhs);
    String op = node->op;

    if (op == String("or") or op == String("and")) {
      assert(get_type(node->lhs)->str() == "__builtins__.tau:bool");
      assert(get_type(node->lhs)->str() == "__builtins__.tau:bool");
      set_type(node, m_modules.lookup_type(String("__builtins__.tau"), String("bool"), {}));
      return;
    }

    const char* func_name = nullptr;
    if (op == String("+")) {
      func_name = "__add__";
    } else if (op == String("-")) {
      func_name = "__sub__";
    } else if (op == String("*")) {
      func_name = "__mul__";
    } else if (op == String("/")) {
      func_name = "__div__";
    } else if (op == String("%")) {
      func_name = "__mod__";
    } else if (op == String("<<")) {
      func_name = "__lshift__";
    } else if (op == String(">>")) {
      func_name = "__rshift__";
    } else if (op == String("&")) {
      func_name = "__and__";
    } else if (op == String("|")) {
      func_name = "__or__";
    } else if (op == String("^")) {
      func_name = "__xor__";
    } else if (op == String("<")) {
      func_name = "__lt__";
    } else if (op == String(">")) {
      func_name = "__gt__";
    } else if (op == String("<=")) {
      func_name = "__le__";
    } else if (op == String(">=")) {
      func_name = "__ge__";
    } else if (op == String("==")) {
      func_name = "__eq__";
    } else if (op == String("!=")) {
      func_name = "__ne__";
    } 
    assert(func_name != nullptr && "Unhandled binary operator");

    auto lhs = (ObjectType const*)get_type(node->lhs);
    //auto rhs = (ObjectType const*)get_type(node->rhs);
    auto const& overloads = *lhs->lookup_funcs(String(func_name));
    // TODO: Find overload correctly by also considering __radd__.
    assert(overloads.size() > 0);
    set_type(node, overloads[0]->rettype());
  }

  void visit_function_expr(ast::FunctionExpr const* node) {
    debug("visit_function_expr\n");
    (void)node;
  }

  void visit_function_call_expr(ast::FunctionCallExpr const* node) {
    debug("visit_function_call_expr\n");
    Type const* tmp_member_call_target = m_member_call_target;
    m_member_call_target = nullptr;
    visit(node->func);
    Type const* member_call_target = m_member_call_target;
    m_member_call_target = tmp_member_call_target;

    std::vector<Type const*> argtypes;
    if (member_call_target != nullptr)
      argtypes.push_back(member_call_target);

    for (auto arg : node->args) {
      visit(arg);
      argtypes.push_back(get_type(arg));
    }
    auto const& overload = *(FunctionOverload const*)get_type(node->func);

    std::vector<FunctionType const*> matches;
    for (unsigned i = 0; i < overload.size(); i++) {
      if (arguments_match(overload[i]->argtypes(), argtypes))
        matches.push_back(overload[i]);
    }

    if (matches.size() == 0u) {
      printf("Error: no matching function to call.\n");
      assert(false);
    }
    set_type(node, matches[0]->rettype());
  }

  // SubscriptExpr     < Expr :: array:*Expr, index:*Expr
  void visit_subscript_expr(ast::SubscriptExpr const* node) {
    debug("visit_subscript_expr\n");
    visit(node->array);
    visit(node->index);

    auto array_type = (ObjectType const*)get_type(node->array);
    auto const& overloads = *array_type->lookup_funcs(String("__getitem__"));
    // TODO: handle overloads
    assert(overloads.size() == 1u);
    set_type(node, overloads[0]->rettype());
  }

  // Check if an expression is an identifier that references an imported
  // module. Returns the imported module, or null if not an imported names.
  Module const* is_imported_module_name(ast::Expr const* expr) {
    if (expr->asttype == ast::TypeTag::IdentExpr) {
      String s = ((ast::IdentExpr const*)expr)->name;
      if (not m_scope.module->is_imported(s))
        return nullptr;
      return m_modules.lookup_module(m_scope.module->filename_for_import(s));
    }
    return nullptr;
  }

  // MemberExpr        < Expr :: target:*Expr, member:String
  void visit_member_expr(ast::MemberExpr const* node) {
    // TODO: The parser can find imported name.
    Module const* imported_module = is_imported_module_name(node->target);
    if (imported_module != nullptr) {
      if (imported_module->has_func(node->member)) {
        set_type(node, imported_module->lookup_func(node->member));
      } else if (imported_module->has_variable(node->member)) {
        set_type(node, imported_module->lookup_variable(node->member));
      } else {
        printf("Error: no such member: %s\n", node->member.str().c_str());
        assert(false);
      }

    } else {

      visit(node->target);
      auto type = (ObjectType const*)get_type(node->target);
      auto orig_type = type;
      // Automatic dereferencing of pointers on field access.
      if (type->def_name() == "__builtins__.tau:Pointer") {
        type = (ObjectType const*)type->parameter(String("S"));
      }
      Type const* restype = nullptr;
      if (type->has_field(node->member)) {
        restype = type->field(node->member);
      } else {
        debug("---- %s %s\n", type->str().c_str(), node->member.str().c_str());
        if (not type->has_func(node->member)) {
          printf("Error: no such member '%s'\n", node->member.str().c_str());
          assert(false);
        }
        restype = type->lookup_funcs(node->member);
        if (orig_type->def_name() != "__builtins__.tau:Pointer")
          orig_type = (ObjectType const*)pointer_to_type(orig_type);
        m_member_call_target = orig_type;
      }
      set_type(node, restype);
    }
  }

  void visit_array_expr(ast::ArrayExpr const* node) {
    debug("visit_array_expr\n");
    (void)node;
  }

  void visit_dict_expr(ast::DictExpr const* node) {
    debug("visit_dict_expr\n");
    (void)node;
  }

  void visit_for_expr(ast::ForExpr const* node) {
    debug("visit_for_expr\n");
    (void)node;
  }

  // IfExpr            < Expr :: cond:*Expr, tstmt:*Stmt, fstmt:*Stmt
  void visit_if_expr(ast::IfExpr const* node) {
    debug("visit_if_expr\n");
    
    visit(node->cond);
    m_scope.push_scope();
    visit(node->tstmt);
    m_scope.pop_scope();

    m_scope.push_scope();
    visit(node->fstmt);
    m_scope.pop_scope();
    Type const* ttype = type_of_stmt(node->tstmt);
    Type const* ftype = type_of_stmt(node->fstmt);

    // TODO: unify these two types smarter. Should use the same code as is_subtype.
    set_type(node, (ttype == ftype) ? ttype : void_type());
  }

  void visit_pointer_to_expr(ast::PointerToExpr const* node) {
    debug("visit_pointer_to_expr\n");
    visit(node->expr);
    set_type(node, pointer_to_type(get_type(node->expr)));
  }

  void visit_dereference_expr(ast::DereferenceExpr const* node) {
    debug("visit_dereference_expr\n");
    visit(node->expr);
    auto type = (ObjectType const*)get_type(node->expr);
    set_type(node, type->parameter(String("S")));
  }

  void visit_while_expr(ast::WhileExpr const* node) {
    debug("visit_while_expr\n");
    visit(node->cond);
    m_scope.push_scope();
    visit(node->body);
    m_scope.pop_scope();
    set_type(node, void_type());
  }

  void visit_compound_stmt(ast::CompoundStmt const* node) {
    debug("visit_compound_stmt\n");
    for (auto stmt : node->stmts)
      visit(stmt);
  }

  void visit_return_stmt(ast::ReturnStmt const* node) {
    debug("visit_return_stmt %p->%p\n", node, node->expr);
    visit(node->expr);
    (void)node;
  }

  // AssignmentStmt    < Stmt :: op:String, dst:*Expr, src:*Expr
  void visit_assignment_stmt(ast::AssignmentStmt const* node) {
    debug("visit_assignment_stmt\n");
    visit(node->src);
    visit(node->dst);
    if (get_type(node->src)->str() != get_type(node->dst)->str()) {
      printf("Type error: '%s' != '%s'\n", get_type(node->src)->str().c_str(), get_type(node->dst)->str().c_str());
      assert(false);
    }
  }

  void visit_expr_stmt(ast::ExprStmt const* node) {
    debug("visit_expr_stmt\n");
    visit(node->expr);
  }

  // LetStmt           < Stmt :: name:String, type:*Type, init:*Expr
  void visit_let_stmt(ast::LetStmt const* node) {
    debug("visit_let_stmt:\n");
    assert(m_scope.can_define(node->name));

    visit(node->init);
    Type const* init_type = (node->init != nullptr) ? get_type(node->init) : nullptr;
    Type const* decl_type = (node->type != nullptr) ? get_type_from_typename(node->type) : init_type;
    assert(decl_type != nullptr);
    assert(init_type == nullptr or init_type->str() == decl_type->str());

    set_type(node, decl_type);
    m_scope.insert(node->name, decl_type);
  }

  void visit_class_decl(ast::ClassDecl const* node) {
    for (auto decl : node->decls) {
      if (decl->asttype == ast::TypeTag::FunctionDecl)
        visit(decl);
    }
  }

  void visit_function_decl(ast::FunctionDecl const* node) {
    auto type = (FunctionType const*)m_modules.type_for_ast(node);
    auto const& argtypes = type->argtypes();
    auto const& argnames = node->type->argnames;
    assert(argtypes.size() == argnames.size());
    m_scope.push_scope();

    for (unsigned i = 0; i < argnames.size(); i++)
      m_scope.insert(argnames[i], argtypes[i]);
    visit(node->body);
  }

  void visit_variable_decl(ast::VariableDecl const* node) {
    (void)node;
  }

  void visit_file(ast::File const* node) {
    m_scope.module = m_modules.lookup_module(node->filename);
    for (auto decl : node->decls) {
      visit(decl);
    }
  }

  void visit_nullptr() {
  }

};


void TypeChecker::typecheck(ast::File const* file) {
  Checker checker(m_modules, m_types);
  checker.visit(file);
}


Type const* TypeChecker::type_for(ast::Node const* node) const {
  debug("type for %p\n", node);
  auto it = m_types.find(node);
  if (it == m_types.end()) {
    std::stringstream ss;
    ast::Printer printer(ss);
    printer.visit(node);
    printf("Unhandled ast: %s\n", ss.str().c_str());
    assert(false);
  }
  assert(it->second != nullptr);
  return it->second;
}

}