#pragma once
#include "string.hh"
#include "lexer.hh"
#include <vector>
#include <map>
#include <string>

namespace taula {
namespace ast {

enum class TypeTag;


struct Node {
  TypeTag asttype;
};

struct Expr : public Node {
};

struct Stmt : public Node {
};

struct Decl : public Node {
};

struct Def : public Node {
};

struct Type : public Node {
};


#include "ast-defs.hh"


} // namespace ast

//! Parse a sequence of tokens into an AST.
class Parser {
  std::vector<ast::Node const*> m_ast;
  Lexer* m_lexer;
  String m_module_filename;
public:
  Parser(Lexer* lexer, String module_filename) :
    m_lexer(lexer), m_module_filename(module_filename) { }

  ast::File const* parse();

  std::vector<ast::Node const*> const& ast() const { return m_ast; }
};

}