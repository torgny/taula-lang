#include "type_builder.hh"
#include <sstream>
#include <assert.h>


namespace std {
  template <> struct hash<std::vector<taula::Type const*>> {
    size_t operator()(std::vector<taula::Type const*> const& params) const {
      // djb2-like hash
      uint64_t hash = 5381;
      for (auto param : params) {
        uintptr_t c = uintptr_t(param) >> 4;
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
      }
      return hash;
    }
  };
}


namespace taula {


struct ParamMap {
  std::vector<Type const*> values;
  std::unordered_map<String, unsigned> indices;

  void append(String name, Type const* value) {
    unsigned idx = values.size();
    values.push_back(value);
    assert(indices.count(name) == 0);
    indices[name] = idx;
  }

  size_t size() const {
    return indices.size();
  }

  Type const* operator[](String name) const {
    auto it = indices.find(name);
    assert(it != indices.end());
    return values[it->second];
  }
};


static int base_size(std::string const& filename, String type_name) {
  if (filename != "__builtins__.tau")
    return 0;

  if (type_name == String("int"))
    return 4;
  else if (type_name == String("Array"))
    return 16;

  // TODO: We must take alignment into consideration here. However, why isn't
  // the backend dealing with this??
  return 8;
  /*
  static std::unordered_map<std::string, int> SIZES = {{String("Pointer"), sizeof(void*)},
                                                       {String("int"), sizeof(int32_t)},
                                                       {String("bool"), sizeof(bool)},
                                                     };

  auto it = SIZES.find(type_name);
  assert(it != SIZES.end());
  return it->second;
  */
}

// A class or interface definition
class ObjectTypeDefinition {
  friend class TypeBuilder;
  friend class ObjectType;
  std::unordered_map<String, FunctionOverload> m_functions;
  std::unordered_map<String, Type const*> m_fields;
  ParamMap m_parameters;
  String m_name;
  Module const* m_module = nullptr;

  std::unordered_map<std::vector<Type const*>, Type const*> m_specializations;

  bool defined = false;

  ObjectType const* build_specialization_for(std::vector<Type const*> const& params) const {
    if (defined)
      assert(params.size() == m_parameters.size());

    auto type = new ObjectType();
    type->m_typedef = this;
    type->m_parameters = params;
    return type;
  }

public:

  // Get the specialization for this type for the given set of parameters.
  Type const* specialization_for(std::vector<Type const*> const& _params) {
    // Either values for all parameters or none has to be provided. If there
    // are none provided we use the default parameters.
    // TODO: Is it really good that Foo<> == Foo<...default parameters...>?
    auto params = (_params.size() > 0) ? _params : m_parameters.values;

    auto it = m_specializations.find(params);
    if (it != m_specializations.end())
      return it->second;
    auto typ = build_specialization_for(params);
    m_specializations[params] = typ;

    return typ;
  }

  unsigned parameter_index(String name) const {
    auto it = m_parameters.indices.find(name);
    assert(it != m_parameters.indices.end());
    return it->second;
  }

  bool has_parameter(String name) const {
    return m_parameters.indices.find(name) != m_parameters.indices.end();
  }
  
  Type const* parameter(String name) const {
    auto it = m_parameters.indices.find(name);
    assert(it != m_parameters.indices.end());
    return m_parameters.values[it->second];
  }

  bool has_field(String name) const {
    return m_fields.find(name) != m_fields.end();  
  }
  
  Type const* field(String name, std::vector<Type const*> const& /*params*/) const {
    auto it = m_fields.find(name);
    assert(it != m_fields.end());
    return it->second;
  }

  bool has_func(String name) const {
    return m_functions.find(name) != m_functions.end();
  }
  
  FunctionOverload const* lookup_funcs(String name, std::vector<Type const*> const& /*params*/) const {
    auto it = m_functions.find(name);
    if (it == m_functions.end()) {
      printf("Error: type %s has no function %s.\n", m_name.str().c_str(), name.str().c_str());
      assert(false);
    }
    return &it->second;
  }

  ParamMap const& parameters() const { return m_parameters; }

  int alloc_size(std::vector<Type const*> const& /*params*/) const {
    int size = base_size(m_module->filename(), m_name);
    for (auto field : m_fields) {
      // TODO: Check if is type variable.
      size += ((ObjectType const*)field.second)->alloc_size();
    }
    return size;
  }

};


// --- FunctionOverload ---

std::string FunctionOverload::str() const {
  std::stringstream ss;
  ss << "[";
  for (auto fn : m_overloads)
    ss << fn->str();
  ss << "]";
  return ss.str();
}

Type const* FunctionOverload::substitute(ParamMap const&) const {
  assert(false);
}

// --- FunctionType ---

Type const* FunctionType::substitute(ParamMap const& params) const {
  FunctionType* functype = new FunctionType();
  functype->m_body = m_body;
  functype->m_rettype = m_rettype->substitute(params);
  functype->m_argnames = m_argnames;
  functype->m_module = m_module;
  for (auto argtype : m_argtypes) {
    functype->m_argtypes.push_back(argtype->substitute(params));
  }
  return functype;
}



// --- ObjectType ---

std::string ObjectType::str() const {
  std::stringstream ss;

  ss << m_typedef->m_module->filename() << ":" << m_typedef->m_name.str();
  if (m_typedef->m_parameters.indices.size() > 0) {
    ss << "<";
    int size = m_parameters.size();
    int count = 0;
    for (auto param : m_parameters) {
      if (param != nullptr)
        ss << param->str();
      else
        ss << m_typedef->m_parameters.values[count];
      if (count < size - 1) ss << ", ";
      count++;
    }
    ss << ">";
  }
  return ss.str();
}

std::string ObjectType::def_name() const {
  return m_typedef->m_module->filename() + ":" + m_typedef->m_name.str();
}

Module const* ObjectType::module() const {
  return m_typedef->m_module;
}

int ObjectType::alloc_size() const {
  return m_typedef->alloc_size(m_parameters);
}

std::unordered_map<String, Type const*> const& ObjectType::fields() const {
  return m_typedef->m_fields;
}

  bool ObjectType::has_func(String name) const {
  return m_typedef->has_func(name);
}

FunctionOverload const* ObjectType::lookup_funcs(String name) const {
  auto it = m_functions.find(name);
  if (it != m_functions.end())
    return &it->second;

  FunctionOverload const& generic_funcs = *m_typedef->lookup_funcs(name, m_parameters);
  FunctionOverload& specialized_funcs = const_cast<ObjectType*>(this)->m_functions[name];

  ParamMap params = m_typedef->parameters();
  params.values = m_parameters;
  for (unsigned i = 0; i < generic_funcs.size(); i++) {
    specialized_funcs.m_overloads.push_back((FunctionType const*)generic_funcs[i]->substitute(params));
  }
  return &specialized_funcs;
}

bool ObjectType::has_field(String name) const {
  return m_typedef->has_field(name);  
}

Type const* ObjectType::field(String name) const {
  return m_typedef->field(name, m_parameters);
}

bool ObjectType::has_parameter(String name) const {
  return m_typedef->has_parameter(name);  
}

Type const* ObjectType::parameter(String name) const {
  assert(m_parameters.size() == m_typedef->m_parameters.size());
  unsigned i = m_typedef->parameter_index(name);
  Type const* value = m_parameters[i];
  if (value != nullptr)
    return value;
  return m_typedef->parameter(name);
}


Type const* ObjectType::substitute(ParamMap const& params) const {
  if (m_typedef->m_parameters.indices == params.indices) {
    auto t = new ObjectType();
    t->m_parameters = params.values;
    t->m_typedef = m_typedef;
    return t;
  }
  return this;
}


// --- TypeVariable ---

// Substitute type parameters with actual types.
Type const* TypeVariable::substitute(ParamMap const& params) const {
  return params[m_name];
}


// --- Module ---

FunctionOverload const* Module::lookup_func(String name) const {
  auto it = m_functions.find(name);
  if (it == m_functions.end()) {
    printf("error: module %s has no function '%s'.\n", m_filename.str().c_str(), name.str().c_str());
    assert(false);
  }
  return &it->second;
}

String Module::filename_for_import(String name) const {
  auto it = m_filename_for_import.find(name);
  assert(it != m_filename_for_import.end());
  return it->second;
}

Type const* Module::lookup_variable(String name) const {
  auto it = m_variables.find(name);
  assert(it != m_variables.end());
  return it->second;
}


// --- TypeBuilder ---

Module* TypeBuilder::module_for(String name, FileForNamespace const& imported_modules) {
  auto filename_it = imported_modules.find(name);
  assert(filename_it != imported_modules.end());
  auto filename = filename_it->second;
  auto it = m_modules.find(filename);

  if (it == m_modules.end()) {
    // We haven't loaded this module yet. We instantiate it's object here,
    // such that we always refer to the same object whenever we referring to a
    // module.
    auto module = new Module();
    module->m_filename = filename;
    m_modules[filename] = module;
    return module;
  }

  // The module is either loaded or has been imported by another module before.
  return it->second;  
}

ObjectTypeDefinition* TypeBuilder::typedef_for(Module* module, String type_name) {
  auto it = module->m_typedefs.find(type_name);
  if (it == module->m_typedefs.end()) {
    // This is the first time we see this type definition.
    auto typdef = new ObjectTypeDefinition();
    typdef->m_name = type_name;
    typdef->m_module = module;
    module->m_typedefs[type_name] = typdef;
    return typdef;
  } else {
    return (ObjectTypeDefinition*)it->second;
  }
}


Type const* TypeBuilder::type_for(Module* module, ast::Type const* type) {
  if (type->asttype == ast::TypeTag::TypeVariable) {
    auto typevar = (ast::TypeVariable const*)type;
    auto t = new TypeVariable();
    t->m_name = typevar->name;
    return t;
  }

  assert(type->asttype == ast::TypeTag::Typename);
  auto typname = (ast::Typename const*)type;
  String name = ((ast::Typename const*)type)->name;

  Module* typedef_module = module_for(typname->module, module->m_filename_for_import);
  ObjectTypeDefinition* typdef = typedef_for(typedef_module, name);

  std::vector<Type const*> parameters;
  for (ast::Type const* param : typname->params) {
    parameters.push_back(type_for(module, param));
  }
  return typdef->specialization_for(parameters);
}


FunctionType const* TypeBuilder::build_function(Module* module, ast::FunctionDecl const* fn) {
  auto ast = fn->type;
  Type const* rettype = type_for(module, ast->rettype);

  std::vector<Type const*> argtypes;
  for (ast::Type const* arg : ast->argtypes) {
    argtypes.push_back(type_for(module, arg));
  }

  auto type = new FunctionType();
  type->m_rettype = rettype;
  type->m_argtypes = argtypes;
  type->m_argnames = ast->argnames;
  type->m_body = fn->body;
  type->m_module = module;
  return type;
}


void TypeBuilder::build_object_type(Module* module, String type_name,
                                    std::vector<ast::Decl const*> const& members,
                                    std::vector<ast::TypeVariableDecl const*> const& parameters) {

  ObjectTypeDefinition* typdef = typedef_for(module, type_name);
  typdef->defined = true;

  for (ast::TypeVariableDecl const* param : parameters) {
    typdef->m_parameters.append(param->name, type_for(module, param->interface));
  }

  for (ast::Decl const* decl : members) {
    switch (decl->asttype) {
      case ast::TypeTag::FunctionDecl: {
        auto fn = (ast::FunctionDecl const*)decl;
        auto fn_type = build_function(module, fn);
        typdef->m_functions[fn->name].m_overloads.push_back(fn_type);
        m_type_for_ast[fn] = fn_type;
        break;
      }
      case ast::TypeTag::VariableDecl: {
        auto var = (ast::VariableDecl const*)decl;
        assert(typdef->m_fields.find(var->name) == typdef->m_fields.end());
        typdef->m_fields[var->name] = type_for(module, var->type);
        break;
      }
      default: assert(false);
    }
  }
}


// Given the filename of a module, get the name of the namespace in which it's
// symbols will be stored when the module is imported. This is simply the
// filename without suffix, e.g., the namespace for "foo/bar/baz.tau" is
// "baz".
// TODO: This should be moved to the parser, as there should be a way to
// override this default behavior, e.g., by doing 'import "bar/baz.tau" as foo'
static String namespace_name(String filename) {
  String suffix = filename.lskip(filename.length() - 4);
  assert(suffix == String(".tau"));

  String prefix = filename.prefix(filename.length() - 4);
  for (int i = prefix.length(); i >= 0; i--) {
    if (prefix[i] == '/')
      return prefix.lskip(i + 1);
  }

  // No path separator found -- the module name is the prefix.
  return prefix;
}

void TypeBuilder::build_file(ast::File const* file) {
  // happens when a module is imported before it's been loaded.
  // Create a new module object unless we've already done it. This
  auto module_it = m_modules.find(file->filename);
  Module* module = nullptr;
  if (module_it == m_modules.end()) {
    module = new Module();
    module->m_filename = file->filename;
    m_modules[file->filename] = module;
  } else {
    module = m_modules[file->filename];
  }
  assert(module != nullptr);
  assert(module->m_filename_for_import.count(String("__builtins__")) == 0);
  module->m_filename_for_import[String("__builtins__")] = String("__builtins__.tau");
  module->m_filename_for_import[file->short_name] = file->filename;

  for (ast::Decl const* decl : file->decls) {
    switch (decl->asttype) {
      case ast::TypeTag::ImportDecl: {
        auto import = (ast::ImportDecl const*)decl;
        module->m_filename_for_import[namespace_name(import->name)] = import->name;
        break;
      }
      case ast::TypeTag::ClassDecl: {
        auto cls = (ast::ClassDecl const*)decl;
        build_object_type(module, cls->name, cls->decls, cls->params);
        break;
      }
      case ast::TypeTag::InterfaceDecl: {
        auto ifc = (ast::InterfaceDecl const*)decl;
        build_object_type(module, ifc->name, ifc->decls, {});
        break;
      }
      case ast::TypeTag::FunctionDecl: {
        auto fn = (ast::FunctionDecl const*)decl;
        auto fn_type = build_function(module, fn);
        module->m_functions[fn->name].m_overloads.push_back(fn_type);
        m_type_for_ast[fn] = fn_type;
        break;
      }
      case ast::TypeTag::VariableDecl: {
        auto var = (ast::VariableDecl const*)decl;
        assert(var->type != nullptr && "Global variables must have type declaration.");
        module->m_variables[var->name] = type_for(module, var->type);
        break;
      }
      default:
        assert(false && "Unhandled declaration type.");
    }
  }
}


ObjectType const* TypeBuilder::lookup_type(String module_filename, String type_name, std::vector<Type const*> const& params) const {
  Module const* module = lookup_module(module_filename);
  auto it_typedef = module->m_typedefs.find(type_name);
  if (it_typedef == module->m_typedefs.end()) {
    printf("Error: %s:%s does not exists.\n", module_filename.str().c_str(), type_name.str().c_str());
    assert(false);
  }
  assert(it_typedef->second != nullptr);
  return (ObjectType const*)((ObjectTypeDefinition*)it_typedef->second)->specialization_for(params);
}


FunctionOverload const* TypeBuilder::lookup_func(String module_filename, String func_name) const {
  Module const* module = lookup_module(module_filename);
  return module->lookup_func(func_name);
}


Module const* TypeBuilder::lookup_module(String module_filename) const {
  auto it = m_modules.find(module_filename);
  if (it == m_modules.end()) {
    printf("No such module %s. Candidates:\n", module_filename.str().c_str());
    for (auto it : m_modules) {
      printf("  %s\n", it.first.str().c_str());
    }
    assert(false);
  }
  return it->second;
}


Type const* TypeBuilder::type_for_ast(ast::Node const* ast) const {
  auto it = m_type_for_ast.find(ast);
  assert(it != m_type_for_ast.end());
  return it->second;
}

}