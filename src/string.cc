#include "string.hh"
#include <string.h>
#include <assert.h>
#include <algorithm>
#include <vector>

namespace taula {



class GarbageCollector {
  std::vector<const char*> m_ptrs;
public:
  void collect() {
    for (auto ptr : m_ptrs)
      delete [] ptr;
    m_ptrs.clear();
  }

  void register_pointer(const char* ptr) {
    m_ptrs.push_back(ptr);
  }
};

static GarbageCollector* GC = new GarbageCollector();

void String::gc() {
  GC->collect();
}

static char* gc_new(size_t size) {
  char* data = new char[size];
  GC->register_pointer(data);
  return data;
}



String::String(const char* begin, uint32_t length, uint32_t tag) :
  m_begin(begin), m_length(length), m_tag(tag) { }


String::String(uint32_t tag) :
  m_begin(NULL), m_length(0), m_tag(tag) { }


String::String(const char* s, uint32_t tag) :
  m_begin(s), m_length(strlen(s)), m_tag(tag) { }


String::String(const char* begin, const char* end, uint32_t tag) :
  m_begin(begin), m_length(std::max(0, int(end - begin))), m_tag(tag) { }


String::String(std::string const& s, uint32_t tag) :
  m_length(s.length()), m_tag(tag)
{
  m_begin = gc_new(m_length);
  memcpy(const_cast<char*>(m_begin), s.c_str(), length());
}


String::String(String const& s, uint32_t tag) :
  m_begin(s.m_begin), m_length(s.m_length), m_tag(tag)
{
}


String::String(String const& s) : String(s, s.m_tag) { }


String& String::operator=(String const& s) {
  if (this == &s)
    return *this;

  m_begin = s.m_begin;
  m_length = s.m_length;
  m_tag = s.m_tag;

  return *this;
}

char String::operator[](int i) const {
  return m_begin[i];
}

bool String::operator==(String const& s) const {
  if (length() != s.length())
    return false;
  if ((0 == memcmp(m_begin, s.m_begin, length())))
    return true;
  return false;
}


bool String::operator!=(String const& rhs) const {
  return !(*this == rhs);
}


String String::operator+(String const& rhs) const {
  if (m_begin == NULL)
    return rhs;
  if (&m_begin[length()] == rhs.m_begin)
    return String(m_begin, &m_begin[length() + rhs.length()], m_tag | rhs.m_tag);

  //printf("WARNING: dynamic concat: %p + %p!\n", m_begin, rhs.m_begin);
  size_t len = length() + rhs.length();
  char* begin = gc_new(len);
  memcpy(begin, m_begin, length());
  memcpy(&begin[length()], rhs.m_begin, rhs.length());
  return String(begin, len, m_tag | rhs.m_tag);
}


String String::lskip(size_t num) const {
  return String(&m_begin[num], m_length - num, m_tag);
}

String String::prefix(size_t num) const {
  return String(m_begin, num, m_tag);
}

std::string String::str() const {
  if (m_begin == NULL)
    return "";
  return std::string(m_begin, &m_begin[length()]);
}


size_t String::length() const {
  return m_length;
}


uint32_t String::tag() const {
  return m_tag;
}


const char* String::begin() const {
  return m_begin;
}


const char* String::end() const {
  return &m_begin[length()];
}


uint64_t String::hash() const {
  // djb2 hash
  uint64_t hash = 5381;
  for (unsigned i = 0; i < length(); i++) {
    int c = m_begin[i];
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
  }
  return hash;
}

std::ostream& operator<<(std::ostream& os, String const& s) {
  return os << s.str();
}





}