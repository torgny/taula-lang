;# This is a 64-bit platform. The stack grows downwards. Parameters to functions
;# are passed in the registers rdi, rsi, rdx, rcx, r8, r9, and further values are
;# passed on the stack in reverse order. Parameters passed on the stack may be
;# modified by the called function. Functions are called using the call
;# instruction that pushes the address of the next instruction to the stack and
;# jumps to the operand. Functions return to the caller using the ret instruction
;# that pops a value from the stack and jump to it. The stack is 16-byte aligned
;# just before the call instruction is called.
;#
;# Functions preserve the registers rbx, rsp, rbp, r12, r13, r14, and 15; while
;# rax, rdi, rsi, rdx, rcx, r8, r9, r10, r11 are scratch registers. The return
;# value is stored in the rax register, or if it is a 128-bit value, then the
;# higher 64-bits go in rdx. Optionally, functions push rbp such that the caller-
;# return-rip is 8 bytes above it, and set erp to the address of the saved rbp.
;# This allows iterating through the existing stack frames.
;#
;# Signal handlers are executed on the same stack, but 128 bytes known as the red
;# zone is subtracted from the stack before anything is pushed to the stack. This
;# allows small leaf functions to use 128 bytes of stack space without reserving
;# stack space by subtracting from the stack pointer. The red zone is well-known
;# to cause problems for x86-64 kernel developers, as the CPU itself doesn't
;# respect the red zone when calling interrupt handlers. This leads to a subtle
;# kernel breakage as the ABI contradicts the CPU behavior. The solution is to
;# build all kernel code with -mno-red-zone or by handling interrupts in kernel
;# mode on another stack than the current (and thus implementing the ABI).

.intel_syntax

;# void interpret(Instruction* instrs, uint64_t* const_pool);
.global interpret
interpret:
  # Allocate space on stack for virtual registers.
  mov   %rax, %rsp              # Temporarily save stack pointer
  mov   %edx, DWORD PTR [%rdi]  # Load number of virtual registers
  neg   %rdx                    # Need to negate because stack grows towards small addresses
  lea   %rsp, [%rsp + %rdx * 8] # Perform the actual allocation
  push  %rax                    # Push old stack pointer on top of virtual registers

  add   %rdi, 4                 # Set PC to start of function code

  # Push callee saved registers
  push %rbx
  push %rbp
  push %r12
  push %r13
  push %r14
  push %r15

  mov %rdx, i_nop

  ;#mov %r8, %rdi    # instrs
  ;#mov %r9, %rsi    # const_pool

  ;# Load instruction word. opcode=bl, opa=bh, opb=al, opc=ah
  mov    %ebx, DWORD PTR [%rdi]  # Load instruction word
  mov    %eax, %ebx
  shr    %eax, 16

  movzx  %edx, %al
  movzx  %ecx, %ah

  movzx  %edx, %bl
  shl    %edx, 5
  add    %edx, %edx
  mov    %rax, %rdx
  jmp    %rax

  ;#mov    %r15, 0x64
  ;#mov    [%rsp + %rdx * 8], %r15
  ;#mov    %rax, [%rsp + %rdx * 8]



;#  mov %eax, DWORD PTR [%rdi]
;#  mov %rax, %rsp


  pop %r15
  pop %r14
  pop %r13
  pop %r12
  pop %rbp
  pop %rbx
  pop %rdx
  mov %rsp, %rdx

  ret


.align 32
instr_start:
i_nop:
  mov    %ebx, DWORD PTR [%rdi]  # Load instruction word
  mov    %eax, %ebx
  shr    %eax, 16

  movzx  %edx, %al
  movzx  %ecx, %ah

  mov    %r15, 0x64
  mov    [%rsp + %rdx * 8], %r15
  mov    %rax, [%rsp + %rdx * 8]

.align 32
i_add:
  ret
  mov    %ebx, DWORD PTR [%rdi]  # Load instruction word
  mov    %eax, %ebx
  shr    %eax, 16

  movzx  %edx, %al
  movzx  %ecx, %ah

  mov    %r15, 0x64
  mov    [%rsp + %rdx * 8], %r15
  mov    %rax, [%rsp + %rdx * 8]
