import re

# Expressions are constructs that stand for values. Declarations are
# constructs that introduce identifiers. Statements are constructs that
# perform basic actions or control the execution of the program.

defn = """

StringExpr        < Expr :: str:String

NumberExpr        < Expr :: num:String

FpNumberExpr      < Expr :: num:String

IdentExpr         < Expr :: name:String

BinaryExpr        < Expr :: op:String, lhs:*Expr, rhs:*Expr

UnaryExpr         < Expr :: op:String, expr:*Expr

FunctionExpr      < Expr :: type:*FunctionType, body:*Stmt

FunctionCallExpr  < Expr :: func:*Expr, args:[*Expr]

SubscriptExpr     < Expr :: array:*Expr, index:*Expr

MemberExpr        < Expr :: target:*Expr, member:String

MatchExpr         < Expr :: op:String, dst:*Expr, src:*Expr

ArrayExpr         < Expr :: elems:[*Expr]

DictExpr          < Expr :: keys:[*Expr], values:[*Expr]

ForExpr           < Expr :: vars:[String], seq:*Expr, body:*Stmt

IfExpr            < Expr :: cond:*Expr, tstmt:*Stmt, fstmt:*Stmt

WhileExpr         < Expr :: cond:*Expr, body:*Stmt

NewExpr           < Expr :: type:*Type, args:[*Expr], lifetime:*Expr

CastExpr          < Expr :: type:*Type, arg:*Expr

PointerToExpr     < Expr :: expr:*Expr

DereferenceExpr   < Expr :: expr:*Expr

CompoundStmt      < Stmt :: stmts:[*Stmt]

PassStmt          < Stmt ::

ReturnStmt        < Stmt :: expr:*Expr

AssignmentStmt    < Stmt :: op:String, dst:*Expr, src:*Expr

ExprStmt          < Stmt :: expr:*Expr

LetStmt           < Stmt :: name:String, type:*Type, init:*Expr

Typename          < Type :: module:String, name:String, params:[*Type]

TypeVariable      < Type :: name:String, params:[*Type]

FunctionType      < Type :: rettype:*Type, argnames:[String], argtypes:[*Type]

TypeVariableDecl         :: name:String, interface:*Type

ImportDecl        < Decl :: name:String

ClassDecl         < Decl :: name:String, decorators:[*Expr], params:[*TypeVariableDecl], decls:[*Decl]

InterfaceDecl     < Decl :: name:String, decorators:[*Expr], params:[*TypeVariableDecl], decls:[*Decl]

FunctionDecl      < Decl :: name:String, decorators:[*Expr], type:*FunctionType, body:*Stmt

VariableDecl      < Decl :: name:String, type:*Type, init:*Expr

File                     :: filename:String, short_name:String, decls:[*Decl]

"""

def snake_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

def member_name(mem):
  if ':' in mem:
      return mem.split(':')[0].replace('[', '').replace('*', '')
  for x in "[]*":
      mem = mem.replace(x, '')
  return snake_case(mem)

def member_type(mem):
  if ':' in mem:
      return member_type(mem.split(':')[1])
  if mem[0] == '*':
      if mem[1].islower():
          return "Node const*"
      return "%s const*" % member_type(mem[1:])
  if mem[0] == '[':
      return "std::vector<%s>" % member_type(mem[1:-1])
  if mem[0].islower():
      return "String"
  return mem

def emit_dump(defname, members):
  #args = ['", %s=" << %s' % (name, name) for name, _type in members]
  #os = ["os", '"%s("' % defname] + args
  #print "  void dump(std::ostream& os) const {"
  #print '    %s;' % " << ".join(os)
  #print "}"
  pass

def emit_class(defname, members, supername):
    inherit = "Node"
    if supername is not None:
        inherit = supername

    print "struct %s : public %s {" % (defname, inherit)
    for name, type in members:
        print "  %s const %s;" % (type, name)
#    print "public:"
    args = ["%s const& %s" % (type, name) for name, type in members]
    inits = ["%s(%s)" % (name, name) for name, _type in members]
    colon = " : " if len(inits) > 0 else ""
    print "  %s(%s)%s%s { asttype = TypeTag::%s; }" % (defname, ", ".join(args), colon, ", ".join(inits), defname)
    emit_dump(defname, members)
    print "};"
    print


def emit(descr):
  classes = []
  for line in descr.split('\n'):
      line = line.strip()
      if not line:
          continue
      lhs, rhs = line.split('::')
      name = lhs.strip()
      supername = None
      if "<"  in name:
          name, supername = name.split("<")
          name = name.strip()
          supername = supername.strip()

      members = []
      if rhs.strip():
        for tok in rhs.split(','):
            tok = tok.strip()
            members.append((member_name(tok), member_type(tok)))
      classes.append((name, supername, members))

  print "enum class TypeTag {"
  for cls, _supername, _members in classes:
      print "  %s," % cls
  print "};"
  print

  for cls, _supername, _members in classes:
      print "class %s;" % cls
  print

  for cls, supername, members in classes:
      emit_class(cls, members, supername)

  print "class Visitor {"
  print "public:"
  print "  void visit(Node const* node) {"
  print "    if (node == nullptr) { visit_nullptr(); return; }"
  print "    switch (node->asttype) {"
  for cls, _supername, _members in classes:
      print "    case TypeTag::%s: visit_%s((%s*)node); return;" % (cls, snake_case(cls), cls)
  print "    }"
  print "  }"
  for cls, _supername, _members in classes:
      print "  virtual void visit_%s(%s const*) { }" % (snake_case(cls), cls)
  print "  virtual void visit_nullptr() { }"
  print "};"
  print


  def visit_code(name, type):
      if type == "String":
          return "os << %s;" % name
      elif type.startswith("std::vector<"):
          return 'os << "["; for (unsigned i = 0; i < %s.size(); i++) { %s if (i != %s.size() - 1) os << ", "; } os << "]";' % (name, visit_code(name + "[i]", type[12:-1]), name)
      else:
          return "visit(%s);" % name


  print "class Printer : public Visitor {"
  print "  std::ostream& os;"
  print "public:"
  print "  Printer(std::ostream& os) : os(os) { }"
  for cls, _supername, members in classes:
      print "  void visit_%s(%s const* node) {" % (snake_case(cls), cls)
      print "    (void)node;"
      print '    os << "%s(";' % cls
      for idx, (name, type) in enumerate(members):
          print "    %s" % visit_code("node->" + name, type)
          if idx < len(members) - 1:
              print '    os << ", ";'
      print '    os << ")";'
      print "  }"
  print '  virtual void visit_nullptr() { os << "null"; }'
  print "};"

emit(defn)