#include "codegen.hh"

#include <libgccjit++.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unordered_map>
#include <string>
#include <algorithm>


#define debug(...) //printf(__VA_ARGS__)


namespace taula {


/*

libgccjit is an awesome, but:
  - given a gccjit::struct_ should be possible to get type of a named field.
  - should be possible to declare a function without providing argument names.
  - getting the size of a types

 */

class CodeGeneratorHelper : public ast::Visitor {
  TypeBuilder const& tb;
  TypeChecker const& tc;

  gccjit::block m_block;
  bool m_block_ended = false;
  gccjit::rvalue m_expr;
  gccjit::function m_func;

  gccjit::rvalue m_extra_arg;
  bool m_extra_arg_is_pointer = false;

  ast::File const* m_file = nullptr;

  gccjit::function m_zeroinit_func;
  gccjit::function m_alloc_func;

  struct {
    gccjit::type type;
    gccjit::field length_field;
    gccjit::field ptr_field;
  } m_array;
  gccjit::function m_len_function;
  gccjit::function m_append_function;

  
  std::unordered_map<std::string, gccjit::lvalue> m_locals;
  std::unordered_map<std::string, gccjit::function> m_functions;
  std::unordered_map<std::string, gccjit::lvalue> m_globals;
  std::unordered_map<void*, std::unordered_map<std::string, gccjit::lvalue>> m_params_for_functions;
  std::unordered_map<std::string, gccjit::type> m_types;
  std::unordered_map<std::string, std::unordered_map<String, gccjit::field>> m_struct_fields;


  bool is_pointer_type(Type const* type) {
    auto ot = (ObjectType const*)type;
    return ot->def_name() == "__builtins__.tau:Pointer";
  }

  bool is_array_type(Type const* type) {
    auto ot = (ObjectType const*)type;
    return ot->def_name() == "__builtins__.tau:Array";    
  }

  gccjit::type get_const(gccjit::type type) {
    return gccjit::type(gcc_jit_type_get_const(type.get_inner_type()));
  }


  gccjit::type type_for(Type const* type) {
    debug("looking up type: %s\n", type->str().c_str());
    auto it = m_types.find(type->str());
    if (it == m_types.end()) {
      if (is_array_type(type)) {
        return m_array.type;
      } else if (is_pointer_type(type)) {
        auto ot = (ObjectType const*)type;
        auto jittype = type_for(ot->parameter(String("S"))).get_pointer();
        m_types[type->str()] = jittype;
        return jittype;
      } else {
        debug("Unhandled type: %s\n", type->str().c_str());
        assert(false && "Unhandled type");
      }
    }
    return it->second;
  }

  int m_annon_counter = 0;
  gccjit::lvalue new_anon_local(gccjit::type type) {
    char str[16];
    sprintf(str, "$%d", m_annon_counter);
    return m_func.new_local(type, str);
  }


  // TODO: is this safe?
  gccjit::lvalue as_lvalue(gccjit::rvalue value) {
    auto inner = (gcc_jit_lvalue*)value.get_inner_rvalue();
    return gccjit::lvalue(inner);
  }

  // TODO: is this safe?
  gccjit::function as_function(gccjit::rvalue value) {
    auto inner = (gcc_jit_function*)value.get_inner_rvalue();
    return gccjit::function(inner);
  }


  gccjit::lvalue get_global(std::string const& namespace_name, String global_name, ObjectType const* type) {
    std::string fullname = namespace_name + "_" + global_name.str();
    std::replace(fullname.begin(), fullname.end(), '/', '_');
    std::replace(fullname.begin(), fullname.end(), '.', '_');
    std::replace(fullname.begin(), fullname.end(), ':', '_');    

    auto it = m_globals.find(fullname);
    if (it != m_globals.end())
      return it->second;
    auto global = ctxt.new_global(GCC_JIT_GLOBAL_INTERNAL, type_for(type), fullname);
    m_globals[fullname] = global;
    return global;
  }

  gccjit::function get_function(std::string const& namespace_name, String funcname,
                                FunctionType const* func_type, bool is_main_function=false) {
    std::string fullname = namespace_name + "_" + funcname.str();

    if (namespace_name.find("__builtins__.tau:Array<") == 0 and funcname == String("len")) {
      return m_len_function;
    }

    std::replace(fullname.begin(), fullname.end(), '/', '_');
    std::replace(fullname.begin(), fullname.end(), '.', '_');
    std::replace(fullname.begin(), fullname.end(), ':', '_');

    auto it = m_functions.find(fullname);
    if (it != m_functions.end())
      return it->second;

    std::unordered_map<std::string, gccjit::lvalue> params_map;
    std::vector<gccjit::param> params;
    for (unsigned i = 0; i < func_type->argnames().size(); i++) {
      auto name = func_type->argnames()[i];
      auto param = ctxt.new_param(type_for(func_type->argtypes()[i]), name.str());
      params_map[name.str()] = param;
      params.push_back(param);
    }

    std::string jitname = fullname;
    if (is_main_function)
      jitname = "main";

    auto func = ctxt.new_function(GCC_JIT_FUNCTION_EXPORTED,
                                  type_for(func_type->rettype()),
                                  jitname,
                                  params,
                                  0);
    m_params_for_functions[func.get_inner_function()] = params_map;
    m_functions[fullname] = func;
    return func;
  }

public:
  gccjit::context ctxt;
  gcc_jit_result* result = nullptr;

  CodeGeneratorHelper(TypeBuilder const& tb, TypeChecker const& tc) : tb(tb), tc(tc) {
    ctxt = gccjit::context::acquire();

    // We might generate unreachable basic blocks, so let the backend deal with them.
    ctxt.set_bool_allow_unreachable_blocks(true);

    /* Set some options on the context.
       Turn this on to see the code being generated, in assembler form.  */
    ctxt.set_bool_option(GCC_JIT_BOOL_OPTION_DUMP_GENERATED_CODE, 0);

    m_types["__builtins__.tau:uint8"] = ctxt.get_type(GCC_JIT_TYPE_UNSIGNED_CHAR);
    m_types["__builtins__.tau:int"] = ctxt.get_type(GCC_JIT_TYPE_INT);
    m_types["__builtins__.tau:void"] = ctxt.get_type(GCC_JIT_TYPE_VOID);
    m_types["__builtins__.tau:float"] = ctxt.get_type(GCC_JIT_TYPE_FLOAT);
    m_types["__builtins__.tau:bool"] = ctxt.get_type(GCC_JIT_TYPE_BOOL);
    m_types["__builtins__.tau:str"] = ctxt.get_type(GCC_JIT_TYPE_CONST_CHAR_PTR);


    auto const_char_p = ctxt.new_rvalue("").get_type();
    {
      std::vector<gccjit::param> params = { ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_VOID_PTR), "ptr"),
                                            ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "size") };
      m_zeroinit_func = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                       ctxt.get_type(GCC_JIT_TYPE_VOID),
                                       "zeroinit",
                                       params,
                                       0);
    }
    {
      std::vector<gccjit::param> params;
      params.push_back(ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "size"));
      m_alloc_func = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                       ctxt.get_type(GCC_JIT_TYPE_VOID_PTR),
                                       "malloc_zeroinit",
                                       params,
                                       0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(const_char_p, "s") };
      m_functions["__builtins___tau_print"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                ctxt.get_type(GCC_JIT_TYPE_VOID),
                                                                "print_str",
                                                                params,
                                                                0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "i") };
      m_functions["__builtins___tau_itos"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                               const_char_p,
                                                               "itos",
                                                               params,
                                                               0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_BOOL), "b") };
      m_functions["__builtins___tau_btos"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                               const_char_p,
                                                               "btos",
                                                               params,
                                                               0);
    }
    {
      std::vector<gccjit::param> params;
      m_functions["__builtins___tau_null_str"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                   const_char_p,
                                                                   "null_str",
                                                                   params,
                                                                   0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(const_char_p, "s") };
      m_functions["__builtins___tau_str_hash"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                   ctxt.get_type(GCC_JIT_TYPE_INT),
                                                                   "str_hash",
                                                                   params,
                                                                   0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(const_char_p, "rhs"), ctxt.new_param(const_char_p, "rhs") };
      m_functions["__builtins___tau_str___eq__"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                     ctxt.get_type(GCC_JIT_TYPE_BOOL),
                                                                     "str_eq",
                                                                     params,
                                                                     0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(const_char_p.get_pointer(), "s"), ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "chars") };
      m_functions["__builtins___tau_str_skip"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                   const_char_p,
                                                                   "str_skip",
                                                                   params,
                                                                   0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(const_char_p.get_pointer(), "s"),
                                            ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "begin"),
                                            ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "end") };
      m_functions["__builtins___tau_str_substr"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                     const_char_p,
                                                                     "str_substr",
                                                                     params,
                                                                     0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(const_char_p.get_pointer(), "s") };
      m_functions["__builtins___tau_str_len"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                  ctxt.get_type(GCC_JIT_TYPE_INT),
                                                                  "str_len",
                                                                  params,
                                                                  0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "i") };
      m_functions["__builtins___tau_u8"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                  ctxt.get_type(GCC_JIT_TYPE_UNSIGNED_CHAR),
                                                                  "u8",
                                                                  params,
                                                                  0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(const_char_p.get_pointer(), "s"),
                                            ctxt.new_param(const_char_p, "prefix"),
                                            ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "begin") };
      m_functions["__builtins___tau_str_startswith"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                         ctxt.get_type(GCC_JIT_TYPE_BOOL),
                                                                         "str_startswith",
                                                                         params,
                                                                         0);
    }
    {
      m_array.length_field = ctxt.new_field(ctxt.get_type(GCC_JIT_TYPE_INT), "length");
      m_array.ptr_field = ctxt.new_field(ctxt.get_type(GCC_JIT_TYPE_VOID_PTR), "ptr");
      std::vector<gccjit::field> fields = { m_array.length_field, m_array.ptr_field };
      m_array.type = ctxt.new_struct_type("__builtins___tau_Array", fields);
    }
    {
      std::vector<gccjit::param> params = {ctxt.new_param(m_array.type.get_pointer(), "array")};
      m_len_function = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                         ctxt.get_type(GCC_JIT_TYPE_INT),
                                         "len",
                                         params,
                                         0);
    }
    {
      std::vector<gccjit::param> params = {ctxt.new_param(m_array.type.get_pointer(), "array"),
                                           ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_INT), "elem_size"),
                                           ctxt.new_param(ctxt.get_type(GCC_JIT_TYPE_VOID_PTR), "elem") };
      m_append_function = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                            ctxt.get_type(GCC_JIT_TYPE_VOID),
                                            "array_append",
                                            params,
                                            0);
    }
    {
      std::vector<gccjit::param> params = { ctxt.new_param(m_array.type, "arr") };
      m_functions["__builtins___tau_arr2str"] = ctxt.new_function(GCC_JIT_FUNCTION_IMPORTED,
                                                                  const_char_p,
                                                                  "arr2str",
                                                                  params,
                                                                  0);
    }
  }

  virtual ~CodeGeneratorHelper() { }

  gccjit::rvalue visit(ast::Node const* node) {
    ast::Visitor::visit(node);
    return m_expr;
  }

  // CastExpr           < Expr :: type:*Type, arg:*Expr
  void visit_cast_expr(ast::CastExpr const* node) {
    auto expr = visit(node->arg);
    m_expr = ctxt.new_cast(expr, type_for(tc.type_for(node)));
  }

  // NewExpr           < Expr :: type:*Type, args:[*Expr]
  void visit_new_expr(ast::NewExpr const* node) {
    debug("visit_new_expr\n");
    auto type = (ObjectType const*)tc.type_for(node);
    if (type->def_name() == "__builtins__.tau:Array") {
      auto elem_type = (ObjectType const*)type->parameter(String("T"));

      assert(node->args.size() == 1u);  
      auto num_elems = visit(node->args[0]);
      auto elem_size = ctxt.new_rvalue(ctxt.get_type(GCC_JIT_TYPE_INT), elem_type->alloc_size());
      auto alloc_size = ctxt.new_binary_op(GCC_JIT_BINARY_OP_MULT, ctxt.get_type(GCC_JIT_TYPE_INT), num_elems, elem_size);
      auto void_ptr = ctxt.new_call(m_alloc_func, {alloc_size});

      auto local = new_anon_local(m_array.type);
      m_block.add_assignment(local.access_field(m_array.length_field), num_elems);
      m_block.add_assignment(local.access_field(m_array.ptr_field), void_ptr);

      m_expr = local;

    } else if (type->def_name() == "__builtins__.tau:Pointer") {
      auto alloc_type = (ObjectType const*)type->parameter(String("S"));
      gccjit::type int_type = ctxt.get_type(GCC_JIT_TYPE_INT);
      std::vector<gccjit::rvalue> args = { ctxt.new_rvalue(int_type, alloc_type->alloc_size()) };
      auto void_ptr = ctxt.new_call(m_alloc_func, args);
      auto result = ctxt.new_cast(void_ptr, type_for(type));

      auto local = new_anon_local(type_for(type));
      m_block.add_assignment(local, result);

      bool has_ctor = alloc_type->has_func(String("__init__"));
      if (has_ctor) {
        auto ctor_type = (*alloc_type->lookup_funcs(String("__init__")))[0];
        gccjit::function func = get_function(alloc_type->str(), String("__init__"), ctor_type);
    
        std::vector<gccjit::rvalue> args;
        args.push_back(local);
        for (auto arg : node->args)
          args.push_back(visit(arg));
        m_block.add_eval(ctxt.new_call(func, args));
      }
  
      m_expr = local;
    } else {
      assert(false);
    }

  }

  void visit_pointer_to_expr(ast::PointerToExpr const* node) {
    debug("visit_pointer_to_expr\n");
    auto expr = visit(node->expr);
    m_expr = as_lvalue(expr).get_address();
  }


  void visit_dereference_expr(ast::DereferenceExpr const* node) {
    debug("visit_dereference_expr\n");
    m_expr = visit(node->expr).dereference();
  }

  void visit_string_expr(ast::StringExpr const* node) {
    debug("visit_string_expr\n");
    m_expr = ctxt.new_rvalue(node->str.str());
  }

  void visit_number_expr(ast::NumberExpr const* node) {
    debug("visit_number_expr\n");
    gccjit::type int_type = ctxt.get_type(GCC_JIT_TYPE_INT);

    m_expr = ctxt.new_rvalue(int_type, atoi(node->num.str().c_str()));
  }

  void visit_fp_number_expr(ast::FpNumberExpr const* node) {
    debug("visit_fp_number_expr\n");
    gccjit::type type = ctxt.get_type(GCC_JIT_TYPE_FLOAT);
    m_expr = ctxt.new_rvalue(type, atof(node->num.str().c_str()));
  }

  void visit_ident_expr(ast::IdentExpr const* node) {
    debug("visit_ident_expr. %p\n", node);
    if (node->name == String("true")) {
      m_expr = ctxt.new_rvalue(ctxt.get_type(GCC_JIT_TYPE_BOOL), true);
      return;
    } else if (node->name == String("false")) {
      m_expr = ctxt.new_rvalue(ctxt.get_type(GCC_JIT_TYPE_BOOL), false);
      return;
    }

    auto it = m_locals.find(node->name.str());
    if (it != m_locals.end()) {
      m_expr = m_locals[node->name.str()];
      return;
    }

    auto type = tc.type_for(node);
    if (type->is_function()) {
      auto func_type = (*(FunctionOverload const*)type)[0];
      gccjit::function func = get_function(func_type->module()->filename(), node->name, func_type);
      m_expr = gccjit::rvalue((gcc_jit_rvalue*)func.get_inner_function());
    } else {
      auto obj_type = (ObjectType const*)type;
      m_expr = get_global(obj_type->module()->filename(), node->name, obj_type);
    }
  }

  gccjit::rvalue numeric_binary_op(ast::BinaryExpr const* node, gccjit::rvalue lhs, gccjit::rvalue rhs) {
    gcc_jit_binary_op op = gcc_jit_binary_op(-1);
    gcc_jit_comparison cmp = gcc_jit_comparison(-1);

    if (node->op == String("+")) {
      op = GCC_JIT_BINARY_OP_PLUS;
    } else if (node->op == String("-")) {
      op = GCC_JIT_BINARY_OP_MINUS;
    } else if (node->op == String("*")) {
      op = GCC_JIT_BINARY_OP_MULT;
    } else if (node->op == String("/")) {
      op = GCC_JIT_BINARY_OP_DIVIDE;
    } else if (node->op == String("%")) {
      op = GCC_JIT_BINARY_OP_MODULO;
    } else if (node->op == String("&")) {
      op = GCC_JIT_BINARY_OP_BITWISE_AND;
    } else if (node->op == String("|")) {
      op = GCC_JIT_BINARY_OP_BITWISE_OR;
    } else if (node->op == String("^")) {
      op = GCC_JIT_BINARY_OP_BITWISE_XOR;
    } else if (node->op == String("<<")) {
      op = GCC_JIT_BINARY_OP_LSHIFT;
    } else if (node->op == String(">>")) {
      op = GCC_JIT_BINARY_OP_RSHIFT;
    } else if (node->op == String("and")) {
      op = GCC_JIT_BINARY_OP_LOGICAL_AND;
    } else if (node->op == String("or")) {
      op = GCC_JIT_BINARY_OP_LOGICAL_OR;
    } else if (node->op == String("<")) {
      cmp = GCC_JIT_COMPARISON_LT;
    } else if (node->op == String(">")) {
      cmp = GCC_JIT_COMPARISON_GT;
    } else if (node->op == String("<=")) {
      cmp = GCC_JIT_COMPARISON_LE;
    } else if (node->op == String(">=")) {
      cmp = GCC_JIT_COMPARISON_GE;
    } else if (node->op == String("==")) {
      cmp = GCC_JIT_COMPARISON_EQ;
    } else if (node->op == String("!=")) {
      cmp = GCC_JIT_COMPARISON_NE;
    } else {
      printf("Unhandled binary operator: %s\n", node->op.str().c_str());
      assert(false && "Unhandled binary operator");
    }

    if (cmp != -1) {
      return ctxt.new_comparison(cmp, lhs, rhs);
    } else {
      assert(op != -1);
      return ctxt.new_binary_op(op, type_for(tc.type_for(node)), lhs, rhs);
    }
  }

  void visit_unary_expr(ast::UnaryExpr const* node) {
    debug("visit_unary_expr: %s\n", node->op.str().c_str());
    auto expr = visit(node->expr);

    if (node->op == String("not")) {
      m_expr = ctxt.new_unary_op(GCC_JIT_UNARY_OP_LOGICAL_NEGATE,
                                 ctxt.get_type(GCC_JIT_TYPE_BOOL),
                                 expr);
    } else {
      assert(false && "Unhandled operator");
    }
  }

  void visit_binary_expr(ast::BinaryExpr const* node) {
    debug("visit_binary_expr: %s\n", node->op.str().c_str());
    auto lhs = visit(node->lhs);
    auto rhs = visit(node->rhs);

    std::string lhs_type = tc.type_for(node->lhs)->str();
    if (lhs_type == "__builtins__.tau:uint8" or lhs_type == "__builtins__.tau:int" or lhs_type == "__builtins__.tau:float" or lhs_type == "__builtins__.tau:bool") {
      m_expr = numeric_binary_op(node, lhs, rhs);
      return;
    } else {

      const char* func_name = nullptr;
      if (node->op == String("==")) {
        func_name = "__eq__";
      }
      assert(func_name != nullptr && "Unhandled binary op");

      auto lhs_type = (ObjectType const*)tc.type_for(node->lhs);
      FunctionOverload const& func = *lhs_type->lookup_funcs(String(func_name));
      auto jitfn = get_function(lhs_type->str(), String(func_name), func[0]);

      std::vector<gccjit::rvalue> args = {lhs, rhs};
      m_expr = ctxt.new_call(jitfn, args);
      return;
    }
    assert(false);
  }

  // FunctionCallExpr  < Expr :: func:*Expr, args:[*Expr]
  void visit_function_call_expr(ast::FunctionCallExpr const* node) {
    debug("visit_function_call_expr\n");

    if (node->func->asttype == ast::TypeTag::MemberExpr) {
      auto member = (ast::MemberExpr*)node->func;
      if (member->member == String("append") and tc.type_for(member->target)->str().find("__builtins__.tau:Array<") == 0) {
        auto array_type = (ObjectType const*)tc.type_for(member->target);
        int elem_size = ((ObjectType const*)array_type->parameter(String("T")))->alloc_size();
        auto elem_size_jit = ctxt.new_rvalue(ctxt.get_type(GCC_JIT_TYPE_INT), elem_size);

        assert(node->args.size() == 1u);
        auto array = as_lvalue(visit(member->target)).get_address();
   
        auto elem_ptr = ctxt.new_cast(as_lvalue(visit(node->args[0])).get_address(), ctxt.get_type(GCC_JIT_TYPE_VOID_PTR));
        std::vector<gccjit::rvalue> args = {array, elem_size_jit, elem_ptr};
        m_expr = ctxt.new_call(m_append_function, args);

        return;
      }
    }

    auto func = as_function(visit(node->func));

    std::vector<gccjit::rvalue> args;
    if (m_extra_arg.get_inner_rvalue() != nullptr) {
      if (m_extra_arg_is_pointer)
        args.push_back(m_extra_arg);
      else
        args.push_back(as_lvalue(m_extra_arg).get_address());
    }
    m_extra_arg = gccjit::rvalue();

    for (auto arg : node->args)
      args.push_back(visit(arg));

    m_expr = ctxt.new_call(func, args);
  }

  // SubscriptExpr     < Expr :: array:*Expr, index:*Expr
  void visit_subscript_expr(ast::SubscriptExpr const* node) {
    debug("visit_subscript_expr\n");
    auto type = (ObjectType const*)tc.type_for(node->array);
    auto index = visit(node->index);
    auto array = visit(node->array);
    if (type->str() == "__builtins__.tau:str") {
      m_expr = ctxt.new_cast(ctxt.new_array_access(array, index), ctxt.get_type(GCC_JIT_TYPE_INT));
    } else {
      assert(is_array_type(type));
      auto void_ptr = array.access_field(m_array.ptr_field);
      auto typed_ptr = ctxt.new_cast(void_ptr, type_for(type->parameter(String("T"))).get_pointer());
      m_expr = ctxt.new_array_access(typed_ptr, index);
    }
  }

  // Check if an expression is an identifier that references an imported
  // module. Returns the imported module, or null if not an imported names.
  Module const* is_imported_module_name(ast::Expr const* expr) {
    if (expr->asttype == ast::TypeTag::IdentExpr) {
      String s = ((ast::IdentExpr const*)expr)->name;
      auto module = tb.lookup_module(m_file->filename);
      if (not module->is_imported(s))
        return nullptr;
      return tb.lookup_module(module->filename_for_import(s));
    }
    return nullptr;
  }

  // MemberExpr        < Expr :: target:*Expr, member:String
  void visit_member_expr(ast::MemberExpr const* node) {
    Module const* imported_module = is_imported_module_name(node->target);

    if (imported_module != nullptr) {
      if (imported_module->has_func(node->member)) {
        FunctionOverload const& func = *imported_module->lookup_func(node->member);
        auto jitfn = get_function(imported_module->filename(), node->member, func[0]);
        m_expr = gccjit::rvalue((gcc_jit_rvalue*)jitfn.get_inner_function());
      } else {
        assert(imported_module->has_variable(node->member));
        auto obj_type = (ObjectType const*)tc.type_for(node);
        m_expr = get_global(obj_type->module()->filename(), node->member, obj_type);
      }

    } else {

      auto target = visit(node->target);
      auto type = (ObjectType const*)tc.type_for(node->target);
      auto orig_target = target;
      auto orig_type = type;

      // Automatic dereferencing of pointers
      if (is_pointer_type(type)) {
        type = (ObjectType const*)type->parameter(String("S"));
        target = as_lvalue(target).dereference();
      }

      if (type->has_field(node->member)) {
        auto it = m_struct_fields.find(type->str());
        assert(it != m_struct_fields.end());
        assert(it->second.find(node->member) != it->second.end());
        auto field = it->second[node->member];

        m_expr = target.access_field(field);
        m_extra_arg = gccjit::rvalue();
      } else {
        assert(type->has_func(node->member));
        FunctionOverload const& func = *type->lookup_funcs(node->member);
        auto jitfn = get_function(type->str(), node->member, func[0]);

        m_expr = gccjit::rvalue((gcc_jit_rvalue*)jitfn.get_inner_function());
        m_extra_arg = orig_target;
        m_extra_arg_is_pointer = is_pointer_type(orig_type);
      }
    }
  }

  void visit_array_expr(ast::ArrayExpr const* node) {
    debug("visit_array_expr\n");
    (void)node;
  }

  void visit_dict_expr(ast::DictExpr const* node) {
    debug("visit_dict_expr\n");
    (void)node;
  }

  void visit_for_expr(ast::ForExpr const* node) {
    debug("visit_for_expr\n");
    (void)node;
  }

  void emit_if_body(ast::Stmt const* node, gccjit::lvalue dst, bool void_result, gccjit::block next_block) {
    m_block_ended = false;
    if (node->asttype == ast::TypeTag::CompoundStmt) {
      auto const& stmts = ((ast::CompoundStmt const*)node)->stmts;
      if (stmts.size() > 0) {
        for (unsigned i = 0; i < stmts.size() - 1; i++)
          visit(stmts[i]);
        node = stmts.back();
      }
    }

    if (node->asttype == ast::TypeTag::ExprStmt) {
      auto result = visit(((ast::ExprStmt const*)node)->expr);
      if (!void_result)
        m_block.add_assignment(dst, result);
      else
        m_block.add_eval(result);
    } else {
      assert(void_result);
      visit(node);
    }
    if (not m_block_ended)
      m_block.end_with_jump(next_block);
    m_block_ended = true;
  }

  // IfExpr            < Expr :: cond:*Expr, tstmt:*Stmt, fstmt:*Stmt
  void visit_if_expr(ast::IfExpr const* node) {
    debug("visit_if_expr\n");
    Type const* type = tc.type_for(node);
    bool void_result = (type->str() == "__builtins__.tau:void");

    auto res = new_anon_local(type_for(type));
    auto cond = visit(node->cond);
    if (tc.type_for(node->cond)->str() == "__builtins__.tau:int")
      cond = ctxt.new_ne(cond, ctxt.get_type(GCC_JIT_TYPE_INT).zero());

    auto true_block = m_func.new_block("if-true");
    auto false_block = m_func.new_block("if-false");
    auto next_block = m_func.new_block("if-next");
    m_block.end_with_conditional(cond, true_block, false_block);

    m_block = true_block;
    emit_if_body(node->tstmt, res, void_result, next_block);

    m_block = false_block;
    emit_if_body(node->fstmt, res, void_result, next_block);

    m_block = next_block;
    m_block_ended = false;

    m_expr = res;
  }

  // WhileExpr         < Expr :: cond:*Expr, body:*Stmt
  void visit_while_expr(ast::WhileExpr const* node) {
    debug("visit_while_expr\n");

    auto cond_block = m_func.new_block("while-cond");
    auto body_block = m_func.new_block("while-body");
    auto next_block = m_func.new_block("while-next");

    m_block.end_with_jump(cond_block);
    m_block = cond_block;
    auto cond = visit(node->cond);
    m_block.end_with_conditional(cond, body_block, next_block);

    m_block = body_block;
    visit(node->body);
    m_block.end_with_jump(cond_block);

    m_block = next_block;
  }

  void visit_compound_stmt(ast::CompoundStmt const* node) {
    debug("visit_compound_stmt\n");
    for (auto stmt : node->stmts)
      visit(stmt);
  }

  void visit_return_stmt(ast::ReturnStmt const* node) {
    debug("visit_return_stmt %p->%p\n", node, node->expr);
    auto expr = visit(node->expr);

    debug("expr: %s\n", expr.get_debug_string().c_str());

    m_block.end_with_return(expr);
    m_block_ended = true;
  }

  // AssignmentStmt    < Stmt :: op:String, dst:*Expr, src:*Expr
  void visit_assignment_stmt(ast::AssignmentStmt const* node) {
    debug("visit_assignment_stmt\n");
    auto src = visit(node->src);
    auto dst = as_lvalue(visit(node->dst));
    m_block.add_assignment(dst, src);
  }

  void visit_expr_stmt(ast::ExprStmt const* node) {
    debug("visit_expr_stmt\n");
    m_block.add_eval(visit(node->expr));
  }

  // LetStmt           < Stmt :: name:String, type:*Type, init:*Expr
  void visit_let_stmt(ast::LetStmt const* node) {
    debug("visit_let_stmt\n");
    auto type = type_for(tc.type_for(node));
    auto local = m_func.new_local(type, node->name.str());
    m_locals[node->name.str()] = local;
    if (node->init != nullptr) {
      auto init = visit(node->init);
      m_block.add_assignment(local, init);
    } else {
      int alloc_size = ((ObjectType const*)tc.type_for(node))->alloc_size();
      auto jit_alloc_size = ctxt.new_rvalue(ctxt.get_type(GCC_JIT_TYPE_INT), alloc_size);
      std::vector<gccjit::rvalue> args;
      args.push_back(local.get_address());
      args.push_back(jit_alloc_size);
      m_block.add_eval(ctxt.new_call(m_zeroinit_func, args));
    }
  }

  void visit_class_decl(ast::ClassDecl const* node) {
    debug("visit_class_decl\n");
    std::vector<gccjit::field> fields;
    auto type = (ObjectType const*)tb.lookup_type(m_file->filename, node->name, {});
    auto& struct_fields = m_struct_fields[type->str()];

    for (auto it : type->fields()) {
      auto field_name = it.first;
      auto field = ctxt.new_field(type_for(it.second), field_name.str());
      fields.push_back(field);
      struct_fields[field_name] = field;
    }
    
    auto strct = ctxt.new_struct_type(node->name.str(), fields);
    debug("adding type: %s\n", type->str().c_str());
    m_types[type->str()] = strct;

    for (auto decl : node->decls) {
      if (decl->asttype == ast::TypeTag::FunctionDecl) {
        visit_function_decl((ast::FunctionDecl const*)decl, m_file->filename.str() + ":" + node->name.str());
      }
    }

  }

  void visit_function_decl(ast::FunctionDecl const* node) {
    visit_function_decl(node, "");
  }

  // FunctionDecl      < Decl :: name:String, decorators:[*Expr], type:*FunctionType, body:*Stmt
  // FunctionType      < Type :: rettype:*Type, argnames:[String], argtypes:[*Type]
  void visit_function_decl(ast::FunctionDecl const* node, std::string namespace_name) {
    debug("visit_function_decl\n");
    auto func_type = (FunctionType const*)tb.type_for_ast(node);

    bool is_main_function = (namespace_name == "" and node->name == String("main"));

    if (namespace_name == "")
      namespace_name = m_file->filename.str();
    m_func = get_function(namespace_name, node->name, func_type, is_main_function);

    m_locals = m_params_for_functions[m_func.get_inner_function()];
    m_block = m_func.new_block("func-entry");
    m_block_ended = false;
    visit(node->body);
    if (not m_block_ended)
      m_block.end_with_return();

    m_func.dump_to_dot("/tmp/foo.dot");

    m_locals.clear();
  }

  void visit_variable_decl(ast::VariableDecl const* node) {
    (void)node;
  }

  void visit_file(ast::File const* node) {
    m_file = node;
    for (auto decl : node->decls) {
      visit(decl);
    }
    m_file = nullptr;
  }

  void visit_nullptr() {
  }

};




CodeGenerator::CodeGenerator(TypeBuilder const& tb, TypeChecker const& tc) {
  m_helper = new CodeGeneratorHelper(tb, tc);
}

CodeGenerator::~CodeGenerator() {
  delete m_helper;
}

void CodeGenerator::emit(ast::File const* file) {
  m_helper->visit(file);
}

void CodeGenerator::compile(const char* dest_filename) {
  if (dest_filename == nullptr)
    m_helper->result = m_helper->ctxt.compile();
  else {
    m_helper->ctxt.set_bool_option(GCC_JIT_BOOL_OPTION_DEBUGINFO, true);
    //m_helper->ctxt.set_bool_option(GCC_JIT_BOOL_OPTION_DUMP_INITIAL_GIMPLE, true);
    m_helper->ctxt.compile_to_file(GCC_JIT_OUTPUT_KIND_OBJECT_FILE, dest_filename);
    //m_helper->ctxt.dump_reproducer_to_file((std::string(dest_filename) + ".c").c_str());
  }
  m_helper->ctxt.release();
}

void* CodeGenerator::get_symbol(const char* name) {
  return gcc_jit_result_get_code(m_helper->result, name);
}

void CodeGenerator::release_result() {
  gcc_jit_result_release(m_helper->result);
  m_helper->result = nullptr;
}

}


