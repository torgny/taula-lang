#include "lexer.hh"
#include <cctype>
#include <memory.h>
#include <assert.h>
#include <sstream>
#include <iostream>

namespace taula {

Token::Token(String const& string, TokenType type, bool implicit) :
  m_string(string, uint32_t(type) | (implicit ? 0x8000 : 0)) { }

String Token::str() const {
  return m_string;
}

TokenType Token::type() const {
  return TokenType(m_string.tag() & 0x7FFF);
}

bool Token::implicit() const {
  return m_string.tag() & 0x8000;
}


static Token is_operator_or_bracket(const char* begin, const char* end) {
  struct Def {
    const char* text;
    int len;
    TokenType type;
  };
  static const Def DEFS[] = {
                              { "<<", 2, TokenType::OP_2LT     },
                              { "<<", 2, TokenType::OP_2LT     },
                              { ">>", 2, TokenType::OP_2GT     },
                              { "==", 2, TokenType::OP_2EQ     },
                              { ">=", 2, TokenType::OP_GE      },
                              { "<=", 2, TokenType::OP_LE      },
                              { "!=", 2, TokenType::OP_NE      },
                              { "~=", 2, TokenType::OP_MATCH   },
                              { "..", 2, TokenType::DOTDOT     },
                              { "+",  1, TokenType::OP_PLUS    },
                              { "-",  1, TokenType::OP_MINUS   },
                              { "*",  1, TokenType::OP_STAR    },
                              { "/",  1, TokenType::OP_SLASH   },
                              { "%",  1, TokenType::OP_PERCENT },
                              { "&",  1, TokenType::OP_AMPER   },
                              { "|",  1, TokenType::OP_PIPE    },
                              { "^",  1, TokenType::OP_HAT     },
                              { "<",  1, TokenType::OP_LT      },
                              { ">",  1, TokenType::OP_GT      },
                              { "=",  1, TokenType::OP_EQ      },
                              { "!",  1, TokenType::OP_EXCLAM  },
                              { "(",  1, TokenType::LPAREN     },
                              { ")",  1, TokenType::RPAREN     },
                              { "{",  1, TokenType::LBRACE     },
                              { "}",  1, TokenType::RBRACE     },
                              { "[",  1, TokenType::LBRACKET   },
                              { "]",  1, TokenType::RBRACKET   },
                              { ";",  1, TokenType::SEMICOLON  },
                              { ":",  1, TokenType::COLON      },
                              { ".",  1, TokenType::DOT        },
                              { ",",  1, TokenType::COMMA      },
                              { "@",  1, TokenType::AT         },
                              { NULL, 1, TokenType::ERROR      }
                            };
  (void)end;
  for (int i = 0; DEFS[i].text != NULL; i++) {
    if (strncmp(DEFS[i].text, begin, DEFS[i].len) == 0) {
      return Token(String(begin, &begin[DEFS[i].len]), DEFS[i].type);
    }
  }
  return Token(String(), TokenType::ERROR);
}


static Token is_identifier(const char* begin, const char* end) {
  if (isalpha(begin[0]) or begin[0] == '_') {
    int len = 0;
    for (; &begin[len] < end and (begin[len] == '_' or isalnum(begin[len])); len++) { }
      return Token(String(begin, &begin[len]), TokenType::IDENT);
  }
  return Token(String(), TokenType::ERROR);
}


static bool is_hexdigit(char c) {
  return (c >= '0' and c <= '9') or (c >= 'a' and c <= 'f') or (c >= 'A' and c <= 'F');
}

static bool is_bindigit(char c) {
  return c == '0' or c == '1';
}

static Token is_number(const char* begin, const char* end) {
  int len = 0;
  bool is_decimal = false;

  // is hex or bin? (0x... or 0b...)
  if (end - begin > 2 and begin[0] == '0' and isalpha(begin[1])) {
    len += 2;
    if (begin[1] == 'x') {
      for (; &begin[len] < end and is_hexdigit(begin[len]); len++) { }
    } else if (begin[1] == 'b') {
      for (; &begin[len] < end and is_bindigit(begin[len]); len++) { }
    } else {
      return Token(String(), TokenType::ERROR);
    }

  // decimal
  } else {
    is_decimal = true;
    for (; &begin[len] < end and isdigit(begin[len]); len++) { }
  }

  if (len == 0 or (&begin[len] < end and isalpha(begin[len])))
    return Token(String(), TokenType::ERROR);

  // floating point
  if (is_decimal and &begin[len] < end and begin[len] == '.') {
    len++;
    for (; isdigit(begin[len]) and &begin[len] < end; len++) { }
    if (&begin[len] < end and isalpha(begin[len]))
      return Token(String(), TokenType::ERROR);
    return Token(String(begin, &begin[len]), TokenType::FPNUMBER);
  }

  return Token(String(begin, &begin[len]), TokenType::NUMBER);
}


static Token is_quoted_string(const char* begin, const char* end) {
  if (begin[0] != '"')
    return Token(String(), TokenType::ERROR);
  
  bool esc = false;
  for (int len = 1; &begin[len] < end; len++) {
    if (!esc and begin[len] == '"')
      return Token(String(&begin[0], &begin[len + 1]), TokenType::QUOTED_STRING);
    esc = (begin[len] == '\\');
  }

  assert(false && "unterminated string");
}



static Token is_keyword(const char* begin, const char* end) {
  struct Def {
    const char* text;
    int len;
    TokenType type;
  };
  static const Def DEFS[] = {
                              { "import",    6, TokenType::KW_IMPORT    },
                              { "if",        2, TokenType::KW_IF        },
                              { "else",      4, TokenType::KW_ELSE      },
                              { "return",    6, TokenType::KW_RETURN    },
                              { "interface", 9, TokenType::KW_INTERFACE },
                              { "while",     5, TokenType::KW_WHILE     },
                              { "enum",      4, TokenType::KW_ENUM      },
                              { "for",       3, TokenType::KW_FOR       },
                              { "in",        2, TokenType::KW_IN        },
                              { "let",       3, TokenType::KW_LET       },
                              { "def",       3, TokenType::KW_DEF       },
                              { "new",       3, TokenType::KW_NEW       },
                              { "class",     5, TokenType::KW_CLASS     },
                              { "pass",      4, TokenType::KW_PASS      },
                              { "and",       3, TokenType::KW_AND       },
                              { "or",        2, TokenType::KW_OR        },
                              { "not",       3, TokenType::KW_NOT       },
                              { "cast",      4, TokenType::KW_CAST      },
                              { NULL,        0, TokenType::ERROR        }
                            };
  (void)end;
  for (int i = 0; DEFS[i].text != NULL; i++) {
    if (strncmp(DEFS[i].text, begin, DEFS[i].len) == 0 and
        not isalnum(begin[DEFS[i].len]) and begin[DEFS[i].len] != '_')
      return Token(String(begin, &begin[DEFS[i].len]), DEFS[i].type);
  }
  return Token(String(""), TokenType::ERROR);
}


String skip_whitespace(String const& string, int& indent) {
  const char* begin = string.begin();
  const char* end = string.end();

  size_t index = 0;
  int newline_index = -1;
  while (true) {
    // Skip spaces, tabs, newlines, etc.
    for (; &begin[index] < end and isspace(begin[index]); index++) {
      if (begin[index] == '\n')
        newline_index = index;
    }

    // Skip /*-style comment
    if (end - begin > 1 and begin[index] == '/' and begin[index + 1] == '*') {
      for (; &begin[index] < end and (begin[index] != '*' or begin[index + 1] != '/'); index++) { }
      index += 2;
      continue;
    }

    // Skip //-style comment
    if (end - begin > 1 and begin[index] == '/' and begin[index + 1] == '/') {
      for (; begin[index] != '\n' and &begin[index] < end; index++) { }
      continue;
    }

    // Skip #-style comment
    if (end - begin > 1 and begin[index] == '#') {
      for (; begin[index] != '\n' and &begin[index] < end; index++) { }
      continue;
    }

    break;
  }
  if (newline_index >= 0)
    indent = index - newline_index - 1;
  return String(&begin[index], end);
}



Lexer::Lexer(String filename,
             const char* begin,
             const char* end) :
  m_filename(filename),
  m_indent(0)
{
  m_queue.push_back(String(begin, end));
}


#define STRING (m_queue.back())

#define RETURN_IF(pred)                                   \
  do {                                                    \
    Token tok = pred(STRING.begin(), STRING.end());       \
    if (tok.type() != TokenType::ERROR) {                 \
      STRING = STRING.lskip(tok.str().length());          \
      return tok;                                         \
    }                                                     \
  } while (false)


static int skip_witespace_and_empty_expansions(std::vector<String>& queue) {
  int indent = -1;

  if (queue.size() == 0u)
    return indent;
  queue.back() = skip_whitespace(queue.back(), indent);

  // Check if at or beyond end of string.
  while (queue.back().length() == 0u) {
    queue.pop_back();
    if (queue.size() == 0u)
      return indent;
    queue.back() = skip_whitespace(queue.back(), indent);
  }
  return indent;
}


static Token IMPLICIT_SEMICOLON(String(";"), TokenType::SEMICOLON, true);


void deindent(unsigned indent, unsigned target_indent, std::queue<Token>& emit) {
  while (indent > target_indent) {
    indent -= 4;
    emit.push(Token(String(), TokenType::END_BLOCK));
    emit.push(IMPLICIT_SEMICOLON);
  }
}


Token Lexer::get_next_token() {
  int new_indent = skip_witespace_and_empty_expansions(m_queue);
  if (m_queue.size() == 0u) {
    deindent(m_indent, 0, m_emit);
    m_indent = 0;
    m_emit.push(Token(String(), TokenType::INPUT_END));
    Token t = m_emit.front();
    m_emit.pop();
    return t;
  }

  // If the indentation is less then previously, then this is block end.
  // If the indentation is more then previously, then this is just a
  // purely visual indentation (only : and = can introduce deeper indentation).
  if (new_indent != -1 and new_indent < int(m_indent)) {
    deindent(m_indent, new_indent, m_emit);
    m_indent = new_indent;
    Token t = m_emit.front();
    m_emit.pop();
    return t;
  }

  assert(m_queue.size() > 0u);
  RETURN_IF(is_keyword);
  RETURN_IF(is_number);
  RETURN_IF(is_quoted_string);
  RETURN_IF(is_identifier);
  RETURN_IF(is_operator_or_bracket);

  // This is an incorrect token. Skip until non alpha numeric character and
  // create an error token.
  size_t len = 1;
  const char* begin = STRING.begin();
  const char* end = STRING.end();
  for (; isalnum(begin[len]) and &begin[len] < end; len++) { }

  Token tok(STRING.prefix(len), TokenType::ERROR);
  STRING = STRING.lskip(len);
  return tok;
}


static bool newline_is_next(String str) {
  const char* begin = str.begin();
  const char* end = str.end();

  for (; begin < end and *begin == ' '; begin++) { }
  if (begin >= end)
    return false; // TODO: end of input should probably be consider a newline to make implicit semicolons work correctly for those cases when there is no newline at end of file
  // TODO: handle /*-style comments
  return begin[0] == '\n' or begin[0] == '#' or (begin[0] == '/' and begin[1] == '/');
}


Token Lexer::next() {
  if (m_emit.size() > 0) {
    Token t = m_emit.front();
    m_emit.pop();
    return t;
  }

  if (m_queue.size() == 0) {
    return Token(String(), TokenType::INPUT_END);
  }


  Token tok = get_next_token();
  TokenType toktype = tok.type();

  bool is_stmt_term = (toktype == TokenType::IDENT or
                       toktype == TokenType::NUMBER or
                       toktype == TokenType::FPNUMBER or
                       toktype == TokenType::QUOTED_STRING or
                       toktype == TokenType::KW_RETURN or
                       toktype == TokenType::KW_PASS or
                       toktype == TokenType::RPAREN or
                       toktype == TokenType::RBRACKET or
                       toktype == TokenType::RBRACE);
  bool is_block_starter = (toktype == TokenType::COLON or
                           toktype == TokenType::OP_EQ);

  bool nl_next = m_queue.size() > 0 and newline_is_next(m_queue.back()); // Can only be called if we know there will be chars to process...? Why?

  if (is_block_starter and nl_next) {
    m_emit.push(Token(String(), TokenType::BEGIN_BLOCK));
    m_indent += 4;
  }
  if (is_stmt_term and nl_next)
    m_emit.push(IMPLICIT_SEMICOLON);

  return tok;
}


}