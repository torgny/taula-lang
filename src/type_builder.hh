#pragma once
#include "parser2.hh"
#include <unordered_map>
#include <string>

namespace taula {

class Module;
class ParamMap;


// Base class for all classes representing types.
class Type {
public:
  virtual std::string str() const = 0;
  virtual Type const* substitute(ParamMap const& params) const = 0;
  virtual bool is_function() const = 0;
};


// A member function or global function
class FunctionType : public Type {
  friend class TypeBuilder;
  std::vector<Type const*> m_argtypes;
  std::vector<String> m_argnames;
  Type const* m_rettype;
  ast::Stmt const* m_body;
  Module const* m_module;
public:
  std::string str() const { return "FunctionType"; }
  Type const* rettype() const { return m_rettype; }
  std::vector<Type const*> const& argtypes() const { return m_argtypes; }
  std::vector<String> argnames() const { return m_argnames; }
  ast::Stmt const* body() const { return m_body; }
  Module const* module() const { return m_module; }

  // Substitute type parameters with actual types.
  Type const* substitute(ParamMap const& params) const;

  bool is_function() const { return true; }

};

// A collection of functions with the same name. This isn't really a type but
// considering them being a type make implementation easier.
class FunctionOverload : public Type {
  friend class TypeBuilder;
  friend class ObjectType;
  std::vector<FunctionType const*> m_overloads;
public:
  size_t size() const { return m_overloads.size(); }
  FunctionType const* operator[](size_t idx) const { return m_overloads[idx]; }
  std::string str() const;

  // Substitute type parameters with actual types.
  Type const* substitute(ParamMap const& params) const;
  bool is_function() const { return true; }
};


class ObjectTypeDefinition;

// A class or interface. Technically a type specialization.
class ObjectType : public Type {
  friend class TypeBuilder;
  friend class ObjectTypeDefinition;

  std::unordered_map<String, FunctionOverload> m_functions;
  std::vector<Type const*> m_parameters;
  ObjectTypeDefinition const* m_typedef = nullptr;
public:
  std::string str() const;

  std::string def_name() const;

  Module const* module() const;

  int alloc_size() const;

  bool has_parameter(String name) const;
  Type const* parameter(String name) const;

  bool has_field(String name) const;
  Type const* field(String name) const;
  std::unordered_map<String, Type const*> const& fields() const;

  bool has_func(String name) const;
  FunctionOverload const* lookup_funcs(String name) const;

  Type const* substitute(ParamMap const& params) const;

  bool is_function() const { return false; }

};


class TypeVariable : public Type {
  friend class TypeBuilder;
  String m_name;
public:
  std::string str() const { return m_name.str(); }
  Type const* substitute(ParamMap const& params) const;
  bool is_function() const { return false; }
};


typedef std::unordered_map<String, String> FileForNamespace;


// A module.
class Module {
  friend class TypeBuilder;
  std::unordered_map<String, ObjectTypeDefinition const*> m_typedefs;
  std::unordered_map<String, Type const*> m_variables;
  std::unordered_map<String, Type const*> m_types;
  std::unordered_map<String, FunctionOverload> m_functions;
  // Map from short name (the name of the namespace) to the imported filename.
  FileForNamespace m_filename_for_import;

  String m_filename;
public:
  // Get the name of the file in which this module is defined.
  std::string filename() const { return m_filename.str(); }

  // Check if a module is imported. The argument 'name' is the local name.
  bool is_imported(String name) const { return m_filename_for_import.find(name) != m_filename_for_import.end(); }

  // Get the filename for the module which is locally known as 'name'.
  String filename_for_import(String name) const;

  // Lookup a global function.
  FunctionOverload const* lookup_func(String name) const;

  // Check if this module has a global function named 'name'.
  bool has_func(String name) const { return m_functions.count(name) > 0; }

  // Lookup global variable.
  Type const* lookup_variable(String name) const;

  // Check if this module has a global variable named 'name'.
  bool has_variable(String name) const { return m_variables.count(name) > 0; }
};


// Builds the types defined in source files.
class TypeBuilder {
  // Map from filename to module.
  std::unordered_map<String, Module*> m_modules;

  FunctionType const* build_function(Module* module, ast::FunctionDecl const*);
  void build_object_type(Module* module, String type_name,
                         std::vector<ast::Decl const*> const& members,
                         std::vector<ast::TypeVariableDecl const*> const& parameters);
  Type const* type_for(Module* module, ast::Type const* type);
  Module* module_for(String name, FileForNamespace const& imported_modules);

  ObjectTypeDefinition* typedef_for(Module* module, String type_name);

  std::unordered_map<ast::Node const*, Type const*> m_type_for_ast;
public:
  void build_file(ast::File const* file);

  ObjectType const* lookup_type(String module_filename, String type_name, std::vector<Type const*> const& params) const;
  FunctionOverload const* lookup_func(String module_filename, String func_name) const;

  Module const* lookup_module(String module_filename) const;

  Type const* type_for_ast(ast::Node const* ast) const;
};

}
