#pragma once
#include "parser2.hh"
#include <unordered_map>
#include <string>

namespace taula {

class TypeBuilder;
class Type;
class Module;

typedef std::unordered_map<ast::Node const*, Type const*> NodeTypes;

class TypeChecker {
  TypeBuilder const& m_modules;
  NodeTypes m_types;
public:
  TypeChecker(TypeBuilder const& modules) : m_modules(modules) { }

  // Assigns a type for every expression in the AST of the module.
  void typecheck(ast::File const* file);

  // TODO: This interface is incompatible with functions in generic types.
  Type const* type_for(ast::Node const* node) const;
};

}