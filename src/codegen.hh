#pragma once
#include "type_builder.hh"
#include "parser2.hh"
#include "typecheck2.hh"

namespace taula {


class CodeGeneratorHelper;
class CodeGenerator {
  CodeGeneratorHelper* m_helper;

  CodeGenerator(CodeGenerator const&);
  void operator=(CodeGenerator const&);
public:
  CodeGenerator(TypeBuilder const& tb, TypeChecker const& tc);
  ~CodeGenerator();

  void emit(ast::File const* file);

  // if no dest_filename, compile to memory.
  void compile(const char* dest_filename=nullptr);

  void* get_symbol(const char* name);

  void release_result();
};

} // namespace taula