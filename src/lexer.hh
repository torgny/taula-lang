#pragma once

#include "string.hh"
#include <stdint.h>
#include <string>
#include <vector>
#include <queue>





namespace taula {
using taula::String;

//! The valid types of this tokens.
enum class TokenType {
  UNDEF         = 0,
  KW_IMPORT     = 1,
  KW_MODULE     = 2,
  KW_IF         = 3,
  KW_ELSE       = 4,
  KW_RETURN     = 6,
  KW_INTERFACE  = 7,
  KW_ENUM       = 8,
  KW_WHILE      = 9,
  KW_FOR        = 11,
  KW_IN         = 12,
  KW_LET        = 14,
  KW_DEF        = 15,
  KW_CLASS      = 16,
  KW_PASS       = 17,
  KW_NEW        = 18,
  KW_AND        = 201,
  KW_OR         = 202,
  KW_NOT        = 203,
  KW_CAST       = 204,

  OP_PLUS       = 20,
  OP_MINUS      = 21,
  OP_STAR       = 22,
  OP_SLASH      = 23,
  OP_PERCENT    = 24,
  OP_AMPER      = 25, // &
  OP_PIPE       = 26,
  OP_HAT        = 27, // ^
  OP_LT         = 28,
  OP_LE         = 29,
  OP_GT         = 30,
  OP_GE         = 31,
  OP_EQ         = 32,
  OP_2LT        = 33, // <<
  OP_2GT        = 34, // >>
  OP_2EQ        = 35, // ==
  OP_EXCLAM     = 36,
  OP_NE         = 37, // !=
  OP_MATCH      = 38, // ~=

  NUMBER        = 50,
  FPNUMBER      = 51,
  QUOTED_STRING = 52, 

  IDENT         = 60,
  SEMICOLON     = 61,
  COLON         = 62,
  DOT           = 63,
  DOTDOT        = 64,
  COMMA         = 65,
  AT            = 66,

  LPAREN        = 70,
  RPAREN        = 71,
  LBRACE        = 72,
  RBRACE        = 73,
  LBRACKET      = 74,
  RBRACKET      = 75,

  BEGIN_BLOCK   = 100, // Abstract token denoting start of a block.
  END_BLOCK     = 101, // Abstract token denoting end of a block.

  INPUT_END     = 254,
  ERROR         = 255
};

/*
struct SourceLine {
  std::string filename;
  uint32_t line;
  uint32_t column;
};

struct SourceLocation {
  std::vector<SourceLine> stack;
};
*/

class Token {
  String m_string;
public:
  Token() { }
  Token(String const& string, TokenType type, bool implicit = false);

  String str() const;
  TokenType type() const;
  bool implicit() const;
};

class TokenMacro {
public:
  virtual String expand(const char* begin,
                        const char* end) const = 0;

//  virtual SourceLocation location(Token token) const = 0;
};


class Lexer {
  String m_filename;
  std::vector<String> m_queue;
  std::queue<Token> m_emit;
  unsigned m_indent;

  Token get_next_token();
public:
  Lexer(String filename,
        const char* begin,
        const char* end);

  //! Get the token.
  Token next();

  //! Get the location of the provided token.
  //SourceLocation location(Token token) const;
};

}