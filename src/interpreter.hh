#include <stdint.h>

union Instruction {
  struct {
    uint8_t opcode;
    uint8_t opa;
    uint8_t opb;
    uint8_t opc;
  } instr;
  uint32_t u32;
  int32_t i32;
};

union Constant {
  uint64_t i;
  double d;
  float f;
  void* p;
};

extern "C" int64_t interpret(Instruction* instrs, uint64_t* const_pool);