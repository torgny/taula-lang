#pragma once
#include <string>
#include <ostream>

namespace taula {

/**
 * An immutable string that share the physical string. Can also point into
 * a persistent string buffer.
 * A String instance also carries and additionally 32 bits worth of
 * data. This can be used by clients, e.g., a precomputed hash code.
 * (Due to alignment requirements on 64 bit architectures, we get the
 * m_flags memory region for free, so why not use it...?).
 */
class String {
  // TODO: reference count
  const char* m_begin;
  uint32_t m_length;
  uint32_t m_tag;
  String(const char* begin, uint32_t length, uint32_t tag);
public:
  //! Delete all dynamically allocated strings.
  static void gc();

  explicit String(uint32_t tag=0);
  explicit String(const char* s, uint32_t tag=0);
  explicit String(std::string const& s, uint32_t tag=0);
  String(const char* begin, const char* end, uint32_t tag=0);
  String(String const& s, uint32_t tag);
  String(String const& s);

  char operator[](int i) const;
  String& operator=(String const& s);

  //! Note: tag are not considered.
  bool operator==(String const& rhs) const;
  bool operator!=(String const& rhs) const;

  //! Note: tag are or:ed.
  String operator+(String const& rhs) const;

  //! Skip a number of characters at the beginning of the string.
  String lskip(size_t num) const;

  //! Get the first 'num' characters of the string.
  String prefix(size_t num) const;

  std::string str() const;

  uint64_t hash() const;

  size_t length() const;
  uint32_t tag() const;

  const char* begin() const;
  const char* end() const;
};

std::ostream& operator<<(std::ostream&, String const&);

} // namespace taula

namespace std {
  template <> struct hash<taula::String> {
    size_t operator()(taula::String const& s) const { return s.hash(); }
  };
}
