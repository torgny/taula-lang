enum class TypeTag {
  StringExpr,
  NumberExpr,
  FpNumberExpr,
  IdentExpr,
  BinaryExpr,
  UnaryExpr,
  FunctionExpr,
  FunctionCallExpr,
  SubscriptExpr,
  MemberExpr,
  MatchExpr,
  ArrayExpr,
  DictExpr,
  ForExpr,
  IfExpr,
  WhileExpr,
  NewExpr,
  CastExpr,
  PointerToExpr,
  DereferenceExpr,
  CompoundStmt,
  PassStmt,
  ReturnStmt,
  AssignmentStmt,
  ExprStmt,
  LetStmt,
  Typename,
  TypeVariable,
  FunctionType,
  TypeVariableDecl,
  ImportDecl,
  ClassDecl,
  InterfaceDecl,
  FunctionDecl,
  VariableDecl,
  File,
};

class StringExpr;
class NumberExpr;
class FpNumberExpr;
class IdentExpr;
class BinaryExpr;
class UnaryExpr;
class FunctionExpr;
class FunctionCallExpr;
class SubscriptExpr;
class MemberExpr;
class MatchExpr;
class ArrayExpr;
class DictExpr;
class ForExpr;
class IfExpr;
class WhileExpr;
class NewExpr;
class CastExpr;
class PointerToExpr;
class DereferenceExpr;
class CompoundStmt;
class PassStmt;
class ReturnStmt;
class AssignmentStmt;
class ExprStmt;
class LetStmt;
class Typename;
class TypeVariable;
class FunctionType;
class TypeVariableDecl;
class ImportDecl;
class ClassDecl;
class InterfaceDecl;
class FunctionDecl;
class VariableDecl;
class File;

struct StringExpr : public Expr {
  String const str;
  StringExpr(String const& str) : str(str) { asttype = TypeTag::StringExpr; }
};

struct NumberExpr : public Expr {
  String const num;
  NumberExpr(String const& num) : num(num) { asttype = TypeTag::NumberExpr; }
};

struct FpNumberExpr : public Expr {
  String const num;
  FpNumberExpr(String const& num) : num(num) { asttype = TypeTag::FpNumberExpr; }
};

struct IdentExpr : public Expr {
  String const name;
  IdentExpr(String const& name) : name(name) { asttype = TypeTag::IdentExpr; }
};

struct BinaryExpr : public Expr {
  String const op;
  Expr const* const lhs;
  Expr const* const rhs;
  BinaryExpr(String const& op, Expr const* const& lhs, Expr const* const& rhs) : op(op), lhs(lhs), rhs(rhs) { asttype = TypeTag::BinaryExpr; }
};

struct UnaryExpr : public Expr {
  String const op;
  Expr const* const expr;
  UnaryExpr(String const& op, Expr const* const& expr) : op(op), expr(expr) { asttype = TypeTag::UnaryExpr; }
};

struct FunctionExpr : public Expr {
  FunctionType const* const type;
  Stmt const* const body;
  FunctionExpr(FunctionType const* const& type, Stmt const* const& body) : type(type), body(body) { asttype = TypeTag::FunctionExpr; }
};

struct FunctionCallExpr : public Expr {
  Expr const* const func;
  std::vector<Expr const*> const args;
  FunctionCallExpr(Expr const* const& func, std::vector<Expr const*> const& args) : func(func), args(args) { asttype = TypeTag::FunctionCallExpr; }
};

struct SubscriptExpr : public Expr {
  Expr const* const array;
  Expr const* const index;
  SubscriptExpr(Expr const* const& array, Expr const* const& index) : array(array), index(index) { asttype = TypeTag::SubscriptExpr; }
};

struct MemberExpr : public Expr {
  Expr const* const target;
  String const member;
  MemberExpr(Expr const* const& target, String const& member) : target(target), member(member) { asttype = TypeTag::MemberExpr; }
};

struct MatchExpr : public Expr {
  String const op;
  Expr const* const dst;
  Expr const* const src;
  MatchExpr(String const& op, Expr const* const& dst, Expr const* const& src) : op(op), dst(dst), src(src) { asttype = TypeTag::MatchExpr; }
};

struct ArrayExpr : public Expr {
  std::vector<Expr const*> const elems;
  ArrayExpr(std::vector<Expr const*> const& elems) : elems(elems) { asttype = TypeTag::ArrayExpr; }
};

struct DictExpr : public Expr {
  std::vector<Expr const*> const keys;
  std::vector<Expr const*> const values;
  DictExpr(std::vector<Expr const*> const& keys, std::vector<Expr const*> const& values) : keys(keys), values(values) { asttype = TypeTag::DictExpr; }
};

struct ForExpr : public Expr {
  std::vector<String> const vars;
  Expr const* const seq;
  Stmt const* const body;
  ForExpr(std::vector<String> const& vars, Expr const* const& seq, Stmt const* const& body) : vars(vars), seq(seq), body(body) { asttype = TypeTag::ForExpr; }
};

struct IfExpr : public Expr {
  Expr const* const cond;
  Stmt const* const tstmt;
  Stmt const* const fstmt;
  IfExpr(Expr const* const& cond, Stmt const* const& tstmt, Stmt const* const& fstmt) : cond(cond), tstmt(tstmt), fstmt(fstmt) { asttype = TypeTag::IfExpr; }
};

struct WhileExpr : public Expr {
  Expr const* const cond;
  Stmt const* const body;
  WhileExpr(Expr const* const& cond, Stmt const* const& body) : cond(cond), body(body) { asttype = TypeTag::WhileExpr; }
};

struct NewExpr : public Expr {
  Type const* const type;
  std::vector<Expr const*> const args;
  Expr const* const lifetime;
  NewExpr(Type const* const& type, std::vector<Expr const*> const& args, Expr const* const& lifetime) : type(type), args(args), lifetime(lifetime) { asttype = TypeTag::NewExpr; }
};

struct CastExpr : public Expr {
  Type const* const type;
  Expr const* const arg;
  CastExpr(Type const* const& type, Expr const* const& arg) : type(type), arg(arg) { asttype = TypeTag::CastExpr; }
};

struct PointerToExpr : public Expr {
  Expr const* const expr;
  PointerToExpr(Expr const* const& expr) : expr(expr) { asttype = TypeTag::PointerToExpr; }
};

struct DereferenceExpr : public Expr {
  Expr const* const expr;
  DereferenceExpr(Expr const* const& expr) : expr(expr) { asttype = TypeTag::DereferenceExpr; }
};

struct CompoundStmt : public Stmt {
  std::vector<Stmt const*> const stmts;
  CompoundStmt(std::vector<Stmt const*> const& stmts) : stmts(stmts) { asttype = TypeTag::CompoundStmt; }
};

struct PassStmt : public Stmt {
  PassStmt() { asttype = TypeTag::PassStmt; }
};

struct ReturnStmt : public Stmt {
  Expr const* const expr;
  ReturnStmt(Expr const* const& expr) : expr(expr) { asttype = TypeTag::ReturnStmt; }
};

struct AssignmentStmt : public Stmt {
  String const op;
  Expr const* const dst;
  Expr const* const src;
  AssignmentStmt(String const& op, Expr const* const& dst, Expr const* const& src) : op(op), dst(dst), src(src) { asttype = TypeTag::AssignmentStmt; }
};

struct ExprStmt : public Stmt {
  Expr const* const expr;
  ExprStmt(Expr const* const& expr) : expr(expr) { asttype = TypeTag::ExprStmt; }
};

struct LetStmt : public Stmt {
  String const name;
  Type const* const type;
  Expr const* const init;
  LetStmt(String const& name, Type const* const& type, Expr const* const& init) : name(name), type(type), init(init) { asttype = TypeTag::LetStmt; }
};

struct Typename : public Type {
  String const module;
  String const name;
  std::vector<Type const*> const params;
  Typename(String const& module, String const& name, std::vector<Type const*> const& params) : module(module), name(name), params(params) { asttype = TypeTag::Typename; }
};

struct TypeVariable : public Type {
  String const name;
  std::vector<Type const*> const params;
  TypeVariable(String const& name, std::vector<Type const*> const& params) : name(name), params(params) { asttype = TypeTag::TypeVariable; }
};

struct FunctionType : public Type {
  Type const* const rettype;
  std::vector<String> const argnames;
  std::vector<Type const*> const argtypes;
  FunctionType(Type const* const& rettype, std::vector<String> const& argnames, std::vector<Type const*> const& argtypes) : rettype(rettype), argnames(argnames), argtypes(argtypes) { asttype = TypeTag::FunctionType; }
};

struct TypeVariableDecl : public Node {
  String const name;
  Type const* const interface;
  TypeVariableDecl(String const& name, Type const* const& interface) : name(name), interface(interface) { asttype = TypeTag::TypeVariableDecl; }
};

struct ImportDecl : public Decl {
  String const name;
  ImportDecl(String const& name) : name(name) { asttype = TypeTag::ImportDecl; }
};

struct ClassDecl : public Decl {
  String const name;
  std::vector<Expr const*> const decorators;
  std::vector<TypeVariableDecl const*> const params;
  std::vector<Decl const*> const decls;
  ClassDecl(String const& name, std::vector<Expr const*> const& decorators, std::vector<TypeVariableDecl const*> const& params, std::vector<Decl const*> const& decls) : name(name), decorators(decorators), params(params), decls(decls) { asttype = TypeTag::ClassDecl; }
};

struct InterfaceDecl : public Decl {
  String const name;
  std::vector<Expr const*> const decorators;
  std::vector<TypeVariableDecl const*> const params;
  std::vector<Decl const*> const decls;
  InterfaceDecl(String const& name, std::vector<Expr const*> const& decorators, std::vector<TypeVariableDecl const*> const& params, std::vector<Decl const*> const& decls) : name(name), decorators(decorators), params(params), decls(decls) { asttype = TypeTag::InterfaceDecl; }
};

struct FunctionDecl : public Decl {
  String const name;
  std::vector<Expr const*> const decorators;
  FunctionType const* const type;
  Stmt const* const body;
  FunctionDecl(String const& name, std::vector<Expr const*> const& decorators, FunctionType const* const& type, Stmt const* const& body) : name(name), decorators(decorators), type(type), body(body) { asttype = TypeTag::FunctionDecl; }
};

struct VariableDecl : public Decl {
  String const name;
  Type const* const type;
  Expr const* const init;
  VariableDecl(String const& name, Type const* const& type, Expr const* const& init) : name(name), type(type), init(init) { asttype = TypeTag::VariableDecl; }
};

struct File : public Node {
  String const filename;
  String const short_name;
  std::vector<Decl const*> const decls;
  File(String const& filename, String const& short_name, std::vector<Decl const*> const& decls) : filename(filename), short_name(short_name), decls(decls) { asttype = TypeTag::File; }
};

class Visitor {
public:
  void visit(Node const* node) {
    if (node == nullptr) { visit_nullptr(); return; }
    switch (node->asttype) {
    case TypeTag::StringExpr: visit_string_expr((StringExpr*)node); return;
    case TypeTag::NumberExpr: visit_number_expr((NumberExpr*)node); return;
    case TypeTag::FpNumberExpr: visit_fp_number_expr((FpNumberExpr*)node); return;
    case TypeTag::IdentExpr: visit_ident_expr((IdentExpr*)node); return;
    case TypeTag::BinaryExpr: visit_binary_expr((BinaryExpr*)node); return;
    case TypeTag::UnaryExpr: visit_unary_expr((UnaryExpr*)node); return;
    case TypeTag::FunctionExpr: visit_function_expr((FunctionExpr*)node); return;
    case TypeTag::FunctionCallExpr: visit_function_call_expr((FunctionCallExpr*)node); return;
    case TypeTag::SubscriptExpr: visit_subscript_expr((SubscriptExpr*)node); return;
    case TypeTag::MemberExpr: visit_member_expr((MemberExpr*)node); return;
    case TypeTag::MatchExpr: visit_match_expr((MatchExpr*)node); return;
    case TypeTag::ArrayExpr: visit_array_expr((ArrayExpr*)node); return;
    case TypeTag::DictExpr: visit_dict_expr((DictExpr*)node); return;
    case TypeTag::ForExpr: visit_for_expr((ForExpr*)node); return;
    case TypeTag::IfExpr: visit_if_expr((IfExpr*)node); return;
    case TypeTag::WhileExpr: visit_while_expr((WhileExpr*)node); return;
    case TypeTag::NewExpr: visit_new_expr((NewExpr*)node); return;
    case TypeTag::CastExpr: visit_cast_expr((CastExpr*)node); return;
    case TypeTag::PointerToExpr: visit_pointer_to_expr((PointerToExpr*)node); return;
    case TypeTag::DereferenceExpr: visit_dereference_expr((DereferenceExpr*)node); return;
    case TypeTag::CompoundStmt: visit_compound_stmt((CompoundStmt*)node); return;
    case TypeTag::PassStmt: visit_pass_stmt((PassStmt*)node); return;
    case TypeTag::ReturnStmt: visit_return_stmt((ReturnStmt*)node); return;
    case TypeTag::AssignmentStmt: visit_assignment_stmt((AssignmentStmt*)node); return;
    case TypeTag::ExprStmt: visit_expr_stmt((ExprStmt*)node); return;
    case TypeTag::LetStmt: visit_let_stmt((LetStmt*)node); return;
    case TypeTag::Typename: visit_typename((Typename*)node); return;
    case TypeTag::TypeVariable: visit_type_variable((TypeVariable*)node); return;
    case TypeTag::FunctionType: visit_function_type((FunctionType*)node); return;
    case TypeTag::TypeVariableDecl: visit_type_variable_decl((TypeVariableDecl*)node); return;
    case TypeTag::ImportDecl: visit_import_decl((ImportDecl*)node); return;
    case TypeTag::ClassDecl: visit_class_decl((ClassDecl*)node); return;
    case TypeTag::InterfaceDecl: visit_interface_decl((InterfaceDecl*)node); return;
    case TypeTag::FunctionDecl: visit_function_decl((FunctionDecl*)node); return;
    case TypeTag::VariableDecl: visit_variable_decl((VariableDecl*)node); return;
    case TypeTag::File: visit_file((File*)node); return;
    }
  }
  virtual void visit_string_expr(StringExpr const*) { }
  virtual void visit_number_expr(NumberExpr const*) { }
  virtual void visit_fp_number_expr(FpNumberExpr const*) { }
  virtual void visit_ident_expr(IdentExpr const*) { }
  virtual void visit_binary_expr(BinaryExpr const*) { }
  virtual void visit_unary_expr(UnaryExpr const*) { }
  virtual void visit_function_expr(FunctionExpr const*) { }
  virtual void visit_function_call_expr(FunctionCallExpr const*) { }
  virtual void visit_subscript_expr(SubscriptExpr const*) { }
  virtual void visit_member_expr(MemberExpr const*) { }
  virtual void visit_match_expr(MatchExpr const*) { }
  virtual void visit_array_expr(ArrayExpr const*) { }
  virtual void visit_dict_expr(DictExpr const*) { }
  virtual void visit_for_expr(ForExpr const*) { }
  virtual void visit_if_expr(IfExpr const*) { }
  virtual void visit_while_expr(WhileExpr const*) { }
  virtual void visit_new_expr(NewExpr const*) { }
  virtual void visit_cast_expr(CastExpr const*) { }
  virtual void visit_pointer_to_expr(PointerToExpr const*) { }
  virtual void visit_dereference_expr(DereferenceExpr const*) { }
  virtual void visit_compound_stmt(CompoundStmt const*) { }
  virtual void visit_pass_stmt(PassStmt const*) { }
  virtual void visit_return_stmt(ReturnStmt const*) { }
  virtual void visit_assignment_stmt(AssignmentStmt const*) { }
  virtual void visit_expr_stmt(ExprStmt const*) { }
  virtual void visit_let_stmt(LetStmt const*) { }
  virtual void visit_typename(Typename const*) { }
  virtual void visit_type_variable(TypeVariable const*) { }
  virtual void visit_function_type(FunctionType const*) { }
  virtual void visit_type_variable_decl(TypeVariableDecl const*) { }
  virtual void visit_import_decl(ImportDecl const*) { }
  virtual void visit_class_decl(ClassDecl const*) { }
  virtual void visit_interface_decl(InterfaceDecl const*) { }
  virtual void visit_function_decl(FunctionDecl const*) { }
  virtual void visit_variable_decl(VariableDecl const*) { }
  virtual void visit_file(File const*) { }
  virtual void visit_nullptr() { }
};

class Printer : public Visitor {
  std::ostream& os;
public:
  Printer(std::ostream& os) : os(os) { }
  void visit_string_expr(StringExpr const* node) {
    (void)node;
    os << "StringExpr(";
    os << node->str;
    os << ")";
  }
  void visit_number_expr(NumberExpr const* node) {
    (void)node;
    os << "NumberExpr(";
    os << node->num;
    os << ")";
  }
  void visit_fp_number_expr(FpNumberExpr const* node) {
    (void)node;
    os << "FpNumberExpr(";
    os << node->num;
    os << ")";
  }
  void visit_ident_expr(IdentExpr const* node) {
    (void)node;
    os << "IdentExpr(";
    os << node->name;
    os << ")";
  }
  void visit_binary_expr(BinaryExpr const* node) {
    (void)node;
    os << "BinaryExpr(";
    os << node->op;
    os << ", ";
    visit(node->lhs);
    os << ", ";
    visit(node->rhs);
    os << ")";
  }
  void visit_unary_expr(UnaryExpr const* node) {
    (void)node;
    os << "UnaryExpr(";
    os << node->op;
    os << ", ";
    visit(node->expr);
    os << ")";
  }
  void visit_function_expr(FunctionExpr const* node) {
    (void)node;
    os << "FunctionExpr(";
    visit(node->type);
    os << ", ";
    visit(node->body);
    os << ")";
  }
  void visit_function_call_expr(FunctionCallExpr const* node) {
    (void)node;
    os << "FunctionCallExpr(";
    visit(node->func);
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->args.size(); i++) { visit(node->args[i]); if (i != node->args.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_subscript_expr(SubscriptExpr const* node) {
    (void)node;
    os << "SubscriptExpr(";
    visit(node->array);
    os << ", ";
    visit(node->index);
    os << ")";
  }
  void visit_member_expr(MemberExpr const* node) {
    (void)node;
    os << "MemberExpr(";
    visit(node->target);
    os << ", ";
    os << node->member;
    os << ")";
  }
  void visit_match_expr(MatchExpr const* node) {
    (void)node;
    os << "MatchExpr(";
    os << node->op;
    os << ", ";
    visit(node->dst);
    os << ", ";
    visit(node->src);
    os << ")";
  }
  void visit_array_expr(ArrayExpr const* node) {
    (void)node;
    os << "ArrayExpr(";
    os << "["; for (unsigned i = 0; i < node->elems.size(); i++) { visit(node->elems[i]); if (i != node->elems.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_dict_expr(DictExpr const* node) {
    (void)node;
    os << "DictExpr(";
    os << "["; for (unsigned i = 0; i < node->keys.size(); i++) { visit(node->keys[i]); if (i != node->keys.size() - 1) os << ", "; } os << "]";
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->values.size(); i++) { visit(node->values[i]); if (i != node->values.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_for_expr(ForExpr const* node) {
    (void)node;
    os << "ForExpr(";
    os << "["; for (unsigned i = 0; i < node->vars.size(); i++) { os << node->vars[i]; if (i != node->vars.size() - 1) os << ", "; } os << "]";
    os << ", ";
    visit(node->seq);
    os << ", ";
    visit(node->body);
    os << ")";
  }
  void visit_if_expr(IfExpr const* node) {
    (void)node;
    os << "IfExpr(";
    visit(node->cond);
    os << ", ";
    visit(node->tstmt);
    os << ", ";
    visit(node->fstmt);
    os << ")";
  }
  void visit_while_expr(WhileExpr const* node) {
    (void)node;
    os << "WhileExpr(";
    visit(node->cond);
    os << ", ";
    visit(node->body);
    os << ")";
  }
  void visit_new_expr(NewExpr const* node) {
    (void)node;
    os << "NewExpr(";
    visit(node->type);
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->args.size(); i++) { visit(node->args[i]); if (i != node->args.size() - 1) os << ", "; } os << "]";
    os << ", ";
    visit(node->lifetime);
    os << ")";
  }
  void visit_cast_expr(CastExpr const* node) {
    (void)node;
    os << "CastExpr(";
    visit(node->type);
    os << ", ";
    visit(node->arg);
    os << ")";
  }
  void visit_pointer_to_expr(PointerToExpr const* node) {
    (void)node;
    os << "PointerToExpr(";
    visit(node->expr);
    os << ")";
  }
  void visit_dereference_expr(DereferenceExpr const* node) {
    (void)node;
    os << "DereferenceExpr(";
    visit(node->expr);
    os << ")";
  }
  void visit_compound_stmt(CompoundStmt const* node) {
    (void)node;
    os << "CompoundStmt(";
    os << "["; for (unsigned i = 0; i < node->stmts.size(); i++) { visit(node->stmts[i]); if (i != node->stmts.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_pass_stmt(PassStmt const* node) {
    (void)node;
    os << "PassStmt(";
    os << ")";
  }
  void visit_return_stmt(ReturnStmt const* node) {
    (void)node;
    os << "ReturnStmt(";
    visit(node->expr);
    os << ")";
  }
  void visit_assignment_stmt(AssignmentStmt const* node) {
    (void)node;
    os << "AssignmentStmt(";
    os << node->op;
    os << ", ";
    visit(node->dst);
    os << ", ";
    visit(node->src);
    os << ")";
  }
  void visit_expr_stmt(ExprStmt const* node) {
    (void)node;
    os << "ExprStmt(";
    visit(node->expr);
    os << ")";
  }
  void visit_let_stmt(LetStmt const* node) {
    (void)node;
    os << "LetStmt(";
    os << node->name;
    os << ", ";
    visit(node->type);
    os << ", ";
    visit(node->init);
    os << ")";
  }
  void visit_typename(Typename const* node) {
    (void)node;
    os << "Typename(";
    os << node->module;
    os << ", ";
    os << node->name;
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->params.size(); i++) { visit(node->params[i]); if (i != node->params.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_type_variable(TypeVariable const* node) {
    (void)node;
    os << "TypeVariable(";
    os << node->name;
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->params.size(); i++) { visit(node->params[i]); if (i != node->params.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_function_type(FunctionType const* node) {
    (void)node;
    os << "FunctionType(";
    visit(node->rettype);
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->argnames.size(); i++) { os << node->argnames[i]; if (i != node->argnames.size() - 1) os << ", "; } os << "]";
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->argtypes.size(); i++) { visit(node->argtypes[i]); if (i != node->argtypes.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_type_variable_decl(TypeVariableDecl const* node) {
    (void)node;
    os << "TypeVariableDecl(";
    os << node->name;
    os << ", ";
    visit(node->interface);
    os << ")";
  }
  void visit_import_decl(ImportDecl const* node) {
    (void)node;
    os << "ImportDecl(";
    os << node->name;
    os << ")";
  }
  void visit_class_decl(ClassDecl const* node) {
    (void)node;
    os << "ClassDecl(";
    os << node->name;
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->decorators.size(); i++) { visit(node->decorators[i]); if (i != node->decorators.size() - 1) os << ", "; } os << "]";
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->params.size(); i++) { visit(node->params[i]); if (i != node->params.size() - 1) os << ", "; } os << "]";
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->decls.size(); i++) { visit(node->decls[i]); if (i != node->decls.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_interface_decl(InterfaceDecl const* node) {
    (void)node;
    os << "InterfaceDecl(";
    os << node->name;
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->decorators.size(); i++) { visit(node->decorators[i]); if (i != node->decorators.size() - 1) os << ", "; } os << "]";
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->params.size(); i++) { visit(node->params[i]); if (i != node->params.size() - 1) os << ", "; } os << "]";
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->decls.size(); i++) { visit(node->decls[i]); if (i != node->decls.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  void visit_function_decl(FunctionDecl const* node) {
    (void)node;
    os << "FunctionDecl(";
    os << node->name;
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->decorators.size(); i++) { visit(node->decorators[i]); if (i != node->decorators.size() - 1) os << ", "; } os << "]";
    os << ", ";
    visit(node->type);
    os << ", ";
    visit(node->body);
    os << ")";
  }
  void visit_variable_decl(VariableDecl const* node) {
    (void)node;
    os << "VariableDecl(";
    os << node->name;
    os << ", ";
    visit(node->type);
    os << ", ";
    visit(node->init);
    os << ")";
  }
  void visit_file(File const* node) {
    (void)node;
    os << "File(";
    os << node->filename;
    os << ", ";
    os << node->short_name;
    os << ", ";
    os << "["; for (unsigned i = 0; i < node->decls.size(); i++) { visit(node->decls[i]); if (i != node->decls.size() - 1) os << ", "; } os << "]";
    os << ")";
  }
  virtual void visit_nullptr() { os << "null"; }
};
