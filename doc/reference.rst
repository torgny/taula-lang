
Lexical elements
################

This chapter describes the lexical elements that make up Taula source code.
These elements are called tokens. There are five types of tokens: keywords,
identifiers, constants, operators, and separators. White space, sometimes
required to separate tokens and denote code blocks through indentation, is
also described in this chapter.

Identifiers
===========

Identifiers are sequences of characters used for naming variables, functions,
and types. You can include letters, decimal digits, and the underscore
character ``_`` in identifiers. The first character of an identifier cannot be
a digit. Lowercase letters and uppercase letters are distinct, such that
``foo`` and ``FOO`` are two different identifiers.

There are two orthogonal properties of an identifier: public vs. private and
type vs non-type identifier. These properties are encoded in the identifier
itself.

Public and private identifiers
------------------------------

Public identifiers must start with a non-underscore character and private
identifiers must start with underscore. Private identifiers aren't imported by
default, they have to be explicitly imported using ``import "fish.tau" :
_AtlanticSalmon, _ArcticChar``. Example, these are private identifiers:

    ``_my_private_function``, ``_MyPrivateClass``

Example, these are public identifiers:

    ``my_public_function``, ``MyPublicClass``

**Rationale:**
  This is a lightweight and practical rule in that it possible to
  intentionally circumvent (e.g., for testing or debugging), hard to
  accidentally misuse, and easy to search for.

Type and non-type identifiers
-----------------------------

A type identifier is a name of a type (class or interface). These must contain
at least one lowercase letter, start with an uppercase letter (except for
builtin types, e.g., ``int``, see Builtins), or start with an underscore
followed by an uppercase letter. A single uppercase character is also a type
identifier. Example:

        ``int``, ``Tree0``, ``_List``, ``_ABCd``, ``T``

Non-type (variable and function) identifiers must start with a lowercase
letter or an underscore followed by a lowercase letter and must not be a name
of a builtin type. Identifiers that consists only of uppercase characters are
also non-type identifiers. Example:

       ``tree``, ``list1``, ``_aBCD``, ``T0``

**Rationale:**
  This lexical rule on type names avoid the need for arbitrary long look-ahead
  when parsing to disambiguates certain code. For instance, without this rule
  would the phrase ``foo<bar.baz`` be an expression or the start of a type
  name?

All types with special syntax, i.e., pointers, arrays, and function types, is
purely syntactic sugar for an underlying built-in class. For example
``*[]int`` (pointer to array of int) is equivalent to
``__builtins__._Pointer<__builtins__._Array<__builtins__.int>>``. Note that
``_Pointer`` and ``_Array`` are private classes of the ``__builtins__`` module
and aren't imported by default while `int` is.

Keywords
========

Keywords are part of the language definition, and cannot be used as
identifiers. This is the list of keywords:

    and break case cast class const continue else enum for if interface let new not or return while

Constants
=========

A constant is a literal numeric, string, or character value, such as ``17`` or
``'p'``. All constants are have a certain type. You can use type casting to
explicitly specify the type of a constant.

Integer Constants
-----------------

An integer constant is a sequence of digits, with an optional prefix to denote
a number base. The underscore character, ``_``, is allowed as a separator and
is ignored. Integer constants are of the built-in type ``int``.

If the sequence of digits is preceded by ``0x`` or ``0X``, then the constant
is considered to be hexadecimal (base 16). Hexadecimal values may use the
digits from ``0`` to ``9``, as well as the letters ``a`` to ``f`` and ``A`` to
``F``. Here are some examples::

    0x2f
    0x8_8
    0xAB43
    0xAb_Cd
    0X1

If the first digit is ``0``, and the next character is ``o`` or ``O``, then
the constant is considered to be octal (base 8). Octal values may only use the
digits from ``0`` to ``7``; ``8`` and ``9`` are not allowed. Here are some
examples::

    0o5_7
    0o12
    0o_3
    0O241

If the first digit is ``0``, and the next character is ``b`` or ``B``, then
the constant is considered to be binary (base 2). Octal values may only use
the digits from ``0`` and ``1``; remaining digits are not allowed. Here are
some examples::

    0b1
    0b101_011
    0b0
    0B1

In all other cases, the sequence of digits is assumed to be decimal (base 10).
Decimal values may use the digits from 0 to 9. Here are some examples::

    459
    23_901
    8
    12

Character Constants
-------------------

A character constant is usually a single character enclosed within single
quotation marks, such as ``'Q'``. A character constant is of the built-in type
``char``.

Some characters, such as the single quotation mark character itself, cannot be
represented using only one character. To represent such characters, there are
several “escape sequences” that you can use:

    \\\\ Backslash character.

    \\? Question mark character.

    \\' Single quotation mark.

    \\" Double quotation mark.

    \\a Audible alert.

    \\b Backspace character.

    \\e <ESC> character.

    \\f Form feed.

    \\n Newline character.

    \\r Carriage return.

    \\t Horizontal tab.

    \\v Vertical tab.

    \\o, \\oo, \\ooo Octal number.

    \\xh, \\xhh, \\xhhh, ... Hexadecimal number.

To use any of these escape sequences, enclose the sequence in single quotes,
and treat it as if it were any other character. For example, the letter ``m``
is ``'m'`` and the newline character is ``'\n'``.

The octal number escape sequence is the backslash character followed by one,
two, or three octal digits (0 to 7). For example, 101 is the octal equivalent
of 65, which is the ASCII character 'A'. Thus, the character constant '\101'
is the same as the character constant 'A'.

The hexadecimal escape sequence is the backslash character, followed by x and
an unlimited number of hexadecimal digits (0 to 9, and a to f or A to F).

Real Number Constants
---------------------

A real number constant is a value that represents a fractional (floating
point) number. It consists of a sequence of digits which represents the
integer (or “whole”) part of the number, a decimal point, and a sequence of
digits which represents the fractional part.

Either the integer part or the fractional part may be omitted, but not both.
Here are some examples::

    let a, b double
    a = 4.7
    b = 4.0

Real number constants can also be followed by e or E, and an integer exponent.
The exponent can be either positive or negative. ::

    let x, y double
    x = 5e2   # x is 5 * 100, or 500.0.
    y = 5e-2  # y is 5 * (1/100), or 0.05.

String Constants
----------------

A string constant is a sequence of zero or more characters, digits, and escape
sequences enclosed within double quotation marks. A string constant is of type
“array of characters”. [TODO] All string constants contain a null termination
character (\0) as their last character. Strings are stored as arrays of
characters, with no inherent size attribute. The null termination character
lets string-processing functions know where the string ends.

Adjacent string constants are concatenated (combined) into one string, with
the null termination character added to the end of the final concatenated
string.

A string cannot contain double quotation marks, as double quotation marks are
used to enclose the string. To include the double quotation mark character in
a string, use the \" escape sequence. You can use any of the escape sequences
that can be used as character constants in strings. Here are some example of
string constants::

    # This is a single string constant.
    "tutti frutti ice cream"

    # These string constants will be concatenated, same as above.
    "tutti " "frutti" " ice " "cream"

    # This one uses two escape sequences.
    "\"hello, world!\""

If a string is too long to fit on one line, you can use a backslash \ to break
it up onto separate lines. ::

    "Today's special is a pastrami sandwich on rye bread with \
    a potato knish and a cherry soda."

Adjacent strings are automatically concatenated, so you can also have string
constants span multiple lines by writing them as separate, adjacent, strings.
For example::

    "Tomorrow's special is a corned beef sandwich on "
    "pumpernickel bread with a kasha knish and seltzer water."

is the same as ::

    "Tomorrow's special is a corned beef sandwich on \
    pumpernickel bread with a kasha knish and seltzer water."

To insert a newline character into the string, so that when the string is
printed it will be printed on two different lines, you can use the newline
escape sequence ‘\n’. ::

    print("potato\nknish")

prints ::

    potato
    knish

Operators
---------

An operator is a special token that performs an operation, such as addition or
subtraction, on either one, or two. Full coverage of operators can be found in
a later chapter. See Expressions and Operators.

Separators
----------

A separator separates tokens. White space (see next section) is special a
separator. The other separators are all single-character tokens themselves:

    ( ) [ ] { } ; , . :

Of these tokens, semicolon (;) is special in that it can be left out and
implicitly be inserted by the compiler. It is inserted after the following
tokens if they are followed by a newline:

* an identifier
* an integer, floating-point, or string literal
* one of the keywords `break`, `continue`, `pass` or `return`
* one of the delimiters `)`, `]`, or `}`

Whitespace
----------

Whitespace is the collective term used for several characters: the space
character, the tab character, the newline character, the vertical tab
character, and the form-feed character. Whitespace is ignored (outside of
string and character constants), and is therefore optional, except when it is
used to separate tokens and indenting code blocks. This means that ::

    def main() int:
        print("hello, world")
        return 0

and ::

    def main(       )int:
        print    (    "hello, world\n"    )
        return      0

are functionally the same program.

Although you must use whitespace to separate many tokens, no whitespace is
required between operators and operands, nor is it required between other
separators and that which they separate. ::

    # All of these are valid.
    x+=1
    x += 1
    x=y+z
    x = y + z
    x=array[2]
    x = array [ 2 ]
    fraction=numerator/*denominator_ptr
    fraction = numerator / * denominator_ptr

Furthermore, wherever one space is allowed, any amount of whitespace is
allowed, except for indentation. ::

    # These two statements are functionally identical.
    x+=1
    x       +=          1

In string constants, spaces and tabs are not ignored; rather, they are part of
the string. Therefore, ::

    "potato knish"

is not the same as ::

    "potato                        knish"

Data Types
##########

Primitive data types
====================

Integer types
-------------

The integer data types range in size from 8 bits to 64 bits. You should use
integer types for storing whole number values (and the char data type for
storing characters). Signed integers use two's-complement.

int8
    The 8-bit signed integer type can hold integer values in the range of -128 to 127.

uint8 
    The 8-bit unsigned integer data type can hold integer values in the range of 0 to 255.

int16
    The 16-bit integer data type can hold integer values in the range of -32,768 to 32,767.

uint16
    The 16-bit unsigned integer data type can hold integer values in the range of 0 to 65,535.

int
    alias for `int32`.

int32
    The 32-bit integer data type can hold integer values in the range of -2,147,483,648 to 2,147,483,647. You may also refer to this data type as int.

uin32
    The 32-bit unsigned integer data type can hold integer values in the range of 0 to 4,294,967,295.

int64
    The 64-bit integer data type can hold integer values in the range of -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807.

uint64
    The 64-bit unsigned long long int data type can hold integer values in the range of at least 0 to 18,446,744,073,709,551,615. [TODO: refer to int64 as long?]

Here are some examples of declaring and defining integer variables::

    let foo int
    let bar uint16 = 42

The first line declares an integer named ``foo`` but does not define its value; it is implicitly initialized to zero.

Floating-point types
--------------------

The floating-point types are ``float`` and ``double``, which is the single-
precision 32-bit and double-precision 64-bit format IEEE 754, respectively.
Values and operations of these types are specified in IEEE Standard for Binary
Floating- Point Arithmetic, ANSI/IEEE Standard 754-1985 (IEEE, New York).

The IEEE 754 standard includes not only positive and negative numbers that
consist of a sign and magnitude, but also positive and negative zeros,
positive and negative infinities, and special Not-a-Number values (hereafter
abbreviated NaN). A NaN value is used to represent the result of certain
invalid operations such as dividing zero by zero. The NaN constants of both
float and double type are predefined as ``float.NaN`` and ``double.NaN``.

[TODO: insert table of range and precision]

Here are some examples::

    let foo double
    let bar = 114.3943

The first line declares a float named ``foo`` but does not define its value; it is
implicitly initialized to ``0.0``.

The real number types provided are of finite precision, and accordingly, not
all real numbers can be represented exactly, for example, 4.2. For this
reason, we recommend that you consider not comparing real numbers for exact
equality with the ``==`` operator, but rather check that real numbers are
within an acceptable tolerance.

There are other more subtle implications of these imprecise representations;
for more details, see David Goldberg’s paper What Every Computer Scientist
Should Know About Floating-Point Arithmetic and section 4.2.2 of Donald
Knuth’s The Art of Computer Programming.

Character type
--------------

char
    TODO write this


Enumerations
============

Unions
======


Classes
=======

A class is a user-defined data type made up of variables of other data types.
A class is also a namespace in which functions can be defined. [TODO: describe
what we mean with a namespace]

Defining classes
----------------

You define a class using the ``class`` keyword followed by the name of the
class followed by the declarations of the class’s members, enclosed by an
indented code block. You declare each member of a class just as you would
normally declare a variable using the ``let`` keyword . The class definition
ends when the indented block ends.

Here is an example of defining a simple class for holding the X and Y
coordinates of a point::

    class Point:
        let x, y int

That defines a class named ``Point``, which contains two members, ``x`` and ``y``, both
of which are of the built-in type ``int``.

Classes may contain instances of other classes, but of course not themselves.
It is possible for a class to contain a field which is a pointer to the same
type.

Accessing class members
-----------------------

You can access the members of a class variable using the member access
operator ``.``. You put the name of the class variable on the left side of the
operator, and the name of the member on the right side. ::

    class Point:
        let x, y int

    let first_point Point

    first_point.x = 0
    first_point.y = 5

You can also access the members of a class variable which is itself a member
of a class variable. ::

    class Rectangle:
        let top_left, bottom_right Point

    let my_rectangle Rectangle

    my_rectangle.top_left.x = 0
    my_rectangle.top_left.y = 5

    my_rectangle.bottom_right.x = 10
    my_rectangle.bottom_right.y = 0


Defining member functions
-------------------------

A class may have zero or more member function defined in its body. A member
function is defined just like functions defined in file scope using the
``def`` keyword. For example::

    class Rectangle:
        let top_left, bottom_right Point
    
        def area(self *Rectangle) int:
            let w = self.bottom_right.x - self.top_left.x
            let h = self.bottom_right.y - self.top_left.y
            return w * h
    ...
    print(my_rectangle.area())


Member functions is special in that they can be called through the
``receiver.func(arg)`` syntax. When called in this manner ``receiver`` is
bound to the first argument, called ``self`` by convention. Member functions
are plain functions just like functions defined in file scope, the difference
lies only in how they are (usually) called. It is however legal to call member
functions using the syntax ``func(receiver, arg)`` as well::

    print(Rectangle.area(my_rectangle))

Generic classes
---------------

A class is generic if it has zero or more *type parameter* defined inside angle
brackets. Example::

    class List<T object>:
        let head T
        let tail *List<T>

Strictly speaking a non-generic class is simply a generic class with zero type
parameters. In the example below, the definitions of ``A`` and ``B`` are
equivalent (except for name)::

    class A<>:
        pass

    class B:
        pass

Type specialization
-------------------

A generic class can be specialized by providing a more specific class for it's
type parameters. This is done by appending the values for the type parameters
inside angle brackets after the generic class' name. Example::

    let i List<int>
    let f Fish<float, List<int>>

Since non-generic classes are simply generic classes with zero type parameters,
non-generic classes can strictly speaking also be specialized. However, as there
are no type parameters to narrow, the parameter list inside the angle brackets
is empty::

    let i int<>    # Equivalent to "let i int"
    let f float<>  # Equivalent to "let f float"


Size of classes
---------------

The size of a class type is equal to the sum of the size of all of its
members, possibly including padding to cause the structure type to align to a
particular byte boundary. The details vary depending on your computer
platform, but it would not be atypical to see structures padded to align on
four- or eight-byte boundaries. This is done in order to speed up memory
accesses of instances of the structure type.

The members are automatically reordered as to minimize padding. This can be
disabled by annotating the class with ``@structure``

A class without any members have zero size.

If you wish to explicitly omit padding from your class (which may, in turn,
decrease the speed of class memory accesses), annotate the class with
``@packed``

Arrays
======
[TODO: merge this with the Pointer chapter]

An array is a data structure that lets you store one or more elements
consecutively in memory. Array elements are indexed beginning at position
zero, not one.

Declaring arrays
----------------

You declare an array by specifying the data type for its elements, and it
name. The size of the array (number of arguments) is not part of the type, but
part of it's runtime information. Here is an example that declares an array
that can store ten integers::

    let my_array []int = new int[10]

Initializing arrays
-------------------

You can initialize the elements in an array when you declare it by listing the
initializing values, separated by commas, enclosed in brackets. Here is an
example::

    let my_array []int = [0, 1, 2, 3, 4]

Accessing array elements
------------------------

You can access the elements of an array by specifying the array name, followed
by the element index, enclosed in brackets. Remember that the array elements
are numbered starting with zero. Here is an example::

    my_array[0] = 5

That assigns the value ``5`` to the first element in the array, at position zero.
You can treat individual array elements like variables of whatever data type
the array is made up of. For example, if you have an array made of a class
data type, you can access the structure elements like this::

    class Point:
        let x, y int

    let point point_array []Point = [ Point(4, 5), Point(8, 9) ]
    point_array[0].x = 3

Arrays of objects
-----------------

You can create an array of a objects just as you can an array of a
primitive data type. ::

    class Point:
        let x, y int

    let point_array []Point = new Point[3]

That example creates a 3-element array of struct point variables called
``point_array``. You can also initialize the elements of a structure array::

    let point_array []Point = [ Point(2, 3), Point(4, 5), Point(6, 7) ];

After initialization, you can still access the members in the array
using the member access operator. You put the array name and element number
(enclosed in brackets) to the left of the operator, and the member name to the
right. ::

    let point point_array []Point = new Point[3];
    point_array[0].x = 2
    point_array[0].y = 3

Pointers
========

Pointers hold memory addresses of stored constants or variables. For any data
type, including both primitive types and user-defined types, you can create a
pointer that holds the memory address of an instance of that type.

Declaring pointers
------------------

You declare a pointer by specifying a name for it and a data type. The data
type indicates of what type of variable the pointer will hold memory
addresses.

To declare a pointer, include the indirection operator (see Pointer Operators)
before the data type. Here is an example of declaring a pointer to hold the address of an ``int`` variable::

    let ip *int

Initializing pointers
---------------------

You can initialize a pointer when you first declare it by specifying a
variable address to store in it. For example, the following code declares an
``int`` variable ``i``, and a pointer variable ``ip`` which is initialized with the address of ``i``::

    let i int
    let ip *int = &i

Note the use of the address operator (see Pointer Operators), used to get the
memory address of a variable. After you declare a pointer, you do not use the
indirection operator with the pointer’s name when assigning it a new address
to point to. On the contrary, that would change the value of the variable that
the points to, not the value of the pointer itself. For example::

    let i, j int
    let ip *int = &i # ‘ip’ now holds the address of ‘i’.
    ip = &j          # ‘ip’ now holds the address of ‘j’.
    *ip = &i         # ‘j’ now holds the address of ‘i’.

It is important to note that if you do not initialize a pointer with the
address of some other existing object, it is initialized zero (the null
pointer). Only non-null pointers are legal to dereference, for instance, the
following code gives an error::

    let ip *int     # Default initialized to null.
    let i int = *ip # Error: dereferencing null.

Similarly, it's illegal to return a (potentially) null pointer or passing it
to a function.

Pointers to structures
----------------------

You can create a pointer to a structure type just as you can a pointer to a
primitive data type. ::

    class Fish:
        let length, weight float

    let salmon Fish = new Fish(4.3, 5.8);
    let fish_ptr *Fish = &salmon;

You can access the members of an object through a pointer using the
regular member access operator. Continuing with the previous example, the
following example will change the values of the members of salmon::

    fish_ptr.length = 5.1;
    fish_ptr.weight = 6.2;

Now the length and width members in salmon are ``5.1`` and ``6.2``, respectively.

Null pointer
------------

If you do not initialize a pointer with the address of some other existing
object it is initialized to the null pointer, denoted ``null``. In the example
below, ``ap`` and ``bp`` are both initialized to the null pointer::

    let ap *int
    let bp *int = null

To ensure that the null pointer is never dereferenced, it is a compile-time error to:

* pass *e* as argument in a function call
* return *e* from a function 
* dereference *e*

where *e* is an expressions that is null in at least one control-flow path through a function. 

**Rationale:**
  This enables local reasoning for humans and compilers. It handles the
  special case of `__init__` which has to initialize object members of pointer
  type. This also allows constructing an array of pointers and initialize its
  members after allocation. The downside is that there are cases when
  refactoring out a function requires some extra thinking.

Function calls
##############

Pass by value
=============

Arguments of every type is passed by value, meaning the value of the variable is copied from the caller to the callee at every call site. Variables of pointer type is also passed by value in the sense that the value of the pointer is copied to the callee.

Semantically, variables holding large values (for instance large arrays) are passed by value, but the compiler is required perform *Argument Copy Elision* (see below).

Argument copy elision
---------------------

Argument Copy Elision is an optimization that is mandatory for a compiler to implement. The optimization is to copy pass-by-value function arguments lazily and only when required. In the following situations, copying a pass-by-value function argument is required:

* any field of the object is assigned to,
* taking the object's address through pass-by-pointer,
* taking its address through ``&foo``, or
* taking the address of any of its members
* the object's size is less than some platform dependent constant

However, in all other situations Argument Copy Elision must take place.

Intuitively, an object is considered to be written to if the compiler sees an
assignment to any of its field, or if the compiler lose track of the object by
taking a pointer to it. If this happens, the compiler inserts an implicit copy
of the object just before the assignment/pointer escape.

Note that if a compiler can guarantee that the object isn't mutated through
any other means than the above rules, the compiler is free to elide argument
copying. The above rules states what a compiler is *required* to do. ::

    class A:
        let foo int

    class B:
        let a *A

    def g(a A):
        let b = B(a) # A copy of 'a' is made
        f(a, b)

   def f(a A, b B):
       b.a.foo = 20
       print(a.foo) # will read value assigned above...


Pass by pointer
===============

An object should be passed by pointer if (and only if) the object is intended
to be mutated by the called function. Otherwise, the object is intended to not
be mutated, the object should be passed by value.


Expressions and Operators
#########################

Expressions
===========

An expression consists of at least one operand and zero or more operators.
Operands are typed entities such as constants, variables, and function calls
that return values. Here are some examples::

    47
    2 + 2
    cos(3.14159) # We presume this returns a floating point value.

Parentheses group subexpressions::

    (2 * ((3 + 10) - (2 * 6)))

Innermost expressions are evaluated first. In the above example, ``3 + 10``
and ``2 * 6`` evaluate to ``13`` and ``12``, respectively. Then ``12`` is
subtracted from ``13``, resulting in ``1``. Finally, ``1`` is multiplied by
``2``, resulting in ``2``. The outermost parentheses are completely optional.

An operator specifies an operation to be performed on its operand(s).
Operators may have one, two, or three operands, depending on the operator.

Assignment Operators
====================

Assignment operators store values in variables. There are several variations
of assignment operators provided.

The standard assignment operator ``=`` simply stores the value of its right
operand in the variable specified by its left operand. As with all assignment
operators, the left operand (commonly referred to as the *lvalue*) cannot be a
literal or constant value. ::

    let x int = 10
    let y float = 45.12 + 2.0
    let z int = (2 * (3 + function()));

    class Foo:
        let bar, baz int
    let quux Foo = Foo(3, 4)

Compound assignment operators perform an operation involving both the left and
right operands, and then assign the resulting expression to the left operand.
Here is a list of the compound assignment operators, and a brief description
of what they do:

    += Adds the two operands together, and then assign the result of the addition to the left operand.

    -= Subtract the right operand from the left operand, and then assign the result of the subtraction to the left operand.

    \*= Multiply the two operands together, and then assign the result of the multiplication to the left operand.

    /= Divide the left operand by the right operand, and assign the result of the division to the left operand.

    %= Perform modular division on the two operands, and assign the result of the division to the left operand.

    <<= Perform a left shift operation on the left operand, shifting by the number of bits specified by the right operand, and assign the result of the shift to the left operand.

    >>= Perform a right shift operation on the left operand, shifting by the number of bits specified by the right operand, and assign the result of the shift to the left operand.

    &= Perform a bitwise conjunction operation on the two operands, and assign the result of the operation to the left operand.

    ^= Performs a bitwise exclusive disjunction operation on the two operands, and assign the result of the operation to the left operand.

    \|= Performs a bitwise inclusive disjunction operation on the two operands, and assign the result of the operation to the left operand.

Here is an example of using one of the compound assignment operators::

    x += y

Since there are no side effects wrought by evaluating the variable ``x`` as an
lvalue, the above code produces the same result as::

    x = x + y

Arithmetic Operators
====================

There are operators provided for standard arithmetic operations: addition,
subtraction, multiplication, and division, along with modular division and
negation. Usage of these operators is straightforward; here are some
examples::

    # Addition.
    x = 5 + 3
    y = 10.23 + 37.332

    # Subtraction.
    x = 5 - 3
    y = 57.223 - 10.903

    # Multiplication.
    x = 5 * 3
    y = 47.4 * 1.001

    # Division.
    x = 5 / 3
    y = 940.0 / 20.2

Integer division of positive values truncates towards zero, so ``5/3`` is
``1``. However, [TODO] if either operand is negative, the direction of
rounding is implementation-defined. See Signed Integer Division for
information about overflow in signed integer division.

You use the modulus operator ``%`` to obtain the remainder produced by
dividing its two operands. You put the operands on either side of the
operator, and it does matter which operand goes on which side: ``3 % 5`` and
``5 % 3`` do not have the same result. ::

    # Modular division.
    x = 5 % 3
    y = 74 % 47

Modular division returns the remainder produced after performing integer
division on the two operands. The operands must be of a primitive integer
type. ::

    # Negation.
    let x int = -5
    let y float = -3.14159

[TODO] If the operand you use with the negative operator is of an unsigned
data type, then the result cannot negative, but rather is the maximum value
of the unsigned data type, minus the value of the operand.

Comparison operators
====================

You use the comparison operators to determine how two operands relate to each
other: are they equal to each other, is one larger than the other, is one
smaller than the other, and so on. When you use any of the comparison
operators, the result is either ``true`` or ``false``.

(In the following two code examples, the variables ``x`` and ``y`` stand for
any two expressions of arithmetic types, or pointers.)

The equal-to operator ``==`` tests its two operands for equality. The result
is ``true`` if the operands are equal, and ``false`` if the operands are not
equal. ::

    if x == y:
        print("x is equal to y")
    else:
        print("x is not equal to y")

The not-equal-to operator ``!=`` tests its two operands for inequality. The
result is ``true`` if the operands are not equal, and ``false`` if the
operands are equal. ::

    if x != y:
        print("x is not equal to y")
    else:
        print("x is equal to y")

Comparing floating-point values for exact equality or inequality can produce
unexpected results. Real Number Types for more information.

You can compare function pointers for equality or inequality; the comparison
tests if two function pointers point to the same function or not.

Beyond equality and inequality, there are operators you can use to test if one
value is less than, greater than, less-than-or-equal-to, or greater-than-or-
equal-to another value. Below are some code samples that exemplify usage of
these operators. In these examples, ``x`` and ``y`` stand for any two
expressions of arithmetic types, but not pointers. ::

    if x < y:
        print("x is less than y")

    if x <= y:
        print("x is less than or equal to y")

    if x > y:
        print("x is greater than y")

    if x >= y:
        print("x is greater than or equal to y")

Logical operators
=================

Logical operators test the truth value of a pair of operands. Any nonzero
expression is considered ``true``, while an expression that evaluates to zero
is considered ``false``.

The logical conjunction operator ``and`` tests if two expressions are both
``true``. If the first expression is ``false``, then the second expression is
not evaluated. ::

    if x == 5 and y == 10:
        print("x is 5 and y is 10")

The logical disjunction operator ``or`` tests if at least one of two
expressions it ``true``. If the first expression is ``true``, then the second
expression is not evaluated. ::

    if x == 5 or  y == 10:
        print("x is 5 or y is 10")

You can prepend a logical expression with a negation operator ``not`` to flip
the truth value::

    if not x == 5:
        print("x is not 5")

Since the second operand in a logical expression pair is not necessarily
evaluated, you can write code with perhaps unintuitive results::

    if foo and gazonk():
        bar()

If ``foo`` is ever ``false``, then not only would ``bar`` not be called, but
neither would ``gazonk``. If you intend to call ``gazonk`` regardless of the
value of ``foo``, you should do so outside of the conjunction expression.

Bit Shifting
============

You use the left-shift operator ``<<`` to shift its first operand’s bits to
the left. The second operand denotes the number of bit places to shift. Bits
shifted off the left side of the value are discarded; new bits added on the
right side will all be ``0``. ::

    let x int = 47   # 47 is 00101111 in binary.
    x = x << 1       # 00101111 << 1 is 01011110.

Similarly, you use the right-shift operator ``>>`` to shift its first
operand’s bits to the right. Bits shifted off the right side are discarded;
new bits added on the left side are usually ``0``, but if the first operand is
a signed negative value, then the added bits will a copy of the sign bit (the
leftmost bit position). ::

    let x int = 47 # 47 is 00101111 in binary.
    x = x >> 1     # 00101111 >> 1 is 00010111.


For both ``<<`` and ``>>``, if the second operand is greater than the bit-
width of the first operand, or the second operand is negative, the behavior is
undefined.

TODO: give example of bit hack.

Bitwise logical operators
=========================

There are operators provided to perform bitwise conjunction, inclusive
disjunction, exclusive disjunction, and negation (complement).

Bitwise conjunction examines each bit in its two operands, and when two
corresponding bits are both ``1``, the resulting bit is ``1``. All other
resulting bits are ``0``. Here is an example of how this works, using binary
numbers::

    print(0b11001001 & 0b10011011) # 0b10001001

Bitwise inclusive disjunction examines each bit in its two operands, and when
two corresponding bits are both ``0``, the resulting bit is ``0``. All other
resulting bits are ``1``. ::

    print(0b11001001 | 0b10011011) # 0b11011011

Bitwise exclusive disjunction examines each bit in its two operands, and when
two corresponding bits are different, the resulting bit is ``1``. All other
resulting bits are ``0``. ::

    print(0b11001001 ^ 0b10011011) # 0b01010010

Bitwise negation reverses each bit in its operand::

    print(~0b11001001) # 0b00110110

Pointer operators
=================

You can use the address operator ``&`` to obtain the memory address of an object. ::

    let x int = 5
    let pointer_to_x *int = &x

[TODO describe how pointers are converted to values] Given a pointer, you can
use the indirection operator ``*`` to obtain the value stored at the address.
This is called dereferencing the pointer. ::

    let x int = 5
    let y int
    let ptr *int

    ptr = &x    # ptr now holds the address of x.
    y = *ptr    # y gets the value stored at the address stored in ptr.

Note that it's an compile error to (potentially) dereference a null pointer.

Type casts
==========

You can use a type cast to explicitly cause an expression to be of a specified
data type. A type cast consists of a the ``cast`` keyword, the type specifier,
followed by an expression in parentheses. Here is an example::

    let x float
    let y int = 7
    let z int = 3
    x = cast float(y / z)

In that example, since ``y`` and ``z`` are both integers, integer division is
performed, and even though ``x`` is a floating-point variable, it receives the
value ``2``. Explicitly casting the result of the division to float does no
good, because the computed value of ``y / z`` is already ``2``.

To fix this problem, you need to convert one of the operands to a floating-
point type before the division takes place::

    let x float
    let y int = 7
    let z int = 3
    x = cast float(y) / z

Here, a floating-point value close to ``2.333...`` is assigned to ``x``.

Type casting only works with scalar types (that is, integer, floating-point or
pointer types). Therefore, this is not allowed::

    class Foo:
        let i int
    let j int = 10
    let foo Foo = cast Foo(j)

A pointer can be cast to any other pointer::

    class Foo:
        let i int
    let j int = 10
    let foo *Foo = cast *Foo(&j)

Array subscripts
================

You can access array elements by specifying the name of the array, and the
array subscript (or index, or element number) enclosed in brackets. Here is an
example, supposing an integer array called my_array::

    my_array[0] = 5

Function calls as expressions
=============================

A call to any function is an expression. ::

    def function() int:
        return 10
    let a = 10 + function()

TODO: describe void

Member access dxpressions
=========================

You can use the member access operator ``.`` to access the members of an
object. You put the name of the object variable on the left side of the
operator, and the name of the member on the right side. ::

    class Point:
        let x, y int

    let first_point Point
    first_point.x = 0
    first_point.y = 5

You can also access the members of an object variable via a pointer
by using the member access operator. ::

    class Fish:
        let length, weight int

    let salmon Fish
    let fish_pointer *Fish = &salmon

    fish_pointer.length = 3
    fish_pointer.weight = 9

See Pointers.

Conditional expressions
=======================

You use the conditional operator to cause the entire conditional expression to
evaluate to either its second or its third operand, based on the truth value
of its first operand. Here’s an example::

    if a: b else: c

If expression ``a`` is ``true``, then expression ``b`` is evaluated and the
result is the value of ``b``. Otherwise, expression ``c`` is evaluated and the
result is ``c``.

Expressions ``b`` and `c` must have compatible types [TODO]. That is, they must
both be

* arithmetic types
* compatible class types
* pointers to compatible types

Here is an example::

    a = if x == 5: y else: z

Here, if ``x`` equals ``5``, then ``a`` will receive the value of ``y``.
Otherwise, ``a`` will receive the value of ``z``. This can be considered a
shorthand way for writing the following code::

    if x == 5:
        a = y
    else:
        a = z

If the first operand of the conditional operator is ``true``, then the third
operand is never evaluated. Similarly, if the first operand is false, then the
second operand is never evaluated. The first operand is always evaluated.

The two code blocks can be arbitrarily complex::

    let i = 10
    let x = if foo:
            let z = bar()
            z + gazonk()
        else:
            i = 8
            baz(i + x)

Operator Precedence
===================

When an expression contains multiple operators, such as ``a + b * f()``, the
operators are grouped based on rules of precedence. For instance, the meaning
of that expression is to call the function ``f`` with no arguments, multiply
the result by ``b``, then add that result to ``a``. That’s what the rules of
operator precedence determine for this expression.

The following is a list of types of expressions, presented in order of highest
precedence first. Sometimes two or more operators have equal precedence; all
those operators are applied from left to right unless stated otherwise.

+------------------------------------+--------------------------------------------+ 
| Operator                           | Description                                |
+====================================+============================================+
| fn                                 | Anonymous function expression              |
+------------------------------------+--------------------------------------------+ 
| if-else                            | Conditional expression                     |
+------------------------------------+--------------------------------------------+ 
| or                                 | Boolean OR                                 |
+------------------------------------+--------------------------------------------+ 
| and                                | Boolean AND                                |
+------------------------------------+--------------------------------------------+ 
| not x                              | Boolean NOT                                |
+------------------------------------+--------------------------------------------+ 
| <, <=, >, >=, !=, ==               | Comparisons                                |
+------------------------------------+--------------------------------------------+ 
| \|                                 | Bitwise OR                                 |
+------------------------------------+--------------------------------------------+ 
| ^                                  | Bitwise XOR                                |
+------------------------------------+--------------------------------------------+ 
| &                                  | Bitwise AND                                |
+------------------------------------+--------------------------------------------+ 
| <<, >>                             | Shifts                                     |
+------------------------------------+--------------------------------------------+ 
| +, -                               | Addition and subtraction                   |
+------------------------------------+--------------------------------------------+ 
| \*, /, //, %                       | Multiplication, division, remainder        |
+------------------------------------+--------------------------------------------+ 
| +x, -x, ~x                         | Positive, negative, bitwise inverse        |
+------------------------------------+--------------------------------------------+ 
| \*\*                               | Exponentiation                             |
+------------------------------------+--------------------------------------------+ 
| x[i], x[i:j], x(args...), x.member | Subscription, slicing, call, member access |
+------------------------------------+--------------------------------------------+ 


Order of Evaluation
===================

You cannot assume that multiple subexpressions are evaluated in the order that
seems natural. The compiler could do it in either order, so you cannot make
assumptions.

Side Effects
------------

A side effect is one of the following:

* modifying an object
* I/O (accessing a file, printing, accessing socket, etc)
* a call to a function which performs any of the above side effects

These are essentially the externally-visible effects of running a program.
They are called side effects because they are effects of expression evaluation
beyond the expression’s actual resulting value.

The compiler is allowed to perform the operations of your program in an order
different to the order implied by the source of your program, provided that in
the end all the necessary side effects actually take place. The compiler is
also allowed to entirely omit some operations; for example it’s allowed to
skip evaluating part of an expression if it can be certain that the value is
not used and evaluating that part of the expression won’t produce any needed
side effects.

Sequence Points
---------------

Another requirement on the compiler is that side effects should take place in
the correct order. In order to provide this without over-constraining the
compiler, the standards specify a list of sequence points. A sequence point is
one of the following:

TODO: specify this

* a call to a function (after argument evaluation is complete)
* the end of the left-hand operand of the and operator `and`
* the end of the left-hand operand of the or operator `or`
* the end of the first operand of the ternary operator `if a: b else: c`
* the end of an expression statement (i.e. an expression followed by ;)
* the end of the controlling expression of an if or switch statement
* the end of the controlling expression of a while or do statement
* the end of any of the three controlling expressions of a for statement
* the end of the expression in a return statement
* immediately before the return of a library function
* after the actions associated with an item of formatted I/O (as used for example with the strftime or the printf and * scanf famlies of functions).
* immediately before and after a call to a comparison function (as called for example by qsort)

At a sequence point, all the side effects of previous expression evaluations
must be complete, and no side effects of later evaluations may have taken
place.

This may seem a little hard to grasp, but there is another way to consider
this. Imagine you wrote a library (some of whose functions are external and
perhaps others not) and compiled it, allowing someone else to call one of your
functions from their code. The definitions above ensure that, at the time they
call your function, the data they pass in has values which are consistent with
the behavior specified by the abstract machine, and any data returned by your
function has a state which is also consistent with the abstract machine. This
includes data accessible via pointers (i.e. not just function parameters and
identifiers with external linkage).

The above is a slight simplification, since compilers exist that perform
whole-program optimization at link time. Importantly however, although they
might perform optimizations, the visible side effects of the program must be
the same as if they were produced by the abstract machine.

Statements
##########

You write statements to cause actions and to control flow within your
programs. You can also write statements that do not do anything at all, or do
things that are uselessly trivial.

Expression Statements
=====================

You can turn any expression into a statement by adding a semicolon to the end
of the expression or terminating the line with a newline. Here are some
examples::

    5
    2 + 2
    10 >= 9

In each of those, all that happens is that each expression is evaluated.
However, they are useless because they do not store a value anywhere, nor do
they actually do anything, other than the evaluation itself. The compiler is
free to ignore such statements.

Expression statements are only useful when they have some kind of side effect,
such as storing a value, calling a function, or (this is esoteric) causing a
fault in the program. Here are some more useful examples::

    let y = x + 25
    print("Hello, user!")

The switch statement
====================

TODO

The break statement
===================

You can use the break statement to terminate a `while`, `for`, or `switch`
statement. Here is an example::

    while x < 10:
        if foo(x):
            break
        x += 1

That example increments ``x`` until it reaches ``10``, or until ``foo(x)``
evaluates to ``true`` terminating the for loop prematurely.

If you put a ``break`` statement inside of a loop or switch statement which
itself is inside of a loop or switch statement, the ``break`` only terminates
the innermost loop or switch statement.

The continue statement
======================

You can use the ``continue`` statement in loops to terminate an iteration of
the loop and begin the next iteration. Here is an example::

    for x in range(0, 100):
        if x % 2 == 0:
            continue
        sum_of_odd_numbers += x

If you put a ``continue`` statement inside a loop which itself is inside a
loop, then it affects only the innermost loop.

The return statement
====================

You can use the ``return`` statement to end the execution of a function and
return program control to the function that called it. Here is the general
form of the return statement:

    return return-value

Here, *return-value* is an optional expression to return. If the function’s
return type is ``void``, then it is invalid to return an expression [TODO:
unless that's a void expression]. You can, however, use the return statement
without a return value.

If the function’s return type is not the same as the type of *return-value*,
and automatic type conversion cannot be performed, then returning *return-
value* is invalid.

If the function’s return type is not ``void`` and no return value is
specified, then the return statement is invalid.

Here are some examples of using the return statement, in both a void and non-
void function::

    def print_plus_five(x int) void:
        print(x + 5)
        return

    def square_value(x int) int:
        return x * x

The alias statement
===================

Sometimes it's convenient to have a shorter name for generic classes::

    class Foo<A object, B object>:
        pass

    alias Bar = Foo<int, float>

    let b Bar

Here, ``b`` is declared to be of type ``Bar`` which is an alias for ``Foo<int,
float>``. An alias does not introduce a new definition, it is purely another
name for an existing definition.

Any kinds of symbol can be given a new name, global variables, functions,
classes, even aliases::

    class Baz:
        pass
    def func():
        pass
    let var int = 0
    alias B = Baz
    alias f = func
    alias v = var
    alias A = B # Equivalent to 'alias A = Baz'

Aliases are useful when you want to make an imported symbol public::

    # In source file baz.tau
    import "bar/foo.tau"

    alias Foo = foo.Foo # Export foo.Foo

When ``baz.tau`` is imported the definition of ``Foo`` from ``bar/foo.tau`` is
available as ``Foo``.


Functions
#########

You can write functions to separate parts of your program into distinct sub-
procedures. To write a function, you must create a function definition.

Every program requires at least one function, called ``main``. That is where
the program's execution begins.

Function definitions
====================

You write a function definition to specify what a function actually does. A
function definition consists of information regarding the function's name,
return type, and types and names of parameters, along with the body of the
function. The function body is a series of statements enclosed in an indented
block of code; in fact it is simply a block (see Blocks).

Here is the general form of a function definition:

    def function-name (parameter-list) return-type:
        function-body

Here, *return-type* is the type of expression(s) returned by the function. It
can be left out, in which case it means that the function returns no value and
is equivalent to specifying the return type to ``void``.

The *parameter-list* is zero or more parameter definition. An parameter definition is on the form:

    parameter-name parameter-type

Here is an simple example of a function definition -- it takes two integers as
its parameters and returns the sum of them as its return value::

    def add_values(x int, y int) int:
        return x + y

Calling functions
=================

You can call a function by using its name and supplying any needed parameters.
Here is the general form of a function call:

    function-name (parameters)

A function call can make up an entire statement, or it can be used as a
sub-expression. Here is an example of a standalone function call::

    foo(5)

In that example, the function ``foo`` is called with the parameter ``5``.

Here is an example of a function call used as a sub-expression::

    a = square(5) + 1

Supposing that the function ``square`` squares its parameter, the above example
assigns the value ``26`` to ``a``.

If a parameter takes more than one argument, you separate parameters with
commas::

    a = quux(5, 10)


Function parameters
===================

Function parameters can be any expression -- a literal value, a value stored
in variable, an address in memory, or a more complex expression built by
combining these.

Within the function body, the parameter is a local copy of the value passed
into the function; you cannot change the value passed in by changing the local
copy. ::

    let x = 23
    foo(x)

    def foo(a int) int:
        a = 2 * a
        return a

In that example, even though the parameter ``a`` is modified in the function
``foo``, the variable ``x`` that is passed to the function does not change. If you
wish to use the function to change the original value of ``x``, then you would
have to incorporate the function call into an assignment statement::

    x = foo(x)

If the value that you pass to a function is a memory address (that is, a
pointer), then you can access (and change) the data stored at the memory
address. This achieves an effect similar to pass-by-reference in other
languages, but is not the same: the memory address is simply a value, just
like any other value, and cannot itself be changed. The difference between
passing a pointer and passing an integer lies in what you can do using the
value within the function.

Here is an example of calling a function with a pointer parameter::

    def foo(x *int):
        *x = *x + 42

    let a = 15
    foo(&a)

The formal parameter for the function is of type pointer-to-int, and we call
the function by passing it the address of a variable of type int. By
dereferencing the pointer within the function body, we can both see and change
the value stored in the address. The above changes the value of ``a`` to ``57``.

## Variable Length Parameter Lists

TODO: rewrite this. varargs are passed as an array

Calling functions through function pointers
===========================================

TODO write this


The main function
=================

Every program requires at least one function, called ``main``. This is where the
program begins executing. The return type for ``main`` is always int.

By convention, the return value from main indicates the program's exit status.
A value of zero indicates success and non-zero indicates an error. ::

    def main(argv []str) int:
        print("Hello world!")
       return 0

The parameter ``argv`` is an array of the parameters, as character strings.
``argv[0]``, the first element in the array, is the name of the program as typed
at the command line; any following array elements are the parameters that
followed the name of the program.

Here is an example main function that accepts command line parameters, and
prints out what those parameters are::

    def main(argv []str) int:
        for arg in argv:
            print(arg)
        return 0

Program structure and scope
###########################

Now that we have seen all of the fundamental elements of programs, it’s time
to look at the big picture.


Program structure
=================

A program may exist entirely within a single source file, but more commonly,
any non-trivial program will consist of several custom source files, and will
also use pre-existing libraries.

Scope
=====

Scope refers to what parts of the program can "see" a defined entity. A
declared entity can be visible only within a particular function, or within a
particular class, or source file.

Definition made at the top-level of a file (i.e., not within a function or
class) are visible to the entire file, including from within functions and
classes, but are not visible outside of the file unless explicitly imported.

Definition made within functions are visible only within those functions.


Imports
=======

The import declaration populates a namespace with symbols (classes, functions,
aliases, and global variables) from another source file. The common form of
import is simply the ``import`` keyword followed by the filename to import as
a quoted string, and imports all public symbols::

    import "foo/bar.tau"

By default imported symbols are put in a namespace with the same name as the
filename, in this case ``bar``. They can be accessed as::

   let b = bar.salmon()

Note that the member operator ``.`` is used to access a member of a namespace just as a member of a class.


Namespaces
----------

A namespace is a file-local bucket of names (aliases) which refer to classes,
functions, aliases, and variables, defined in another file. Namespaces are
used to organize imported files and to prevent name collisions that can occur
when importing symbols from different sources.

A namespace is a closed view into another source file in the sense that the
file that imports the namespace cannot modify it.


Explicit namespace
------------------

If two files have the same name but are located in different paths, there will
be an import conflict as they would occupy the same namespace. This can be
resolved by importing into an explicitly specified namespace::

   import "foo/bar.tau" foobar
   import "baz/bar.tau" bazbar

Here, the public symbols are imported into the namespace ``foobar`` and
``bazbar`` respectively.


Explicit imports
----------------

It is possible to explicitly enumerate the required symbols::

    import "foo/bar.tau" : Gazonk, baz

this will create the default namespace (``bar`` in this case) and populate it
with the names ``Gazonk`` and ``baz``. No other symbols will be imported, even
though there are more public symbols in ``foo/bar.tau``.

To explicitly enumerate required symbols and import them to a non-default
namespace do::

    import "foo/bar.tau" foobar : Gazonk, baz

this will create a namespace ``foobar`` and populate it with ``Gazonk`` and
``baz`` and no other symbols.

Using explicit imports, symbols from different source files can be imported into the same namespace::

    import "foo.tau" baz : Gazonk, Jazonk
    import "bar.tau" baz : bazonk

This also works for the default namespace::

    import "abc/foo.tau" : Gazonk, Jazonk
    import "def/foo.tau" : bazonk


Import private symbols
----------------------

Sometimes there a need to use a symbol that is private to another module.,
e.g., when testing, debugging, refactoring, etc. Only public definitions are
imported by default; private symbols has to be imported explicitly::

   import "foo/bar.tau" : _secret_function, _HiddenClass

this will only import ``_secret_function``, ``_HiddenClass`` to the namespace
``bar``; no other symbols are imported, not even other public ones.

If all public symbols and some private symbols are required, two imports can
be used::

   import "foo/bar.tau" # Imports all public symbols
   import "foo/bar.tau" : _secret_function, _HiddenClass


Import without namespace
------------------------

If a symbol is used often in a file, it can be cumbersome to specify the
namespace every time. In cases like this, symbols can be imported to the
global namespace, denoted ``_``, by enumerating them::

    import "foo/bar.tau" _ : Salmon, go_fishing

    def foo():
        go_fishing(Salomon())

Note that importing to the global namespace using the non-enumerating form is
illegal::

    import "foo/bar.tau" _ # Error: only enumerated symbols can be imported to global namespace.

This makes it possible for humans and compilers to easily find the definition of a symbol.


Names of imported files
-----------------------

The import system assumes that a specific file is always imported using the
same file name, that is, the same quoted string to ``import``. Importing a
single file using two different names will compile the file twice, just as if
the file was copied in the source tree.

Importing a single file to two different namespaces does not cause any of the
above problems. That is, this is legal::

    import "foo/bar.tau" bar0
    import "foo/bar.tau" bar1

though it's bad coding style, however, when enumerating import symbols it can
be useful.

The name of the imported file can be any that is allowed on the system on
which it is compiled. However, if the default namespace is used, the base name
must be a legal non-type identifier, e.g., in ``foo/class.tau`` is illegal,
while ``class/foo.tau`` is legal. Importing a file which name is not a legal
identifier can be done as follows::

    import "foo/class.tau" myclass
    import "baz/a+b.tau" ab


Exported symbols
----------------

Exported symbols are symbols that can be imported by another source file. Only
symbols defined using ``interface``, ``class``, ``def``, ``alias``, and
``let`` are exported. This means that imported symbols are not exported. For
example, symbols imported by ``foo.tau`` can not be accessed by doing ``import
"foo.tau"``.

To export an imported symbol one has to explicitly create an alias for it::

    import "bar/foo.tau" : Gazonk

    alias Gazonk = foo.Gazonk

This creates a new name for ``bar.Gazonk`` which is exported. This is purely a
rename, that is, there is still only one definition of ``Gazonk``.

Only public symbols (symbols not starting with an underscore) is exported by
default. Private symbols (symbols starting with an underscore) can be imported
by explicitly naming them.
