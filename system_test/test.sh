#! /usr/bin/env bash

passed=0
failed=0

DIR=`dirname $0`

DIFF_FLAGS="-q"
if [ "$1" == "-v" ]; then
	shift
	DIFF_FLAGS="-y"
fi

# Only run the provided tests (if any, otherwise run all)
if [ $# -gt 0 ]; then
    TESTS=$*
else
	TESTS=$DIR/*_test.tau
fi

# Compile builtins. Needed by all tests
g++ $DIR/../builtins/builtins.cc -c -o $DIR/builtins.o

# Run each test.
for file in $TESTS
do
	grep -oe '# expect: .*$' $file | sed 's/# expect: //' > $file.expected
	echo "   ---=== Started 'frontend/taula $file' ===---"
	frontend/taula -q -o $file.o -I ../src -I ../system_test $file
	gcc $DIR/builtins.o $file.o -o $file.exe
	$file.exe > $file.actual
	colordiff $DIFF_FLAGS $file.actual $file.expected | sed 's/^/        /'
	if [ $? -ne 0 ]; then
		failed=$((failed + 1))
	else
		passed=$((passed + 1))
	fi
	rm -f $file.actual $file.expected $file.o $file.exe
	echo "   ---=== Finished 'frontend/taula $file' ===---"
	echo
	echo
done

rm -f $DIR/builtins.o

echo
#echo "   Summary:"
#echo "      Passed: $passed"
#echo "      Failed: $failed"