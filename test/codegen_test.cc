#include "protest.hh"
#include "codegen.hh"
#include "parser2.hh"
#include "typecheck2.hh"

#include <string.h>

namespace taula {

extern "C" {
void* malloc_zeroinit(size_t size) {
  void* p = malloc(size);
  memset(p, 0, size);
  return p;
}
void zeroinit(void* p, size_t size) {
  memset(p, 0, size);
}
}


static std::vector<ast::Node const*> parse(const char* code, String module_name=String("path/mod.tau")) {
  Lexer lexer(module_name, code, &code[strlen(code)]);
  Parser parser(&lexer, module_name);
  parser.parse();
  return parser.ast();
}

static void add_builtins(TypeBuilder& tb) {
  auto ast = parse("class int:                                           \n"
                   "    def __add__(self int, rhs int) int: pass         \n"
                   "    def __sub__(self int, rhs int) int: pass         \n"
                   "    def __mul__(self int, rhs int) int: pass         \n"
                   "    def __lt__(self int, rhs int) bool: pass         \n"
                   "                                                     \n"
                   "class float:                                         \n"
                   "    def __add__(self float, rhs float) float: pass   \n"
                   "    def __sub__(self float, rhs float) float: pass   \n"
                   "    def __mul__(self float, rhs float) float: pass   \n"
                   "                                                     \n"
                   "class bool:                                          \n"
                   "    pass                                             \n"
                   "                                                     \n"
                   "class str:                                           \n"
                   "    pass                                             \n"
                   "                                                     \n"
                   "class Array<T>:                                      \n"
                   "    def __getitem__(self Array<T>, idx int) T:       \n"
                   "        pass                                         \n"
                   "    def __setitem__(self Array<T>, idx int, val T):  \n"
                   "        pass                                         \n"
                   "                                                     \n"
                   "class Pointer<S>:                                    \n"
                   "    pass                                             \n"
                   , String("__builtins__.tau"));
  tb.build_file((ast::File const*)ast.back());
}


void compile(const char* code, TypeChecker& tc, TypeBuilder& tb, CodeGenerator& cg) {
  auto ast = parse(code);
  add_builtins(tb);
  tb.build_file((ast::File const*)ast.back());
  tc.typecheck((ast::File const*)ast.back());
  cg.emit((ast::File const*)ast.back());
  cg.compile();
}

struct Array {
  int length;
  void* ptr;
};

typedef int (*Fia)(Array);
typedef Array (*Fa)();
typedef void* (*Fp)();
typedef void* (*Fpp)(void*);
typedef bool (*Fbi)(int);
typedef int (*Fii)(int);
typedef int (*Fi)();
typedef float (*Fff)(float);

suite("codegen") {
  TypeBuilder tb;
  TypeChecker tc(tb);
  CodeGenerator cg(tb, tc);

  test("integer binary expression") {
    compile("def bar(a int) int: return a + a * 3 - 1 \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(2) == 2 + 2 * 3 - 1);
    check(func(9) == 9 + 9 * 3 - 1);
  }

  test("integer less than") {
    compile("def bar(a int) bool: return a < 6 \n"
            , tc, tb, cg);
    
    auto func = (Fbi)cg.get_symbol("path_mod_tau_bar");
    check(func(5) == 1);
    check(func(6) == 0);
    check(func(7) == 0);
  }

  test("float binary expression") {
    compile("def bar(a float) float: return a * 2.5 - 0.25\n"
            , tc, tb, cg);
    
    auto func = (Fff)cg.get_symbol("path_mod_tau_bar");
    check(func(2) == 4.75);
  }

  test("if expression") {
    compile("def bar(a int) int: return if a: 10 else: 20 \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(0) == 20);
    check(func(1) == 10);
    check(func(2) == 10);
  }


  test("if stmt") {
    compile("def bar(a int) int:         \n"
            "    if a: return 10         \n"
            "    return 20               \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(0) == 20);
    check(func(1) == 10);
    check(func(2) == 10);
  }

  test("if-else stmt") {
    compile("def bar(a int) int:         \n"
            "    if a: return 10         \n"
            "    else: return 20         \n"
            "    return 0                \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(0) == 20);
    check(func(1) == 10);
    check(func(2) == 10);
  }


  test("declare local variable") {
    compile("def bar() int:              \n"
            "    let x = 10              \n"
            "    return x                \n"
            , tc, tb, cg);
    
    auto func = (Fi)cg.get_symbol("path_mod_tau_bar");
    check(func() == 10);
  }

  test("modify local variable") {
    compile("def bar() int:              \n"
            "    let x = 10              \n"
            "    x = 20                  \n"
            "    return x                \n"
            , tc, tb, cg);
    
    auto func = (Fi)cg.get_symbol("path_mod_tau_bar");
    check(func() == 20);
  }

  test("modify local variable in if stmt") {
    compile("def bar(a int) int:         \n"
            "    let x = 10              \n"
            "    if a:                   \n"
            "        x = 20              \n"
            "    return x                \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(0) == 10);
    check(func(1) == 20);
  }

  test("modify local variable in if expression") {
    // TODO: Strictly speaking we'd like this to evaluate to 15 (for a == 1).
    // However, this would involve copying the value of 'x' before evaluating
    // the if-expressions. For integers this is fine, but it can be
    // arbitrarily costly to copy a large object, thus we don't do this. Is
    // there no other way? Regardsless, it's bad style to have expressions modify
    // state.
    compile("def bar(a int) int:         \n"
            "    let x = 10              \n"
            "    return x + if a:        \n"
            "        x = 20              \n"
            "        5                   \n"
            "    else:                   \n"
            "        6                   \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(0) == 16);
    check(func(1) == 25);
  }

  test("call function without arguments") {
    compile("def foo() int: return 99             \n"
            "def bar() int: return 1 + foo()      \n"
            , tc, tb, cg);
    
    auto func = (Fi)cg.get_symbol("path_mod_tau_bar");
    check(func() == 1 + 99);
  }

  test("call function with one argument") {
    compile("def foo(a int) int: return a + 99     \n"
            "def bar(a int) int: return 1 + foo(a) \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(10) == 1 + 10 + 99);
  }

  test("read and write object field") {
    compile("class Foo:                            \n"
            "    let bar int                       \n"
            "def bar(a int) int:                   \n"
            "    let f Foo                         \n"
            "    f.bar = a + 1                     \n"
            "    return f.bar + 2                  \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(10) == 10 + 1 + 2);
  }

  test("passing object as function argument") {
    compile("class Foo:                            \n"
            "    let bar int                       \n"
            "def b(f Foo) int: return f.bar + 3    \n"
            "def bar(a int) int:                   \n"
            "    let f Foo                         \n"
            "    f.bar = a + 1                     \n"
            "    return b(f)                       \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(10) == 10 + 1 + 3);
  }


  test("call function on object") {
    compile("class Foo:                            \n"
            "    let field int                     \n"
            "    def foo(self *Foo, a int) int:    \n"
            "        return self.field + a         \n"
            "def bar(a int) int:                   \n"
            "    let f Foo                         \n"
            "    f.field = 99                      \n"
            "    return f.foo(a)                   \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(10) == 109);
  }

  test("while statement") {
    compile("def bar(a int) int:                   \n"
            "    let i int = 1                     \n"
            "    while i < a:                      \n"
            "        i = i * 2                     \n"
            "    return i                          \n"
            , tc, tb, cg);
    
    auto func = (Fii)cg.get_symbol("path_mod_tau_bar");
    check(func(0) == 1);
    check(func(1) == 1);
    check(func(2) == 2);
    check(func(3) == 4);
    check(func(4) == 4);
    check(func(5) == 8);
  }


  test("pointer to local") {
    compile("def bar() int:                        \n"
            "    let i int = 1                     \n"
            "    let p *int = &i                   \n"
            "    *p = 10                           \n"
            "    return i                          \n"
            , tc, tb, cg);
    
    auto func = (Fi)cg.get_symbol("path_mod_tau_bar");
    check(func() == 10);
  }


  test("read and write object field through pointer") {
    compile("class Foo:                            \n"
            "    let bar int                       \n"
            "def bar() int:                        \n"
            "    let ff Foo                        \n"
            "    let f = &ff                       \n"
            "    f.bar = 99                        \n"
            "    return f.bar + ff.bar             \n"
            , tc, tb, cg);
    
    auto func = (Fi)cg.get_symbol("path_mod_tau_bar");
    check(func() == 99 + 99);
  }

  test("new object") {
    compile("class Foo:                            \n"
            "    let bar int                       \n"
            "def bar() *Foo:                       \n"
            "    let f = new Foo()                 \n"
            "    f.bar = 99                        \n"
            "    return f                          \n"
            , tc, tb, cg);
    
    auto func = (Fp)cg.get_symbol("path_mod_tau_bar");
    auto res = (int*)func();
    check(*res == 99);
    free(res);
  }

  test("new object with constructor") {
    compile("class Foo:                            \n"
            "    let bar int                       \n"
            "    def __init__(self *Foo, bar int): \n"
            "        self.bar = bar                \n"
            "def bar() *Foo:                       \n"
            "    let f = new Foo(99)               \n"
            "    return f                          \n"
            , tc, tb, cg);
    
    auto func = (Fp)cg.get_symbol("path_mod_tau_bar");
    auto res = (int*)func();
    check(*res == 99);
    free(res);
  }

  test("cast pointer") {
    compile("class Foo:                            \n"
            "    let bar int                       \n"
            "def bar() *int:                       \n"
            "    let f = new Foo()                 \n"
            "    f.bar = 199                       \n"
            "    return cast *int(f)               \n"
            , tc, tb, cg);
    
    auto func = (Fp)cg.get_symbol("path_mod_tau_bar");
    auto res = (int*)func();
    check(*res == 199);
    free(res);
  }

  test("array subscript") {
    compile("def bar(a []int):                     \n"
            "    a[0] = a[3]                       \n"
            "    a[1] = a[2]                       \n"
            , tc, tb, cg);
    
    auto func = (Fia)cg.get_symbol("path_mod_tau_bar");
    int a[4] = {40, 41, 42, 43};
    func(Array{4, a});
    check(a[0] == 43);
    check(a[1] == 42);
    check(a[2] == 42);
    check(a[3] == 43);
  }

  test("new array") {
    compile("def bar() []int:                      \n"
            "    let a []int = new int[2]          \n"
            "    a[0] = 31                         \n"
            "    a[1] = 21                         \n"
            "    return a                          \n"
            , tc, tb, cg);
    
    auto func = (Fa)cg.get_symbol("path_mod_tau_bar");
    Array a = func();
    auto elems = (int*)a.ptr;
    check(a.length == 2);
    check(elems[0] == 31);
    check(elems[1] == 21);
  }

  test("string") {
    compile("def bar() str:                        \n"
            "    return \"hello world\"            \n"
            , tc, tb, cg);
    
    auto func = (Fp)cg.get_symbol("path_mod_tau_bar");
    auto s = (const char*)func();
    check(std::string(s)== "hello world");
  }

  test("global variable") {
    compile("let XYZ int                           \n"
            "def bar() int:                        \n"
            "    XYZ = 100                         \n"
            "    return XYZ                        \n"
            , tc, tb, cg);
    
    auto func = (Fi)cg.get_symbol("path_mod_tau_bar");
    auto s = func();
    check(s == 100);
  }

  cg.release_result();
}

}