#include "string.hh"
#include "protest.hh"

using namespace taula;

suite("string") {

  test("default constructor") {
    String s;
    check(s.length() == 0u);
    check(s.str() == "");
  }

  test("from buffer") {
    const char* buffer = "abcdef";

    String s0(&buffer[1], &buffer[5]);
    check(s0.length() == 4u);
    check(s0.str() == "bcde");

    String s1(buffer);
    check(s1.length() == 6u);
    check(s1.str() == "abcdef");
  }

  test("length of overflowed buffer") {
    const char* buffer = "abc";
    String s(&buffer[2], buffer);
    check(s.length() == 0u);
  }

  test("from std::string") {
    std::string ss = "0123456789012345678901234567890123456789";
    String s(ss);

    check(s.length() == 40u);
    check(s.str() == ss);
  }

  test("copy from String(std::string)") {
    std::string ss = "abc";
    String s0(ss);
    String s1(s0);

    check(s1.length() == 3u);
    check(s1.str() == "abc");
  }

  test("copy from String(buffer)") {
    const char* buffer = "abcdef";
    String s0(&buffer[1], &buffer[5]);
    String s1(s0);

    check(s1.length() == 4u);
    check(s1.str() == "bcde");
  }

  test("assignment") {
    const char* buffer = "abcdef";
    std::string ss = "abc";
    String s0(&buffer[1], &buffer[5]);
    String s1(ss);
    String s2;

    s2 = s0;
    check(s2.length() == 4u);
    check(s2.str() == "bcde");

    s2 = s1;
    check(s2.length() == 3u);
    check(s2.str() == "abc");
  }

  test("concat strings from buffer") {
    const char* buffer = "abcdef";
    String s0(buffer, buffer + 6);
    String s1(buffer, buffer + 3);
    String s2(buffer + 3, buffer + 6);
    String s3(s1 + s2);

    check(s0 == s3);

    // This is really ugly. Test that if the strings are
    // consecutive in the buffer, then the buffer is reused.
    const char* ptr0 = *reinterpret_cast<const char**>(&s0);
    const char* ptr3 = *reinterpret_cast<const char**>(&s3);
    check(ptr0 == ptr3);
  }

  test("concat strings from reference counted strings") {
    String s0(std::string("abcdef"));
    String s1(std::string("abc"));
    String s2(std::string("def"));
    String s3(s1 + s2);

    check(s0 == s3);
  }

  test("lskip of String(buffer)") {
    String s("abc");
    check(s.lskip(1) == String("bc"));
    check(s.lskip(2) == String("c"));
    check(s.lskip(3) == String());
  }

  test("lskip of reference counted String") {
    String s;
    {
      String ss(std::string("_abc"));
      s = ss.lskip(1);
    }
    check(s.lskip(1) == String("bc"));
    check(s.lskip(2) == String("c"));
    check(s.lskip(3) == String());
  }


  test("prefix of String(buffer)") {
    String s("abc");
    check(s.prefix(0) == String());
    check(s.prefix(1) == String("a"));
    check(s.prefix(2) == String("ab"));
    check(s.prefix(3) == String("abc"));
  }

  test("prefix of reference counted String") {
    String s;
    {
      String ss(std::string("abc_"));
      s = ss.prefix(3);
    }
    check(s.prefix(0) == String());
    check(s.prefix(1) == String("a"));
    check(s.prefix(2) == String("ab"));
    check(s.prefix(3) == String("abc"));
  }

  test("comparison") {
    const char* buffer = "abcdef";
    String s0(&buffer[1], &buffer[5]);
    String s1(&buffer[1], &buffer[5]);
    String s2(&buffer[1], &buffer[4]);
    String s3(std::string("bar"));
    String s4(std::string("bcde"));
    String s5(s4);

    check(s0 == s0);
    check(s0 == s1);
    check(!(s0 == s2));
    check(!(s0 == s3));
    check(s0 == s4);
    check(s0 == s5);

    check(s1 == s0);
    check(s1 == s1);
    check(!(s1 == s2));
    check(!(s1 == s3));
    check(s1 == s4);
    check(s1 == s5);

    check(!(s2 == s0));
    check(!(s2 == s1));
    check(s2 == s2);
    check(!(s2 == s3));
    check(!(s2 == s4));
    check(!(s2 == s5));

    check(!(s3 == s0));
    check(!(s3 == s1));
    check(!(s3 == s2));
    check(s3 == s3);
    check(!(s3 == s4));
    check(!(s3 == s5));

    check(s4 == s0);
    check(s4 == s1);
    check(!(s4 == s2));
    check(!(s4 == s3));
    check(s4 == s4);
    check(s4 == s5);

    check(s5 == s0);
    check(s5 == s1);
    check(!(s5 == s2));
    check(!(s5 == s3));
    check(s5 == s4);
    check(s5 == s5);
  }

  String::gc();
}