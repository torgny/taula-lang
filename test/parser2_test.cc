#include "parser2.hh"
#include "protest.hh"
#include <string.h>

#include <sstream>

using namespace taula;


static std::vector<ast::Node const*> parse(const char* code) {
  Lexer lexer(String("filename"), code, &code[strlen(code)]);
  Parser parser(&lexer, String("module.tau"));
  parser.parse();
  auto ast = parser.ast();
  // TODO: Memory leak of last element in vector.
  ast.pop_back(); // Ugly fix for getting rid of ast::File that is last in the ast vector.
  return ast;
}


static void erase(std::vector<ast::Node const*>& ast) {
  while (ast.size() > 0) {
    delete ast.back();
    ast.pop_back();
  }
}


static std::string str(ast::Node const* node) {
  std::stringstream ss;
  ast::Printer printer(ss);
  printer.visit(node);
  return ss.str();
}

static ast::Stmt const* fnbody(ast::Node const* ast) {
  auto fn = (ast::FunctionDecl*)ast;
  return fn->body;
}

static ast::Expr const* return_expr(ast::Node const* ast) {
  auto ret = (ast::ReturnStmt*)fnbody(ast);
  return ret->expr;
}

suite("parser2/import") {

  test("import declaration") {
    auto ast = parse("import \"path/file.tau\"\n");
    check(str(ast.back()) == "ImportDecl(path/file.tau)");
    erase(ast);
  }

}

suite("parser2/function") {
  test("no arguments") {
    auto ast = parse("def func():       \n"
                     "    let i int = 0 \n"
                     "    let d double  \n"
                     "    let h = 0     \n"
                     "    return 0      \n"
                     );
    auto fn = (ast::FunctionDecl*)ast.back();
    check(fn->name == String("func"));
    check(fn->type->argnames.size() == 0u);
    check(fn->type->argtypes.size() == 0u);
    erase(ast);
  }

  test("one arguments") {
    auto ast = parse("def func2(a int):   \n"
                     "    return 0       \n"
                     );
    auto fn = (ast::FunctionDecl*)ast.back();
    check(fn->name == String("func2"));
    check(fn->type->argnames.size() == 1u);
    check(fn->type->argtypes.size() == 1u);
    check(fn->type->argnames[0] == String("a"));
    check(str(fn->type->argtypes[0]) == "Typename(__builtins__, int, [])");
    erase(ast);
  }

  test("two arguments") {
    auto ast = parse("def func(a int, b int):   \n"
                     "    return 0              \n"
                     );
    auto fn = (ast::FunctionDecl*)ast.back();
    check(fn->type->argnames.size() == 2u);
    check(fn->type->argtypes.size() == 2u);
    erase(ast);
  }

  test("with return type") {
    auto ast = parse("def func() int:   \n"
                     "    return 0      \n"
                     );
    auto fn = (ast::FunctionDecl*)ast.back();
    check(str(fn->type->rettype) == "Typename(__builtins__, int, [])");
    erase(ast);
  }
}

suite("parser2/assignment") {
  test("simple assignment") {
    auto ast = parse("def f():    \n"
                     "    x = 0   \n");
    auto asgn = fnbody(ast.back());
    check(str(asgn) == "AssignmentStmt(=, IdentExpr(x), NumberExpr(0))");
  }
}


suite("parser2/if") {

  test("one line") {
    auto ast = parse("def f():            \n"
                     "    if a: x = 10    \n");
    auto ex = fnbody(ast.back());
    check(str(ex) == "ExprStmt(IfExpr(IdentExpr(a), AssignmentStmt(=, IdentExpr(x), NumberExpr(10)), CompoundStmt([])))");
    erase(ast);
  }

  test("without else") {
    auto ast = parse("def f():            \n"
                     "    if a: return 10 \n");
    auto ex = fnbody(ast.back());
    check(str(ex) == "ExprStmt(IfExpr(IdentExpr(a), ReturnStmt(NumberExpr(10)), CompoundStmt([])))");
    erase(ast);
  }

  test("with else") {
    auto ast = parse("def f():               \n"
                     "    if a:              \n"
                     "        return 10      \n"
                     "    else:              \n"
                     "        return 11      \n");
    auto ex = fnbody(ast.back());
    check(str(ex) == "ExprStmt(IfExpr(IdentExpr(a), ReturnStmt(NumberExpr(10)), ReturnStmt(NumberExpr(11))))");
    erase(ast);
  }

  test("with else-if") {
    auto ast = parse("def f():               \n"
                     "    if a:              \n"
                     "        return 10      \n"
                     "    else if b:         \n"
                     "        return 11      \n");
    auto ex = fnbody(ast.back());
    check(str(ex) == "ExprStmt(IfExpr(IdentExpr(a), ReturnStmt(NumberExpr(10)), ExprStmt(IfExpr(IdentExpr(b), ReturnStmt(NumberExpr(11)), CompoundStmt([])))))");
    erase(ast);
  }

  test("with else-if and else") {
    auto ast = parse("def f():               \n"
                     "    if a:              \n"
                     "        return 10      \n"
                     "    else if b:         \n"
                     "        return 11      \n"
                     "    else:              \n"
                     "        return 12      \n");
    auto ex = fnbody(ast.back());
    check(str(ex) == "ExprStmt(IfExpr(IdentExpr(a), ReturnStmt(NumberExpr(10)), ExprStmt(IfExpr(IdentExpr(b), ReturnStmt(NumberExpr(11)), ReturnStmt(NumberExpr(12))))))");
    erase(ast);
  }

  test("expression") {
    auto ast = parse("def f():            \n"
                     "    return if x: 10 \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "IfExpr(IdentExpr(x), ExprStmt(NumberExpr(10)), CompoundStmt([]))");
    erase(ast);
  }

  test("if as condition") {
    auto ast = parse("def f():                          \n"
                     "    return if if x: 1 else: 0: 10 \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "IfExpr(IfExpr(IdentExpr(x), ExprStmt(NumberExpr(1)), ExprStmt(NumberExpr(0))), ExprStmt(NumberExpr(10)), CompoundStmt([]))");
    erase(ast);
  }
}


suite("parser2/while") {
  test("statement") {
    auto ast = parse("def f():              \n"
                     "    while a:          \n"
                     "        10            \n");
    auto s = fnbody(ast.back());
    check(str(s) == "ExprStmt(WhileExpr(IdentExpr(a), ExprStmt(NumberExpr(10))))");
    erase(ast);
  }

  test("expression") {
    auto ast = parse("def f():              \n"
                     "    return while a:   \n"
                     "        10            \n");
    auto s = fnbody(ast.back());
    check(str(s) == "ReturnStmt(WhileExpr(IdentExpr(a), ExprStmt(NumberExpr(10))))");
    erase(ast);
  }
}



suite("parser2/for") {
  test("statement") {
    auto ast = parse("def f():              \n"
                     "    for a in x:       \n"
                     "        10            \n");
    check(str(fnbody(ast.back())) == "ExprStmt(ForExpr([a], IdentExpr(x), ExprStmt(NumberExpr(10))))");
    erase(ast);
  }

  test("two variables") {
    auto ast = parse("def f():              \n"
                     "    for a, b in x:    \n"
                     "        10            \n");
    check(str(fnbody(ast.back())) == "ExprStmt(ForExpr([a, b], IdentExpr(x), ExprStmt(NumberExpr(10))))");
    erase(ast);
  }

  test("expression") {
    auto ast = parse("def f():                   \n"
                     "    return for a in x: 10  \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "ForExpr([a], IdentExpr(x), ExprStmt(NumberExpr(10)))");
    erase(ast);
  }

  test("nested expression") {
    auto ast = parse("def f():                              \n"
                     "    return for a in x: for b in y: 10 \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "ForExpr([a], IdentExpr(x), ExprStmt(ForExpr([b], IdentExpr(y), ExprStmt(NumberExpr(10)))))");
    erase(ast);
  }

  test("expression with block") {
    auto ast = parse("def f():                   \n"
                     "    return for a in x:     \n"
                     "        10                 \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "ForExpr([a], IdentExpr(x), ExprStmt(NumberExpr(10)))");
    erase(ast);
  }
}

suite("parser2/expr/literals") {

  test("floating point") {
    auto ast = parse("def f():               \n"
                     "    return 0.0         \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "FpNumberExpr(0.0)");
    erase(ast);
  }

  test("empty array") {
    auto ast = parse("def f():               \n"
                     "    return []          \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "ArrayExpr([])");
    erase(ast);
  }

  test("array") {
    auto ast = parse("def f():               \n"
                     "    return [1, 2]      \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "ArrayExpr([NumberExpr(1), NumberExpr(2)])");
    erase(ast);
  }

  test("dict") {
    auto ast = parse("def f():                  \n"
                     "    return [1: 10, 2: 20] \n");
    auto ex = return_expr(ast.back());
    check(str(ex) == "DictExpr([NumberExpr(1), NumberExpr(2)], [NumberExpr(10), NumberExpr(20)])");
    erase(ast);
  }
}


suite("parser2/expr") {

  test("parenthesis") {
    auto ast = parse("def f(): return (((foo)));");
    auto ex = return_expr(ast.back());
    check(str(ex) == "IdentExpr(foo)");

    erase(ast);
  }

  test("quoted string") {
    auto ast = parse("def f(): return \"hello world\";");
    auto ex = return_expr(ast.back());
    check(str(ex) == "StringExpr(hello world)");

    erase(ast);
  }

  test("x * y + z") {
    auto ast = parse("def f(): return x * y + z;");
    auto bin = str(return_expr(ast.back()));
    check(bin == "BinaryExpr(+, BinaryExpr(*, IdentExpr(x), IdentExpr(y)), IdentExpr(z))");
    erase(ast);
  }

  test("x + y * z") {
    auto ast = parse("def f(): return x + y * z;");
    auto bin = str(return_expr(ast.back()));
    check(bin == "BinaryExpr(+, IdentExpr(x), BinaryExpr(*, IdentExpr(y), IdentExpr(z)))");
    erase(ast);
  }

  test("w * x + y * z") {
    auto ast = parse("def f(): return w * x + y * z;");
    auto bin = str(return_expr(ast.back()));
    check(bin == "BinaryExpr(+, BinaryExpr(*, IdentExpr(w), IdentExpr(x)), BinaryExpr(*, IdentExpr(y), IdentExpr(z)))");
    erase(ast);
  }

  test("w + x * y + z") {
    auto ast = parse("def f(): return w + x * y + z;");
    auto bin = str(return_expr(ast.back()));
    check(bin == "BinaryExpr(+, IdentExpr(w), BinaryExpr(+, BinaryExpr(*, IdentExpr(x), IdentExpr(y)), IdentExpr(z)))");
    erase(ast);
  }

  test("function call without arguments") {
    auto ast = parse("def f(): return foobar();");
    auto expr = str(return_expr(ast.back()));
    check(expr == "FunctionCallExpr(IdentExpr(foobar), [])");
    erase(ast);
  }

  test("function call with arguments") {
    auto ast = parse("def f(): return foobar(0, 1);");
    auto expr = str(return_expr(ast.back()));
    check(expr == "FunctionCallExpr(IdentExpr(foobar), [NumberExpr(0), NumberExpr(1)])");
    erase(ast);
  }

  test("subscript") {
    auto ast = parse("def f(): a[b];");
    auto expr = str(return_expr(ast.back()));
    check(expr == "SubscriptExpr(IdentExpr(a), IdentExpr(b))");
    erase(ast);
  }

  test("subscript and function call") {
    auto ast = parse("def f(): a[b](c);");
    auto expr = str(return_expr(ast.back()));
    check(expr == "FunctionCallExpr(SubscriptExpr(IdentExpr(a), IdentExpr(b)), [IdentExpr(c)])");
    erase(ast);
  }

  test("member access") {
    auto ast = parse("def f(): a.b;");
    auto expr = str(return_expr(ast.back()));
    check(expr == "MemberExpr(IdentExpr(a), b)");
    erase(ast);
  }

  test("member access and function call") {
    auto ast = parse("def f(): a.b();");
    auto expr = str(return_expr(ast.back()));
    check(expr == "FunctionCallExpr(MemberExpr(IdentExpr(a), b), [])");
    erase(ast);
  }

  test("pointer to") {
    auto ast = parse("def f(): return &foo;");
    auto ex = return_expr(ast.back());
    check(str(ex) == "PointerToExpr(IdentExpr(foo))");

    erase(ast);
  }

  test("dereference pointer") {
    auto ast = parse("def f(): return *foo;");
    auto ex = return_expr(ast.back());
    check(str(ex) == "DereferenceExpr(IdentExpr(foo))");

    erase(ast);
  }

  test("binary and, or") {
    auto ast = parse("def f(): return z or x and y;");
    auto ex = return_expr(ast.back());
    check(str(ex) == "BinaryExpr(or, IdentExpr(z), BinaryExpr(and, IdentExpr(x), IdentExpr(y)))");

    erase(ast);
  }

  String::gc();
}


suite("parser2/interface") {

  test("one function") {
    auto ast = parse("interface i:      \n"
                     "    def f()       \n");
    check(str(ast.back()) == "InterfaceDecl(i, [], [], [FunctionDecl(f, [], FunctionType(Typename(__builtins__, void, []), [], []), null)])");
    erase(ast);
  }

  test("one function with arguments and return type") {
    auto ast = parse("interface i:         \n"
                     "    def f(a int) int \n");
    check(str(ast.back()) == "InterfaceDecl(i, [], [], [FunctionDecl(f, [], FunctionType(Typename(__builtins__, int, []), [a], [Typename(__builtins__, int, [])]), null)])");
    erase(ast);
  }

  test("zero functions") {
    auto ast = parse("interface i: \n"
                     "             \n");
    check(str(ast.back()) == "InterfaceDecl(i, [], [], [])");
    erase(ast);
  }

  String::gc();
}

suite("parser2/class") {

  test("empty body") {
    auto ast = parse("class i:            \n"
                     "    pass            \n");
    check(str(ast.back()) == "ClassDecl(i, [], [], [])");
    erase(ast);
  }

  test("one function") {
    auto ast = parse("class i:            \n"
                     "    def f():        \n"
                     "        return 0    \n");
    check(str(ast.back()) == "ClassDecl(i, [], [], [FunctionDecl(f, [], FunctionType(Typename(__builtins__, void, []), [], []), ReturnStmt(NumberExpr(0)))])");
    erase(ast);
  }

  test("with decorator") {
    auto ast = parse("@foobar(10)         \n"
                     "class i:            \n"
                     "    def f():        \n"
                     "        pass        \n");
    auto decl = (ast::ClassDecl*)ast.back();
    check(str(decl->decorators[0]) == "FunctionCallExpr(IdentExpr(foobar), [NumberExpr(10)])");
    erase(ast);
  }

  test("one member") {
    auto ast = parse("class i:            \n"
                     "    let foo int     \n");
    check(str(ast.back()) == "ClassDecl(i, [], [], [VariableDecl(foo, Typename(__builtins__, int, []), null)])");
    erase(ast);
  }

  test("with type parameter") {
    auto ast = parse("class Foo<T>:       \n"
                     "    pass            \n");
    check(str(ast.back()) == "ClassDecl(Foo, [], [TypeVariableDecl(T, Typename(__builtins__, object, []))], [])");
    erase(ast);
  }

  test("with function returning type variable") {
    auto ast = parse("class Foo<T>:           \n"
                     "    def foo() T: pass   \n");
    auto decl = (ast::ClassDecl*)ast.back();
    check(str(decl->decls[0]) == "FunctionDecl(foo, [], FunctionType(TypeVariable(T, []), [], []), PassStmt())");
    erase(ast);
  }

  test("with function returning specialized type variable") {
    auto ast = parse("class Foo<T>:                \n"
                     "    def foo() T<Foo>: pass   \n");
    auto decl = (ast::ClassDecl*)ast.back();
    check(str(decl->decls[0]) == "FunctionDecl(foo, [], FunctionType(TypeVariable(T, [Typename(module, Foo, [])]), [], []), PassStmt())");
    erase(ast);
  }

  test("with zero type parameters") {
    auto ast = parse("class Foo<>:       \n"
                     "    pass            \n");
    check(str(ast.back()) == "ClassDecl(Foo, [], [], [])");
    erase(ast);
  }

  test("with constrained type parameter") {
    auto ast = parse("class Foo<T x.Bar>:       \n"
                     "    pass            \n");
    check(str(ast.back()) == "ClassDecl(Foo, [], [TypeVariableDecl(T, Typename(x, Bar, []))], [])");
    erase(ast);
  }

  String::gc();
}


suite("parser2/types") {
  test("imported type") {
    auto ast = parse("def foo() x.Bar: \n"
                     "    return 0     \n");
    auto fn = (ast::FunctionDecl*)ast.back();
    check(str(fn->type->rettype) == "Typename(x, Bar, [])");
    erase(ast);
  }

  test("local type") {
    auto ast = parse("def foo() Bar:   \n"
                     "    return 0     \n");
    auto fn = (ast::FunctionDecl*)ast.back();
    // Note: 'module' comes from the name passed to the parser.
    check(str(fn->type->rettype) == "Typename(module, Bar, [])");
    erase(ast);
  }

  test("parameterized type") {
    auto ast = parse("def foo() x.Foo<y.Bar, z.Baz>: \n"
                     "    return 0         \n");
    auto fn = (ast::FunctionDecl*)ast.back();

    check(str(fn->type->rettype) == "Typename(x, Foo, [Typename(y, Bar, []), Typename(z, Baz, [])])");
    erase(ast);
  }

  test("nested parameterized type") {
    auto ast = parse("def foo() x.Foo<y.Bar<z.Baz>>: \n"
                     "    return 0         \n");
    auto fn = (ast::FunctionDecl*)ast.back();

    check(str(fn->type->rettype) == "Typename(x, Foo, [Typename(y, Bar, [Typename(z, Baz, [])])])");
    erase(ast);
  }

  test("parameterized type with zero parameters") {
    auto ast = parse("def foo() x.Foo<>: \n"
                     "    return 0         \n");
    auto fn = (ast::FunctionDecl*)ast.back();

    check(str(fn->type->rettype) == "Typename(x, Foo, [])");
    erase(ast);
  }

  test("pointer type") {
    auto ast = parse("def foo() *int: \n"
                     "    return 0         \n");
    auto fn = (ast::FunctionDecl*)ast.back();

    check(str(fn->type->rettype) == "Typename(__builtins__, Pointer, [Typename(__builtins__, int, [])])");
    erase(ast);
  }

  test("array type") {
    auto ast = parse("def foo() []int: \n"
                     "    return 0         \n");
    auto fn = (ast::FunctionDecl*)ast.back();

    check(str(fn->type->rettype) == "Typename(__builtins__, Array, [Typename(__builtins__, int, [])])");
    erase(ast);
  }

  test("array and pointer") {
    auto ast = parse("def foo() []*int: \n"
                     "    return 0         \n");
    auto fn = (ast::FunctionDecl*)ast.back();

    check(str(fn->type->rettype) == "Typename(__builtins__, Array, [Typename(__builtins__, Pointer, [Typename(__builtins__, int, [])])])");
    erase(ast);
  }
}


/**
  Problems:

  Foo.T -- function or type variables?

  Foo.T() -- instantiate type or call function of 'Foo'?

  bar.Foo -- 'bar' isn't always a module reference. It could be a reference to
  a local variable and 'Foo' is a type variable in the type of the variable.
  We could disallow this kind of member access and force the access to go
  through the type instead, but still stat doesn't solve all issues. For example,
  bar.Foo.T, how do we know that 'bar' is a module and not a local class?

  All of this can be handle, though, if there is a way to determine exactly when
  a type starts in the grammar, for example, the 'new' keyword.


def foo(f FooType*):
    let i int = 20
    foobar(i, new BarType() in f) // put the newly allocated BarType instance in the scope/lifetime of 'f'.


    new BarType() -- allocate new object that lives until the end of the function. This means that this is equivalent to
    using 'let' and allocate an object on the stack. That is, 'let x BarType* = new BarType()' is equivalent to
    'let x BarType'.
    foo.new BarType() -- allocate new object with the same lifetime as 'foo'
    For allocating object with the lifetime of the application, do 'GLOBAL.new BarType()' where GLOBAL is a global variable.

    This means that we can search for dynamic allocations by searching for '.new '
    ...
*/
suite("parser2/new expression") {
  test("allocate with lexical lifetime") {
    auto ast = parse("def f(): return new Foo();");
    auto expr = str(return_expr(ast.back()));
    check(expr == "NewExpr(Typename(module, Foo, []), [], null)");
    erase(ast);
  }

  test("allocate with dynamic lifetime") {
    auto ast = parse("def f(b *Bar): return bar.new Foo();");
    auto expr = str(return_expr(ast.back()));
    check(expr == "NewExpr(Typename(module, Foo, []), [], IdentExpr(bar))");
    erase(ast);
  }

  test("generic type") {
    auto ast = parse("def f(): return new Foo<int>();");
    auto expr = str(return_expr(ast.back()));
    check(expr == "NewExpr(Typename(module, Foo, [Typename(__builtins__, int, [])]), [], null)");
    erase(ast);
  }

  test("allocate array") {
    auto ast = parse("def f(): return new int[10];");
    auto expr = str(return_expr(ast.back()));
    check(expr == "NewExpr(Typename(__builtins__, Array, [Typename(__builtins__, int, [])]), [NumberExpr(10)], null)");
    erase(ast);
  }
}

suite("parser2/cast expression") {
  test("cast") {
    auto ast = parse("def f(): return cast Foo(foo);");
    auto expr = str(return_expr(ast.back()));
    check(expr == "CastExpr(Typename(module, Foo, []), IdentExpr(foo))");
    erase(ast);
  }
}