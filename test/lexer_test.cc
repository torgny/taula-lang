#include "lexer.hh"
#include "string.hh"
#include <string.h>
#include "protest.hh"


using namespace taula;

Lexer lexer_for(const char* str) {
  const char* end = &str[strlen(str)];
  return Lexer(String("filename"), str, end);
}


suite("lexer") {

  Token tok(String(""), TokenType::ERROR);

  test("keywords") {
    Lexer lexer = lexer_for("import if else return interface enum while");

    tok = lexer.next();
    check(tok.type() == TokenType::KW_IMPORT);

    tok = lexer.next();
    check(tok.type() == TokenType::KW_IF);

    tok = lexer.next();
    check(tok.type() == TokenType::KW_ELSE);

    tok = lexer.next();
    check(tok.type() == TokenType::KW_RETURN);

    tok = lexer.next();
    check(tok.type() == TokenType::KW_INTERFACE);

    tok = lexer.next();
    check(tok.type() == TokenType::KW_ENUM);

    tok = lexer.next();
    check(tok.type() == TokenType::KW_WHILE);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }


  test("number") {
    Lexer lexer = lexer_for("10 0x1af 0b011 0.2 11.99");

    tok = lexer.next();
    check(tok.type() == TokenType::NUMBER);
    //check(tok.u64(str) == 10u);

    tok = lexer.next();
    check(tok.type() == TokenType::NUMBER);
    //check(tok.u64(str) == 0x1afu);

    tok = lexer.next();
    check(tok.type() == TokenType::NUMBER);
    //check(tok.u64(str) == 3u);

    tok = lexer.next();
    check(tok.type() == TokenType::FPNUMBER);

    tok = lexer.next();
    check(tok.type() == TokenType::FPNUMBER);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("identifier") {
    Lexer lexer = lexer_for("x y0 _0 abcde_");

    tok = lexer.next();
    check(tok.type() == TokenType::IDENT);

    tok = lexer.next();
    check(tok.type() == TokenType::IDENT);

    tok = lexer.next();
    check(tok.type() == TokenType::IDENT);

    tok = lexer.next();
    check(tok.type() == TokenType::IDENT);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("operator") {
    Lexer lexer = lexer_for("~=!=<<<=>>>=== +-*/%&|^<>! =");

    tok = lexer.next();
    check(tok.type() == TokenType::OP_MATCH);
    check(tok.str() == String("~="));

    tok = lexer.next();
    check(tok.type() == TokenType::OP_NE);
    check(tok.str() == String("!="));

    tok = lexer.next();
    check(tok.type() == TokenType::OP_2LT);
    check(tok.str() == String("<<"));

    tok = lexer.next();
    check(tok.type() == TokenType::OP_LE);
    check(tok.str() == String("<="));

    tok = lexer.next();
    check(tok.type() == TokenType::OP_2GT);
    check(tok.str() == String(">>"));

    tok = lexer.next();
    check(tok.type() == TokenType::OP_GE);
    check(tok.str() == String(">="));

    tok = lexer.next();
    check(tok.type() == TokenType::OP_2EQ);
    check(tok.str() == String("=="));

    tok = lexer.next();
    check(tok.type() == TokenType::OP_PLUS);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_MINUS);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_STAR);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_SLASH);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_PERCENT);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_AMPER);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_PIPE);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_HAT);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_LT);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_GT);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_EXCLAM);

    tok = lexer.next();
    check(tok.type() == TokenType::OP_EQ);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("brackets") {
    Lexer lexer = lexer_for("(){}[]");

    tok = lexer.next();
    check(tok.type() == TokenType::LPAREN);

    tok = lexer.next();
    check(tok.type() == TokenType::RPAREN);

    tok = lexer.next();
    check(tok.type() == TokenType::LBRACE);

    tok = lexer.next();
    check(tok.type() == TokenType::RBRACE);

    tok = lexer.next();
    check(tok.type() == TokenType::LBRACKET);

    tok = lexer.next();
    check(tok.type() == TokenType::RBRACKET);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("implicit semicolons") {
    Lexer lexer = lexer_for("foo\n10\nreturn\n)\n]\n}\n");

    tok = lexer.next();
    check(tok.str() == String("foo"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("10"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("return"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String(")"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("]"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("}"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("implicit semicolons made explicit") {
    Lexer lexer = lexer_for("foo;\n10;\nreturn;\n);\n];\n};\n");

    tok = lexer.next();
    check(tok.str() == String("foo"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("10"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("return"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String(")"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("]"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("}"));

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("blocks") {
    Lexer lexer = lexer_for("foo:           \n"
                            "    +          \n"
                            "    baz =      \n"
                            "        beer * \n"
                            "             & \n"
                            "|              \n"
                            "ba: -          \n"
                            "more:          \n"
                            "    less:      \n"
                            "        !      \n"
                            "yyy:           \n"
                            "!              \n");

    tok = lexer.next();
    check(tok.str() == String("foo"));

    tok = lexer.next();
    check(tok.str() == String(":"));

    tok = lexer.next();
    check(tok.type() == TokenType::BEGIN_BLOCK);

    tok = lexer.next();
    check(tok.str() == String("+"));

    tok = lexer.next();
    check(tok.str() == String("baz"));

    tok = lexer.next();
    check(tok.str() == String("="));

    tok = lexer.next();
    check(tok.type() == TokenType::BEGIN_BLOCK);

    tok = lexer.next();
    check(tok.str() == String("beer"));

    tok = lexer.next();
    check(tok.str() == String("*"));

    tok = lexer.next();
    check(tok.str() == String("&"));

    tok = lexer.next();
    check(tok.type() == TokenType::END_BLOCK);

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.type() == TokenType::END_BLOCK);

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("|"));

    tok = lexer.next();
    check(tok.str() == String("ba"));

    tok = lexer.next();
    check(tok.str() == String(":"));

    tok = lexer.next();
    check(tok.str() == String("-"));

    tok = lexer.next();
    check(tok.str() == String("more"));

    tok = lexer.next();
    check(tok.str() == String(":"));

    tok = lexer.next();
    check(tok.type() == TokenType::BEGIN_BLOCK);

    tok = lexer.next();
    check(tok.str() == String("less"));

    tok = lexer.next();
    check(tok.str() == String(":"));

    tok = lexer.next();
    check(tok.type() == TokenType::BEGIN_BLOCK);

    tok = lexer.next();
    check(tok.str() == String("!"));

    tok = lexer.next();
    check(tok.type() == TokenType::END_BLOCK);

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.type() == TokenType::END_BLOCK);

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("yyy"));

    tok = lexer.next();
    check(tok.str() == String(":"));

    tok = lexer.next();
    check(tok.type() == TokenType::BEGIN_BLOCK);

    tok = lexer.next();
    check(tok.type() == TokenType::END_BLOCK);

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.str() == String("!"));

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("misc") {
    Lexer lexer = lexer_for(";. ..,@");

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.type() == TokenType::DOT);

    tok = lexer.next();
    check(tok.type() == TokenType::DOTDOT);

    tok = lexer.next();
    check(tok.type() == TokenType::COMMA);

    tok = lexer.next();
    check(tok.type() == TokenType::AT);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("comment") {
    Lexer lexer = lexer_for("0 /*foo*/ 1 // bar\n2");

    tok = lexer.next();
    check(tok.type() == TokenType::NUMBER);

    tok = lexer.next();
    check(tok.type() == TokenType::NUMBER);

    tok = lexer.next();
    check(tok.type() == TokenType::SEMICOLON);

    tok = lexer.next();
    check(tok.type() == TokenType::NUMBER);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("string") {
    Lexer lexer = lexer_for("\"\" \"hello\"");

    tok = lexer.next();
    check(tok.type() == TokenType::QUOTED_STRING);

    tok = lexer.next();
    check(tok.type() == TokenType::QUOTED_STRING);

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  test("error and recovery") {
    Lexer lexer = lexer_for("0a foo `+` bar");

    tok = lexer.next();
    check(tok.type() == TokenType::ERROR);
    check(tok.str() == String("0a"));

    tok = lexer.next();
    check(tok.type() == TokenType::IDENT);
    check(tok.str() == String("foo"));

    tok = lexer.next();
    check(tok.type() == TokenType::ERROR);
    check(tok.str() == String("`"));

    tok = lexer.next();
    check(tok.type() == TokenType::OP_PLUS);
    check(tok.str() == String("+"));

    tok = lexer.next();
    check(tok.type() == TokenType::ERROR);
    check(tok.str() == String("`"));

    tok = lexer.next();
    check(tok.type() == TokenType::IDENT);
    check(tok.str() == String("bar"));

    tok = lexer.next();
    check(tok.type() == TokenType::INPUT_END);
  }

  String::gc();
}
