#include "type_builder.hh"
#include "protest.hh"
#include <string.h>
#include <sstream>

using namespace taula;

static std::vector<ast::Node const*> parse(const char* code, String module_name=String("path/m.tau")) {
  Lexer lexer(module_name, code, &code[strlen(code)]);
  Parser parser(&lexer, module_name);
  parser.parse();
  return parser.ast();
}


static void erase(std::vector<ast::Node const*>& ast) {
  while (ast.size() > 0) {
    delete ast.back();
    ast.pop_back();
  }
}

suite ("type builder") {
  String filename("path/m.tau");

  test("module with int global") {
    auto ast = parse("let foo int  \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto cls = (ObjectType const*)tb.lookup_module(filename)->lookup_variable(String("foo"));
    check(cls->str() == "__builtins__.tau:int");
    erase(ast);
  }

  test("class with int field") {
    auto ast = parse("class Foo:       \n"
                     "    let foo Foo  \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto cls = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {});
    auto field = cls->field(String("foo"));
    check(field->str() == "path/m.tau:Foo");
    erase(ast);
  }

  test("class with type parameter") {
    auto ast = parse("class Bar: pass      \n"
                     "class Foo<T Bar>:    \n"
                     "    pass             \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());

    auto bar = (ObjectType const*)tb.lookup_type(filename, String("Bar"), {});    
    auto cls0 = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {bar});
    auto cls1 = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {});

    auto field = cls0->parameter(String("T"));
    check(field->str() == "path/m.tau:Bar");
    check(cls0 == cls1);

    erase(ast);
  }

  test("return specialized generic class") {
    auto ast = parse("class Foo<T>:          \n"
                     "    pass               \n"
                     "def foo() Foo<int>:    \n"
                     "    pass               \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto const& funcs = *tb.lookup_func(filename, String("foo"));
    check(funcs.size() == 1u);
    check(funcs[0]->rettype()->str() == "path/m.tau:Foo<__builtins__.tau:int>");
    erase(ast);
  }

  test("interface with function") {
    auto ast = parse("interface Foo:   \n"
                     "    def bar()    \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto cls = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {});
    auto const& funcs = *cls->lookup_funcs(String("bar"));
    check(funcs.size() == 1u);
    check(funcs[0]->rettype()->str() == "__builtins__.tau:void");
    erase(ast);
  }

  test("class with void function") {
    auto ast = parse("class Foo:       \n"
                     "    def bar():   \n"
                     "        pass     \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto cls = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {});
    auto const& funcs = *cls->lookup_funcs(String("bar"));
    check(funcs.size() == 1u);
    check(funcs[0]->rettype()->str() == "__builtins__.tau:void");
    erase(ast);
  }

  test("returning class") {
    auto ast = parse("class Foo:         \n"
                     "    def bar() Foo: \n"
                     "        pass       \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto cls = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {});
    auto const& funcs = *cls->lookup_funcs(String("bar"));
    check(funcs.size() == 1u);
    check(funcs[0]->rettype()->str() == "path/m.tau:Foo");
    erase(ast);
  }

  test("class as argument") {
    auto ast = parse("class Foo:          \n"
                     "    def bar(f Foo): \n"
                     "        pass        \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto cls = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {});
    auto const& funcs = *cls->lookup_funcs(String("bar"));
    check(funcs.size() == 1u);
    check(funcs[0]->argtypes()[0]->str() == "path/m.tau:Foo");
    erase(ast);
  }

  test("mutually referring classes") {
    auto ast = parse("class Foo:         \n"
                     "    def bar() Bar: \n"
                     "        pass       \n"
                     "class Bar:         \n"
                     "    def foo() Foo: \n"
                     "        pass       \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());

    auto foo = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {});
    auto bar_rettype = (*foo->lookup_funcs(String("bar")))[0]->rettype();
    auto bar = (ObjectType const*)tb.lookup_type(filename, String("Bar"), {});
    auto foo_rettype = (*bar->lookup_funcs(String("foo")))[0]->rettype();

    check(bar_rettype->str() == bar->str());
    check(foo_rettype->str() == foo->str());

    erase(ast);
  }

  test("with imports") {
    auto ast = parse("import \"path/baz.tau\" \n"
                     "class Foo:              \n"
                     "    def bar(f baz.Bar): \n"
                     "        pass            \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    auto cls = (ObjectType const*)tb.lookup_type(filename, String("Foo"), {});
    auto const& funcs = *cls->lookup_funcs(String("bar"));
    check(funcs.size() == 1u);
    check(funcs[0]->argtypes()[0]->str() == "path/baz.tau:Bar");
    erase(ast);
  }

  test("mutually referring classes in different modules") {
    auto ast_a = parse("import \"path/modb.tau\"    \n"
                       "class Foo:                  \n"
                       "    def bar() modb.Bar:     \n"
                       "        pass                \n",
                       String("path/moda.tau"));
    auto ast_b = parse("import \"path/moda.tau\"    \n"
                       "class Bar:                  \n"
                       "    def foo() moda.Foo:     \n"
                       "        pass                \n",
                       String("path/modb.tau"));

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast_a.back());
    tb.build_file((ast::File const*)ast_b.back());

    auto foo = (ObjectType const*)tb.lookup_type(String("path/moda.tau"), String("Foo"), {});
    auto bar_rettype = (*foo->lookup_funcs(String("bar")))[0]->rettype();
    auto bar = (ObjectType const*)tb.lookup_type(String("path/modb.tau"), String("Bar"), {});
    auto foo_rettype = (*bar->lookup_funcs(String("foo")))[0]->rettype();

    check(bar_rettype->str() == bar->str());
    check(foo_rettype->str() == foo->str());

    erase(ast_a);
    erase(ast_b);
  }

  test("global function returning void") {
    auto ast = parse("def foo():       \n"
                     "    pass         \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto const& funcs = *tb.lookup_func(filename, String("foo"));
    check(funcs.size() == 1u);
    check(funcs[0]->rettype()->str() == "__builtins__.tau:void");
    erase(ast);
  }

  test("overloaded function") {
    auto ast = parse("import \"bar.tau\" \n"
                     "def foo() bar.A:   \n"
                     "    pass           \n"
                     "def foo() bar.B:   \n"
                     "    pass           \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto const& funcs = *tb.lookup_func(filename, String("foo"));
    check(funcs.size() == 2u);
    check(funcs[0]->rettype()->str() == "bar.tau:A");
    check(funcs[1]->rettype()->str() == "bar.tau:B");
    erase(ast);
  }

  test("return type of function of specialized generic class") {
    auto ast = parse("class Foo<T>:           \n"
                     "    def bar() T:        \n"
                     "        pass            \n"
                     "def baz() Foo<int>:     \n"
                     "    pass                \n");

    TypeBuilder tb;
    tb.build_file((ast::File const*)ast.back());
    
    auto baz = *tb.lookup_func(filename, String("baz"));
    auto foo = (ObjectType const*)baz[0]->rettype();
    auto bar = *foo->lookup_funcs(String("bar"));
    auto foo_int = bar[0]->rettype();
    check(foo_int->str() == "__builtins__.tau:int");
    erase(ast);
  }
}
