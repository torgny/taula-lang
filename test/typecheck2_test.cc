#include "typecheck2.hh"
#include "type_builder.hh"
#include "protest.hh"
#include <assert.h>
#include <string.h>
#include <sstream>

using namespace taula;

static std::vector<ast::Node const*> parse(const char* code, String module_name=String("path/m.tau")) {
  Lexer lexer(module_name, code, &code[strlen(code)]);
  Parser parser(&lexer, module_name);
  parser.parse();
  return parser.ast();
}

static void erase(std::vector<ast::Node const*>& ast) {
  while (ast.size() > 0) {
    delete ast.back();
    ast.pop_back();
  }
}

static std::string str(ast::Node const* node) {
  std::stringstream ss;
  ast::Printer printer(ss);
  printer.visit(node);
  return ss.str();
}

static void print(ast::Node const* node) {
  printf("%s\n", str(node).c_str());
}

static ast::ReturnStmt const* return_stmt(ast::Node const* ast) {
  assert(ast->asttype == ast::TypeTag::FunctionDecl);
  auto fn = (ast::FunctionDecl*)ast;
  ast::Stmt const* body = fn->body;
  if (body->asttype == ast::TypeTag::ReturnStmt) {
    return (ast::ReturnStmt const*)body;
  } else if (body->asttype == ast::TypeTag::CompoundStmt) {
    auto stmts = (ast::CompoundStmt const*)body;
    auto last = stmts->stmts.back();
    assert(last->asttype == ast::TypeTag::ReturnStmt);
    return (ast::ReturnStmt const*)last;
  }
  assert(false);
}

static void add_builtins(TypeBuilder& tb) {
  auto ast = parse("class int:                                           \n"
                   "    def __add__(self int, rhs int) int:              \n"
                   "        pass                                         \n"
                   "    def __sub__(self int, rhs int) int:              \n"
                   "        pass                                         \n"
                   "                                                     \n"
                   "class float:                                         \n"
                   "    pass                                             \n"
                   "                                                     \n"
                   "class bool:                                          \n"
                   "    pass                                             \n"
                   "                                                     \n"
                   "class str:                                           \n"
                   "    pass                                             \n"
                   "                                                     \n"
                   "class Array<T>:                                      \n"
                   "    def __getitem__(self Array<T>, idx int) T:       \n"
                   "        pass                                         \n"
                   "    def __setitem__(self Array<T>, idx int, val T):  \n"
                   "        pass                                         \n"
                   "                                                     \n"
                   "class Pointer<S>:                                    \n"
                   "    pass                                             \n"
                   , String("__builtins__.tau"));
  tb.build_file((ast::File const*)ast.back());
}

static std::string return_type(std::vector<ast::Node const*> const& ast) {
  auto ret = return_stmt(ast[ast.size() - 2]);
  TypeBuilder tb;
  tb.build_file((ast::File const*)ast.back());
  add_builtins(tb);

  TypeChecker tc(tb);
  tc.typecheck((ast::File const*)ast.back());
  auto type = tc.type_for(ret->expr);
  if (type == nullptr)
    return "nullptr";
  return type->str();
}


suite("typecheck/arithmetic") {
	test("integer literal") {
    auto ast = parse("def f():           \n"
                     "    return 1       \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
	}

  test("float literal") {
    auto ast = parse("def f():           \n"
                     "    return 1.0     \n");
    check(return_type(ast) == "__builtins__.tau:float");
    erase(ast);
  }

  test("integer add") {
    auto ast = parse("def f() int:         \n"
                     "    return 1 + 2     \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

  test("integer add and subtract") {
    auto ast = parse("def f() int:         \n"
                     "    return 1 + 2 - 3 \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }
}


suite("typecheck/if-then-else expression") {

  test("same type in both branches") {
    auto ast = parse("def f(x bool) int:           \n"
                     "    return if x: 1 else: 2   \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

  test("different types in branches") {
    auto ast = parse("def f(x bool) void:          \n"
                     "    return if x: 1 else: 2.0 \n");
    check(return_type(ast) == "__builtins__.tau:void");
    erase(ast);
  }

  test("nested") {
    auto ast = parse("def f(x bool) int:                                         \n"
                     "    return if x: (if x: 1 else: 2) else: (if x: 1 else: 2) \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }


  test("else if") {
    auto ast = parse("def f(x bool) int:                      \n"
                     "    return if x: 1 else if x: 2 else: 3 \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

  test("else if with different types") {
    auto ast = parse("def f(x bool) void:                       \n"
                     "    return if x: 1.0 else if x: 2 else: 3 \n");
    check(return_type(ast) == "__builtins__.tau:void");
    erase(ast);
  }

  test("several statements") {
    auto ast = parse("def f(x bool) float:       \n"
                     "    return if x:           \n"
                     "        10                 \n"
                     "        1.0                \n"
                     "    else:                  \n"
                     "        20                 \n"
                     "        2.0                \n");
    check(return_type(ast) == "__builtins__.tau:float");
    erase(ast);
  }

  test("no else") {
    auto ast = parse("def f(x bool) void:        \n"
                     "    return if x:           \n"
                     "        1.0                \n");
    check(return_type(ast) == "__builtins__.tau:void");
    erase(ast);
  }

  // TODO: Should we handle variable shadowing? Or it this a compilation error?
  test("new local variable") {
    auto ast = parse("def f(x bool) int:         \n"
                     "    let y int = 0          \n"
                     "    return if x:           \n"
                     "        let z int = 0      \n"
                     "        z                  \n"
                     "    else:                  \n"
                     "        y                  \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }   
}

suite("typecheck/function call") {
  test("simple call") {
    auto ast = parse("def f() int:       \n"
                     "    return 0       \n"
                     "def g() int:       \n"
                     "    return f()     \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

}


suite("typecheck/variables") {

  test("declare local using type and init") {
    auto ast = parse("def f() int:       \n"
                     "    let x int = 0  \n"
                     "    return x       \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

  test("declare local using type") {
    auto ast = parse("def f() int:       \n"
                     "    let x int      \n"
                     "    return x       \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

  test("declare local using init") {
    auto ast = parse("def f() int:       \n"
                     "    let x = 0       \n"
                     "    return x       \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

  test("function argument") {
    auto ast = parse("def f(x int) int:   \n"
                     "    return x       \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

  test("global variable") {
    auto ast = parse("let x int = 0      \n"
                     "def f() int:       \n"
                     "    return x       \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }
}


suite("typecheck/pointer") {

  test("new expression") {
    auto ast = parse("class Foo:               \n"
                     "    pass                 \n"
                     "def f() *Foo:            \n"
                     "    return new Foo()     \n");
    check(return_type(ast) == "__builtins__.tau:Pointer<path/m.tau:Foo>");
    erase(ast);
  }

  test("cast expression") {
    auto ast = parse("class Foo:               \n"
                     "    pass                 \n"
                     "def f(i *int) *Foo:      \n"
                     "    return cast *Foo(i)  \n");
    check(return_type(ast) == "__builtins__.tau:Pointer<path/m.tau:Foo>");
    erase(ast);
  }

  test("pointer to local") {
    auto ast = parse("def f() *int:            \n"
                     "    let x int = 0        \n"
                     "    return &x            \n");
    check(return_type(ast) == "__builtins__.tau:Pointer<__builtins__.tau:int>");
    erase(ast);
  }
  
  test("dereference pointer") {
    auto ast = parse("def f(a *int) int:       \n"
                     "    return *a            \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }
}


suite("typecheck/array") {

  test("getitem") {
    auto ast = parse("def f(x []int) int: \n"
                     "    return x[0]     \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }

  test("new") {
    auto ast = parse("def f() []int:             \n"
                     "    return new int[10]     \n");
    check(return_type(ast) == "__builtins__.tau:Array<__builtins__.tau:int>");
    erase(ast);
  }

  test("string") {
    auto ast = parse("def f() str:              \n"
                     "    return \"hello\"      \n");
    check(return_type(ast) == "__builtins__.tau:str");
    erase(ast);
  }

}


suite("typecheck/field") {

  test("read field") {
    auto ast = parse("class Foo:          \n"
                     "    let bar int     \n"
                     "def f(f Foo) int:   \n"
                     "    return f.bar    \n");
    check(return_type(ast) == "__builtins__.tau:int");
    erase(ast);
  }
}
