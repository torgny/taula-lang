#include "codegen.hh"
#include "parser2.hh"
#include "typecheck2.hh"

#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <set>
#include <algorithm>

using namespace taula;

static bool quiet = false;

#define PRINT(...) { if (not quiet) printf(__VA_ARGS__); }

void find_dependencies(const char* imported_filename,
                       std::vector<std::pair<std::string, std::string>>& deps,
                       std::vector<std::string> const& source_dirs,
                       std::set<std::string>& processed,
                       bool absolute_filename) {
  if (processed.find(imported_filename) != processed.end())
    return;

  PRINT("Adding dependency: %s\n", imported_filename);
  processed.insert(imported_filename);

  std::string filename;
  int fd = -1;
  if (absolute_filename) {
    filename = imported_filename;
    fd = open(filename.c_str(), O_RDONLY);
  } else {
    for (auto const& dir : source_dirs) { 
      filename = dir + "/" + imported_filename;
      fd = open(filename.c_str(), O_RDONLY);
      if (fd >= 0)
        break;
    }
  }

  struct stat sb;
  if (fstat(fd, &sb) == -1) {
    PRINT("Error: failed to open %s.\n", imported_filename);
    return;
  }

  deps.push_back(std::pair<std::string, std::string>(filename, imported_filename));

  auto code = (const char*)mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

  String module_filename(imported_filename, &imported_filename[strlen(imported_filename)]);
  Lexer lexer(module_filename, code, &code[sb.st_size]);
  Parser parser(&lexer, module_filename);
  parser.parse();

  auto ast = parser.ast();
  auto file = (ast::File const*)ast.back();
  for (auto decl : file->decls) {
    if (decl->asttype == ast::TypeTag::ImportDecl) {
      find_dependencies(((ast::ImportDecl*)decl)->name.str().c_str(), deps, source_dirs, processed, false);
    }
  }
  close(fd);
}

int compile(const char* physical_filename, const char* logical_filename,
            TypeBuilder& tb, TypeChecker& tc, CodeGenerator& cg, bool builtins) {
    PRINT("Compiling: %s\n", physical_filename);
    int fd = open(physical_filename, O_RDONLY);
    struct stat sb;
    if (fstat(fd, &sb) == -1) {
      PRINT("Error: failed to open %s.\n", physical_filename);
      return 1;
    }

    auto code = (const char*)mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

    String module_filename(logical_filename, &logical_filename[strlen(logical_filename)]);
    Lexer lexer(module_filename, code, &code[sb.st_size]);
    Parser parser(&lexer, module_filename);
    parser.parse();

    auto ast = parser.ast();
    auto file = (ast::File const*)ast.back();
    tb.build_file(file);

    // Builtins are only needed for typechecking.
    if (builtins)
      return 0;

    tc.typecheck(file);
    cg.emit(file);

    return 0;
}


int main(int argn, const char* argv[]) {

  if (argn < 2) {
    printf("Error: no input files.\n");
    return 1;
  }

  std::string output_file = "output.o";

  // Skip program name
  argv = &argv[1];

  std::vector<std::string> source_dirs;

  while (true) {
    // Quiet
    if (std::string(argv[0]) == "-q") {
      quiet = true;
      argv = &argv[1];
      continue;
    }

    // Output file
    if (std::string(argv[0]) == "-o") {
      output_file = argv[1];
      argv = &argv[2];
      continue;
    }

    // Source directory
    if (std::string(argv[0]) == "-I") {
      source_dirs.push_back(argv[1]);
      argv = &argv[2];
      continue;
    }

    break;
  }

  TypeBuilder tb;
  TypeChecker tc(tb);
  CodeGenerator cg(tb, tc);

  std::vector<std::pair<std::string, std::string>> deps;
  std::set<std::string> processed;
  for (int i = 0; argv[i] != nullptr; i++) {
    find_dependencies(argv[i], deps, source_dirs, processed, true);
  }

  if (compile("../builtins/__builtins__.tau", "__builtins__.tau", tb, tc, cg, true) != 0)
    return 1;

  for (int i = deps.size() - 1; i >= 0; i--) {
    auto const& physical_filename = deps[i].first;
    auto const& logical_filename = deps[i].second;
    if (compile(physical_filename.c_str(), logical_filename.c_str(), tb, tc, cg, false) != 0)
      return 1;
  }

  PRINT("backend compile.\n");
  cg.compile(output_file.c_str());


  return 0;
}