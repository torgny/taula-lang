#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

extern "C" {

const char* itos(int i) {
	char* s = (char*)malloc(16);
	sprintf(s, "%d", i);
	return s;
}

const char* btos(bool b) {
	return b ? "true" : "false";
}

struct Array {
	int length;
	void* ptr;
};

int len(Array* a) {
	if (a != NULL)
		return a->length;
	return 0;
}

void array_append(Array* a, int elem_size, void* elem) {
	a->ptr = realloc(a->ptr, (a->length + 1) * elem_size);
	char* p = (char*)(a->ptr);
	memcpy(&p[elem_size * a->length], elem, elem_size);
	a->length++;
}

int str_hash(const char* str) {
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return abs(hash); // Remove abs when we have unsigned integers.
}

bool str_eq(const char* lhs, const char* rhs) {
	if (lhs == NULL or rhs == NULL)
		return lhs == rhs;
	return strcmp(lhs, rhs) == 0;
}

const char* str_skip(const char** str_p, int chars) {
	const char* str = *str_p;
	return &str[chars];
}

bool str_startswith(const char** str_p, const char* pre, int begin) {
	const char* str = *str_p;
	str = &str[begin];
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
}

const char* str_substr(const char** str_p, int begin, int end) {
	assert(begin <= end);
	if (begin == end)
		return "";
	const char* str = *str_p;
	char* dst = (char*)malloc(end - begin + 1);
	strncpy(dst, &str[begin], end - begin);
	dst[end - begin + 1] = 0;
	return dst;
}

int str_len(const char** str_p) {
	const char* str = *str_p;
	if (str == NULL)
		return 0;
	return strlen(str);
}

void* malloc_zeroinit(size_t size) {
	void* p = malloc(size);
	memset(p, 0, size);
	return p;
}

void zeroinit(void* p, size_t size) {
  memset(p, 0, size);
}

const char* null_str() {
	return NULL;
}

unsigned char u8(int i) {
	return i;
}

const char* arr2str(Array arr) {
	return (const char*)arr.ptr;
}

void print_str(const char* s) {
	if (s == NULL)
		puts("(null)");
	else
		puts(s);
}

}